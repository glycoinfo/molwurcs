# MolWURCS

MolWURCS is a software designed to interconvert between WURCS and many file formats used in molecular modeling and computational chemistry and related area.

"WURCS" stands for Web3.0 Unique Representation of Carbohydrate Structure

* Matsubara, M. _et al._, _J. Chem. Inf. Model._, 57, 632-37 (2017)


## Features
* Read and write major text formats using CDK ( https://cdk.github.io/ )
  * Compatible with SD file, SMILES, and etc...
* Extract glycan structure(s) from input molecules and output them in WURCS format
* Build a molecule from WURCS in 2D diagram


## Requirements
* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher


## Build

    $ mvn clean compile assembly:single


## Usage

    $ java -jar MolWURCS.jar  [--with-aglycone] [--title-property-id <PROPERTY_ID>] [--output-no-result] --in <FORMAT> --out <FORMAT> [<INPUT_FORMAT_STRING>]

### Options

| option | argument | description |
| ------ | -------- | ----------- |
| -i, --in          | FORMAT=[wurcs\|mdlv2000\|sdf\|sdfv3000\|smi] | Set input format |
| -o, --out         | FORMAT=[wurcs\|mdlv2000\|sdf\|sdfv3000\|smi] | Set output format |
|     --with-aglycone     |             | Set option for outputting WURCS with aglycone |
| -p, --title-property-id | PROPERTY_ID | Use property as title, which key of the value is <PROPERTY_ID> |
| -n, --output-no-result  |             | Set option for outputting result explicitly even if the conversion is failed or no result |

### Available Formats
| format | description |
| ------ | ----------- |
| wurcs    | WURCS |
| mdlv2000 | MDLV2000 Format |
| sdf      | SD File for MDLV2000 format |
| sdfv3000 | SD File for MDLV3000 format |
| smi      | SMILES |


### Example

#### WURCS to molecules
Give input string as an argument

    $ java -jar MolWURCS.jar --in wurcs --out sdf "WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/"

Title for the WURCS can also be specified in input string  
(The title must be head of the string and separated with space or tab characters)

    $ java -jar MolWURCS.jar --in wurcs --out sdf "Title    WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/"

Give input string as standard input

    $ echo "Title    WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/" | java -jar MolWURCS.jar --in wurcs --out sdf

The result is printed on the standard output as following:
```
Title
  CDK     12102017472D

 12 12  0  0  1  0  0  0  0  0999 V2000
   -0.0074   -1.2404    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.2978   -0.4954    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.2960    1.0277    0.0000 C   0  0  2  0  0  0  0  0  0  0  0  0
    0.0122    1.7736    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
    1.3026    1.0286    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
    2.6017    1.7784    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3021   -0.4723    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0023   -2.7404    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5973   -1.2446    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5925    1.7820    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0163    3.2736    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.9006    1.0282    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  2  3  1  0  0  0  0
  3  4  1  0  0  0  0
  4  5  1  0  0  0  0
  5  6  1  1  0  0  0
  1  7  1  0  0  0  0
  1  8  1  1  0  0  0
  2  9  1  6  0  0  0
  3 10  1  1  0  0  0
  4 11  1  6  0  0  0
  5  7  1  0  0  0  0
  6 12  1  0  0  0  0
M  END
> <WURCS>
WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/

$$$$
```

The title of input WURCS can be added into output molecule as a property specifying property id with option '--title_property_id' ('-p' for short version)

    $ echo "b-D-Glc    WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/" | java -jar MolWURCS.jar --in wurcs --out sdf --title_property_id "TITLE"

```
b-D-Glc
  CDK     09082113542D

 12 12  0  0  1  0  0  0  0  0999 V2000
   -0.0074   -1.2404    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.2978   -0.4954    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
   -1.2960    1.0277    0.0000 C   0  0  2  0  0  0  0  0  0  0  0  0
    0.0122    1.7736    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
    1.3026    1.0286    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
    2.6017    1.7784    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3021   -0.4723    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
   -0.0023   -2.7404    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5973   -1.2446    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
   -2.5925    1.7820    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0163    3.2736    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.9006    1.0282    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  2  3  1  0  0  0  0
  3  4  1  0  0  0  0
  4  5  1  0  0  0  0
  5  6  1  1  0  0  0
  1  7  1  0  0  0  0
  1  8  1  1  0  0  0
  2  9  1  6  0  0  0
  3 10  1  1  0  0  0
  4 11  1  6  0  0  0
  5  7  1  0  0  0  0
  6 12  1  0  0  0  0
M  END
> <INPUT_WURCS>
WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/

> <TITLE>
b-D-Glc

$$$$
```


#### WURCS from molecules
Give input file as an argument

    $ java -jar MolWURCS.jar --in sdf --out wurcs "path/to/file.sdf"

Give input file contents as standard input

    $ cat "path/to/file.sdf" | java -jar MolWURCS.jar --in sdf --out wurcs

## Messages
Some processes output messages as standard error.  
The following messages...
- inform that input structure is changed during conversion process, or
- give notice the reason why WURCS is not output.

### INFO

* Start process for molecule: `title of input molecule`
  * Output when WURCS extraction process for a molecule is started.
  * `title of input molecule` is a string which is specified as title of the given molecule.
* Partition into molecules.
  * Output when input structure has multiple molecules.
  * **Two or more WURCS might be output in this case.**
* One or more metal atoms are removed.
  * Output when the given molecule has metal atoms and they are removed.
  * Metal atom is not considered as an element of WURCS.
  * **Output WURCS is different from input structure due to this change.**
* Charges on one or more atoms are changed to zero.
  * Output when an atom have a charge and it is changed to zero.
  * WURCS does not care any atomic charge.
    * Any negative charges are taken as missing hydrogens.
  * **Output WURCS is different from input structure due to this change.**
* Neutralized one or more single bonds connecting atoms with positive and negative charges into double bonds.
  * Output when a single bond connects two atoms which one has positive and another has negative charges.
    * The single bond will be changed to double bond, and each atom charge is neutralized.
  * **Output WURCS is different from input structure due to this change.**
* A modification is removed as aglycone.
  * Output when an atom group to be modification is removed in modification filtering process.
    * This process filters ones which do not satisfy some conditions such as too many rings are contained.
  * **Two or more WURCS might be output in this case.**
  * **Output WURCS is different from input structure due to this change.**
* Input molecule is separated with aglycone.
  * Output when the given molecule contains glycan with aglycone and the aglycone is removed from and separates the glycan.
  * The aglycone part is not contained in output WURCS.
  * **Two or more WURCS might be output in this case.**
  * **Output WURCS is different from input structure due to this change.**
  * **This separation is not performed when option `with-aglycone` is specified at input.**

### WARN

* No atom in the molecule except for metal and hydrogen atoms.
  * Output when any atoms which can be elements of WURCS are not contained in the given molecule.
  * **No WURCS is output in this case.**
* No carbon chain in the molecule.
  * Output when any carbon chains to be backbone of a monosaccharide are not found from the given molecule.
  * **No WURCS is output in this case.**

### ERROR

* Molecule containing radical atom(s) is not supported.
  * Output when radical atom(s) are contained in the given molecule.
  * Any radical atoms cannot be represented as WURCS element for now.
  * **No WURCS is output in this case.**
* The atom type `atom type` is not defined as backbone carbon for now.
  * Output when the given molecule contains a backbone carbon which the atom type is not defined in current WURCS rule.
  * **No WURCS is output in this case.**

## Notice
It is recommended to use [PubChem Standardization Service](https://pubchem.ncbi.nlm.nih.gov/standardize/standardize.cgi) for any input molecules except for WURCS.