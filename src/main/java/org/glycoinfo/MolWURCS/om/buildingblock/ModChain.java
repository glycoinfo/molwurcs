package org.glycoinfo.MolWURCS.om.buildingblock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for atomic information in MAP of Modification.
 * @author Masaaki Matsubara
 *
 */
public class ModChain {

	private static Logger logger = LoggerFactory.getLogger(ModChain.class);

	private IAtomContainer m_acModChain;
	private Map<IBond, Integer> m_mapBondFromStarToIndex;
	private List<IAtom> m_lStar;

	private Map<Integer, IAtom> m_mapMAPIDToAtom;

	public ModChain(IAtomContainer a_acModChain) {
		this.m_acModChain = a_acModChain;
		this.m_mapBondFromStarToIndex = new HashMap<>();
		this.m_lStar = new ArrayList<>();

		this.m_mapMAPIDToAtom = new HashMap<>();

	}

	public IAtomContainer getAtomContainer() {
		return this.m_acModChain;
	}

	/**
	 * Returns bonds containing the carbon which is a MAP Star with the specified index (StarIndex).
	 * These bonds are created in {@link #setAtomAsStar(int, IAtom, IBond)}.
	 * @param a_iStarIndex The number of StarIndex
	 * @return List of IBond from the carbons having the specified StarIndex. {@code null} if no such bonds.
	 */
	public List<IBond> getBondsFromStar(int a_iStarIndex) {
		List<IBond> lBonds = new ArrayList<>();
		for ( IBond bond : this.m_mapBondFromStarToIndex.keySet() ) {
			int iStarIndex = this.m_mapBondFromStarToIndex.get(bond);
			if ( iStarIndex != a_iStarIndex )
				continue;
			lBonds.add(bond);
		}
		return (lBonds.isEmpty())? null : lBonds;
	}

	/**
	 * Returns bonds containing the carbon which is a MAP Star.
	 * These bonds are created in {@link #setAtomAsStar(int, IAtom)}.
	 * @return List of IBond from the carbons.
	 */
	public List<IBond> getBondsFromStar() {
		List<IBond> bondsAll = new ArrayList<>(this.m_mapBondFromStarToIndex.keySet());
		return bondsAll;
	}

	public IAtom getAtom(int a_iMAPID) {
		return this.m_mapMAPIDToAtom.get(a_iMAPID);
	}

	public int getAtomMAPID(IAtom a_atom) {
		for ( int iMAPID : this.m_mapMAPIDToAtom.keySet() ) {
			if ( this.m_mapMAPIDToAtom.get(iMAPID).equals(a_atom) )
				return iMAPID;
		}
		return -1;
	}

	/**
	 * Set the specified atom as a MAP star connecting the specified bond with
	 * the specified index number (StarIndex).
	 * The star is the same carbon atom contained in the backbone carbons.
	 * and must be in the IAtomContainer specified at constructor.
	 * After specifying the star, the atom will remove from the IAtomContainer
	 * and a bond from the carbon to connected atom in the IAtomContainer will be created.
	 * The bond can be taken using {@link #getBondsFromStar(int)} specifying StarIndex.
	 * @param a_iStarIndex The number of star index. 
	 * @param a_atomStar IAtom of the star in MAP. This must be a carbon atom
	 *        and be contained in the IAtomContainer specified at constructor.
	 * @param a_bondFromStar IBond connecting the star carbon
	 * @return {@code true} if the star is set successfully.
	 */
	public boolean setAtomAsStar(int a_iStarIndex, IAtom a_atomStar, IBond a_bondFromStar) {
		if ( !this.m_acModChain.contains(a_atomStar) ) {
			logger.error("The atom for MAP star must be contained in the modification.");
			return false;
		}
		if ( !this.m_acModChain.contains(a_bondFromStar) ) {
			logger.error("The bond connecting MAP star must be contained in the modification.");
			return false;
		}
		if ( !a_bondFromStar.contains(a_atomStar) ) {
			logger.error("The bond connecting MAP star must have the atom for MAP star.");
			return false;
		}
		if ( a_atomStar.getAtomicNumber() != 6 ) {
			logger.error("The atom for MAP star must be a carbon.");
			return false;
		}

		if ( !this.m_lStar.contains(a_atomStar) )
			this.m_lStar.add(a_atomStar);
		this.m_mapBondFromStarToIndex.put(a_bondFromStar, a_iStarIndex);

		// Remove carbon from AtomContainer
		this.m_acModChain.removeBond(a_bondFromStar);
		if ( this.m_acModChain.getConnectedBondsCount(a_atomStar) == 0 )
			this.m_acModChain.removeAtom(a_atomStar);
		return true;
	}

	/**
	 * Reset star index for the bond from star. The bond must be specified
	 * using {@link #setAtomAsStar(int, IAtom, IBond)} before use of this method.
	 * @param a_iStarIndex The star index to replace
	 * @param a_bondFromStar IBond connecting a star carbon
	 * @return {@code true} if the replace is succeeded, {@code false} otherwise.
	 */
	public boolean resetStarIndex(int a_iStarIndex, IBond a_bondFromStar) {
		if ( !this.m_mapBondFromStarToIndex.containsKey(a_bondFromStar) ) {
			logger.warn("The bond is not set as a bond from star carbon.");
			return false;
		}
		this.m_mapBondFromStarToIndex.put(a_bondFromStar, a_iStarIndex);
		return true;
	}

	/**
	 * Returns star index for the specified bond connecting a star carbon.
	 * @param a_bondFromStar IBond connecting a star carbon
	 * @return star index for the star carbon
	 */
	public int getStarIndex(IBond a_bondFromStar) {
		if ( !this.m_mapBondFromStarToIndex.containsKey(a_bondFromStar) )
			return -1;
		return this.m_mapBondFromStarToIndex.get(a_bondFromStar);
	}

	public List<IAtom> getStars() {
		return new ArrayList<>(this.m_lStar);
	}

	/**
	 * Set the specified ID in MAP to the specified atom. The atom must be contained in this.
	 * @param a_iMAPID The number of atom ID in MAP
	 * @param a_atom IAtom to set the specified ID
	 * @return {@code true} if the ID is set successfully
	 */
	public boolean setIDToAtom(int a_iMAPID, IAtom a_atom) {
		if ( !this.m_acModChain.contains(a_atom) && !this.m_lStar.contains(a_atom) ) {
			logger.warn("The atom does not contain in this modification.");
			return false;
		}
		this.m_mapMAPIDToAtom.put(a_iMAPID, a_atom);
		return true;
	}

	/**
	 * Replace star carbon to the given atom.
	 * @param a_atomStarOld
	 * @param a_atomStarNew
	 * @param a_bondFromStarNew
	 * @return {@code true} if the replace is succeeded.
	 */
	public boolean replaceStar(IAtom a_atomStarOld, IAtom a_atomStarNew, IBond a_bondFromStarNew) {
		if ( a_atomStarOld.getAtomicNumber() != 6 ) {
			logger.error("The specified atom for old star must be a carbon.");
			return false;
		}
		if ( a_atomStarNew.getAtomicNumber() != 6 ) {
			logger.error("The specified atom for new star must be a carbon.");
			return false;
		}
		if ( !a_bondFromStarNew.contains(a_atomStarNew) ) {
			logger.error("The specified atom for new star must be in the new bond from star.");
			return false;
		}
		if ( !this.m_lStar.contains(a_atomStarOld) ) {
			logger.error("The specified atom for old star must be set as a star before.");
			return false;
		}

		IAtom atomMod = a_bondFromStarNew.getOther(a_atomStarNew);
		// Find old bond from star
		IBond bondFromStarOld = null;
		for ( IBond bondFromStar : this.m_mapBondFromStarToIndex.keySet() ) {
			if ( !bondFromStar.contains(atomMod) )
				continue;
			if ( !bondFromStar.contains(a_atomStarOld) )
				continue;
			if ( bondFromStar.getOrder() != a_bondFromStarNew.getOrder() )
				continue;
			bondFromStarOld = bondFromStar;
		}
		if ( bondFromStarOld == null ) {
			logger.error("Not found corresponding bond from star.");
			return false;
		}

		// For star
		this.m_lStar.remove(a_atomStarOld);
		this.m_lStar.add(a_atomStarNew);

		// For bond from star
		int index = this.m_mapBondFromStarToIndex.get(bondFromStarOld);
		this.m_mapBondFromStarToIndex.remove(bondFromStarOld);
		this.m_mapBondFromStarToIndex.put(a_bondFromStarNew, index);

		return true;
	}
}
