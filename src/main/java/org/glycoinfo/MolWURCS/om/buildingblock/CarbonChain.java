package org.glycoinfo.MolWURCS.om.buildingblock;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.util.cdk.ChemObjectProvider;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;

/**
 * Class for the carbon chain corresponding to Backbone. CarbonUnit is a component of this object.
 * @author Masaaki Matsubara
 * @see CarbonUnit
 */
public class CarbonChain {

	private List<CarbonUnit> m_lCarbonUnits;
	private List<IBond> m_lBondsConnectingCarbons;

	public CarbonChain() {
		this.m_lCarbonUnits = new ArrayList<>();
		this.m_lBondsConnectingCarbons = new ArrayList<>();
	}

	/**
	 * Add CarbonUnit to the CarbonChain. 
	 * @param unit CarbonUnit to add to the CarbonChain.
	 * @param orderPrev IBond.Order of the bond to the previous carbon. nullable for the first one.
	 * @param stereoBond IBond.Stereo of the bond to the previous carbon. nullable for the first one or the bond has no stereo.
	 * @see CarbonUnit
	 * @see IBond.Order
	 * @see IBond.Stereo
	 */
	public void addCarbonUnit(CarbonUnit unit, Order orderPrev, IBond.Stereo stereoBond) {
		if ( unit == null )
			// TODO: throw exception?
			return;
		this.m_lCarbonUnits.add(unit);

		if ( this.m_lCarbonUnits.size() < 2 )
			return;
		if ( orderPrev == null )
			// TODO: throw exception
			return;

		// connect last carbon
		CarbonUnit prev = this.m_lCarbonUnits.get(this.m_lCarbonUnits.size()-2);
		prev.setNextUnit(unit);
		unit.setPreviousUnit(prev);

		// Create bond with order
		IBond bond = ChemObjectProvider.createBond(prev.getCenterAtom(), unit.getCenterAtom(), orderPrev);
		if ( stereoBond != null )
			bond.setStereo(stereoBond);
		this.m_lBondsConnectingCarbons.add(bond);
		prev.setNextBond(bond);
		unit.setPreviousBond(bond);
	}

	public List<CarbonUnit> getCarbonUnits() {
		return this.m_lCarbonUnits;
	}

	/**
	 * Returns CabonUnit with the specified number.
	 * @param pos The number of position of carbon (> 0).
	 * @return A CarbonUnit at the specified position number
	 */
	public CarbonUnit getCarbonUnit(int pos) {
		return this.m_lCarbonUnits.get(pos-1);
	}

}
