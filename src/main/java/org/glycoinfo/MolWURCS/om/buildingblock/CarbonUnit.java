package org.glycoinfo.MolWURCS.om.buildingblock;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.util.cdk.ChemObjectProvider;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.stereo.TetrahedralChirality;

/**
 * Class for a carbon in the carbon chain. A component of CarbonChain.
 * @author Masaaki Matsubara
 * @see CarbonChain
 */
public class CarbonUnit {

	public static enum Stereo {
		S(ITetrahedralChirality.Stereo.CLOCKWISE),
		R(ITetrahedralChirality.Stereo.ANTI_CLOCKWISE),
		X(null);

		private ITetrahedralChirality.Stereo chiral;

		private Stereo(ITetrahedralChirality.Stereo chiral) {
			this.chiral = chiral;
		}

		/**
		 * Convert ITetrahedralChirality.Stereo to Stereo.
		 * @param chiral
		 * @return S for CLOCKWISE, R for ANTI_CLOCKWISE, or X for null
		 */
		public static Stereo toStereo(ITetrahedralChirality.Stereo chiral) {
			for ( Stereo stereo : values() ) {
				if ( stereo.chiral == chiral )
					return stereo;
			}
			return null;
		}
	}

	private IAtom m_atomCenter;
	private CarbonUnit m_unitPrev;
	private IBond m_bondPrev;
	private CarbonUnit m_unitNext;
	private IBond m_bondNext;
	private IBond[] m_bondMods;
	private Stereo m_stereo;

	public CarbonUnit(IAtom center,
			IAtom atomMod1, Order orderMod1,
			IAtom atomMod2, Order orderMod2,
			IAtom atomMod3, Order orderMod3
			) {
		this.m_atomCenter = center;

		this.m_unitPrev = null;
		this.m_bondPrev = null;
		this.m_unitNext = null;
		this.m_bondNext = null;

		this.m_bondMods = new IBond[3];
		setMod(0, atomMod1, orderMod1);
		setMod(1, atomMod2, orderMod2);
		setMod(2, atomMod3, orderMod3);

		this.m_stereo = null;
	}

	/**
	 * Constructor with center carbon and modification bonds
	 * @param center IAtom of center carbon of this unit
	 * @param bondMod1 IBond connecting center atom, nullable.
	 * @param bondMod2 IBond connecting center atom, nullable.
	 * @param bondMod3 IBond connecting center atom, nullable.
	 */
	public CarbonUnit(IAtom center, IBond bondMod1, IBond bondMod2, IBond bondMod3 ) {
		this(center,
				(bondMod1 == null)? null : bondMod1.getOther(center),
				(bondMod1 == null)? null : bondMod1.getOrder(),
				(bondMod2 == null)? null : bondMod2.getOther(center),
				(bondMod2 == null)? null : bondMod2.getOrder(),
				(bondMod3 == null)? null : bondMod3.getOther(center),
				(bondMod3 == null)? null : bondMod3.getOrder()
			);
	}

	public IAtom getCenterAtom() {
		return m_atomCenter;
	}

	void setPreviousUnit(CarbonUnit a_unit) {
		this.m_unitPrev = a_unit;
	}

	public CarbonUnit getPreviousUnit() {
		return this.m_unitPrev;
	}

	void setPreviousBond(IBond a_bond) {
		this.m_bondPrev = a_bond;
	}

	public IBond getPreviousBond() {
		return this.m_bondPrev;
	}

	void setNextUnit(CarbonUnit a_unit) {
		this.m_unitNext = a_unit;
	}

	public CarbonUnit getNextUnit() {
		return this.m_unitNext;
	}

	void setNextBond(IBond a_bond) {
		this.m_bondNext = a_bond;
	}

	public IBond getNextBond() {
		return this.m_bondNext;
	}

	public boolean isTerminal() {
		return this.m_unitPrev == null || this.m_unitNext == null;
	}

	private void setMod(int num, IAtom a_atomMod, Order a_order) {
		if ( a_atomMod == null || a_order == null )
			return;
		if ( num < 0 || num > 2 )
			return;
		this.m_bondMods[num] = ChemObjectProvider.createBond(this.m_atomCenter, a_atomMod, a_order);
	}

	public void setMod1(IAtom a_atomMod, Order a_order) {
		setMod(0, a_atomMod, a_order);
	}

	public IBond getBondToMod1() {
		return this.m_bondMods[0];
	}

	public void setMod2(IAtom a_atomMod, Order a_order) {
		setMod(1, a_atomMod, a_order);
	}

	public IBond getBondToMod2() {
		return this.m_bondMods[1];
	}

	public void setMod3(IAtom a_atomMod, Order a_order) {
		setMod(2, a_atomMod, a_order);
	}

	public IBond getBondToMod3() {
		return this.m_bondMods[2];
	}

	public int getModBondsCount() {
		int nBonds = 0;
		for ( IBond bond : this.m_bondMods )
			if ( bond != null )
				nBonds++;
		return nBonds;
	}

	/**
	 * Count number of modification bonds which have the specified order
	 * and are connecting atoms with the specified atomic number.
	 * @param a_order Order of the modification bond, nullable for any bond orders
	 * @param a_atomNum Atomic numbers of the modification atoms to be counted,
	 * nullable for any atoms
	 * @return The number of modification bonds matching the specified conditions
	 */
	public int countModBondsWith(Order a_order, int... a_atomNums) {
		return countModBondsWith(a_order, false, a_atomNums);
	}

	/**
	 * Count number of modification bonds which have the specified order
	 * and are connecting atoms with the specified atomic number.
	 * If negative mode is set, the result will be inverted.
	 * i.e. the atomic numbers for counting are ones except for the specified numbers.
	 * @param a_order Order of the modification bond, nullable for any bond orders
	 * @param a_mode A boolean flag for negative mode
	 * @param a_atomNum Atomic numbers of the modification atoms to be counted,
	 * nullable for any atoms
	 * @return The number of modification bonds matching the specified conditions
	 */
	public int countModBondsWith(Order a_order, boolean a_mode, int... a_atomNums) {
		int nMatched = 0;
		for ( IBond bond : this.m_bondMods ) {
			if ( bond == null )
				continue;
			int iAtomNumConn = bond.getOther(this.m_atomCenter).getAtomicNumber();
			if ( a_order != null && bond.getOrder() != a_order )
				continue;
			if ( a_atomNums == null ) {
				nMatched++;
				continue;
			}
			boolean isMatched = false;
			for ( int iAtomNum : a_atomNums ) {
				if ( iAtomNum != iAtomNumConn )
					continue;
				isMatched = true;
				break;
			}
			if ( a_mode )
				isMatched = !isMatched;
			if ( isMatched )
				nMatched++;
		}
		return nMatched;
	}

	/**
	 * Returns list of modification bonds connecting to this carbon.
	 * Non-destructive for the connecting bonds.
	 * @return List of modification bonds
	 */
	public List<IBond> getModBonds() {
		List<IBond> bonds = new ArrayList<>();
		for ( int i=0; i<this.m_bondMods.length; i++ ) {
			if ( this.m_bondMods[i] == null )
				continue;
			bonds.add(this.m_bondMods[i]);
		}
		return bonds;
	}

	/**
	 * Returns list of modification atoms connecting to this carbon.
	 * @return List of modification atoms
	 */
	public List<IAtom> getModficationAtoms() {
		List<IAtom> atoms = new ArrayList<>();
		for ( int i=0; i<this.m_bondMods.length; i++ ) {
			if ( this.m_bondMods[i] == null )
				continue;
			atoms.add(this.m_bondMods[i].getOther(this.m_atomCenter));
		}
		return atoms;
	}

	public void setStereo(Stereo a_stereo) {
		this.m_stereo = a_stereo;
	}

	public Stereo getStereo() {
		return this.m_stereo;
	}

	/**
	 * Returns ordered ligands around center carbon.<br>
	 * Example (<b>CLOCKWISE</b>):
	 * <pre>
	 *        head:    middle:  tail:
	 *   1       M3       C        C    
	 *   |       |        |        |    
	 * 2-C-3  M1-C-M2  M1-C-M2  M1-C-M2 
	 *   |       |        |        |    
	 *   4       C        C        M3   
	 * </pre>
	 * @return List of IAtom for lingands around center carbon, {@code null} if no modification is connected
	 */
	public IAtom[] createLigands() {
		if ( this.m_bondMods[0] == null )
			return null;
		IAtom atomSecond = (this.m_bondMods[1] != null)?
				this.m_bondMods[1].getEnd() : this.m_atomCenter;
		IAtom atomThird = (this.m_bondMods[2] != null)?
				this.m_bondMods[2].getEnd() : this.m_atomCenter;
		IAtom[] ligands = new IAtom[4];
		ligands[0] = (this.m_unitPrev != null)?
				this.m_unitPrev.getCenterAtom() : atomThird;
		ligands[1] = this.m_bondMods[0].getEnd();
		ligands[2] = (this.m_unitNext != null)?
				this.m_unitNext.getCenterAtom() : atomThird;
		ligands[3] = atomSecond;
		return ligands;
	}

	public ITetrahedralChirality createTetrahedralChilality() {
		if ( this.m_stereo == null )
			return null;
		if ( this.m_stereo == Stereo.X )
			return null;

		return new TetrahedralChirality(
				this.m_atomCenter, createLigands(), this.m_stereo.chiral
			);
	}
}
