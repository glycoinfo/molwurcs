package org.glycoinfo.MolWURCS.om.buildingblock;

public class ChainConnection {

	public static enum DirectionType {
		NONE,
		CHIRAL_CC,
		CHIRAL_ACC,
		DBSTEREO_E,
		DBSTEREO_Z,
		UNKNOWN;
	}

	private CarbonChain m_cc;
	private ModChain m_mc;

	private int m_iCarbonPos;
	private int m_iModBondIDOnCU;
	private DirectionType m_dType;
	private int m_iStarIndex;

	public ChainConnection(CarbonChain a_cc, ModChain a_mc, int a_iPos, int a_iModBondID, DirectionType a_dType, int a_iStarIndex) {
		this.m_cc = a_cc;
		this.m_mc = a_mc;

		this.m_iCarbonPos = a_iPos;
		this.m_iModBondIDOnCU = a_iModBondID;
		this.m_dType = a_dType;
		this.m_iStarIndex = a_iStarIndex;
	}

	public CarbonChain getCarbonChain() {
		return this.m_cc;
	}

	public ModChain getModChain() {
		return this.m_mc;
	}

	public int getCarbonPosition() {
		return this.m_iCarbonPos;
	}

	public int getModIDOnCarbonUnit() {
		return this.m_iModBondIDOnCU;
	}

	public DirectionType getDirectionType() {
		return this.m_dType;
	}

	public int getStarIndex() {
		return this.m_iStarIndex;
	}

}
