package org.glycoinfo.MolWURCS.exchange.fromWURCS;

import java.util.HashMap;
import java.util.Map;

import org.glycoinfo.MolWURCS.util.cdk.ChemObjectProvider;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomCyclic;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPBondType;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPStereo;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for conversion of MAPGraph
 * @author Masaaki Matsubara
 *
 */
public class MAPGraphToAtomContainer {

	private static Logger logger = LoggerFactory.getLogger(MAPGraphToAtomContainer.class);

	private IAtomContainer m_molMod;
	private Map<MAPAtomAbstract, IAtom> m_mapToAtom;
	private Map<MAPConnection, IBond> m_mapToBond;

	public MAPGraphToAtomContainer(MAPGraph a_map) {
		convert(a_map);
	}

	public IAtomContainer getAtomContainer() {
		return this.m_molMod;
	}

	public IAtom getMappedAtom(MAPAtomAbstract a_mapAtom) {
		return this.m_mapToAtom.get(a_mapAtom);
	}

	public IBond getMappedBond(MAPConnection a_mapConn) {
		return this.m_mapToBond.get(a_mapConn);
	}

	private void convert(MAPGraph a_map) {

		IAtomContainer molMod = ChemObjectProvider.createAtomContainer();
		Map<MAPAtomAbstract, IAtom> mapToAtom = new HashMap<>();
		Map<MAPConnection, IBond> mapToBond = new HashMap<>();
		for ( MAPAtomAbstract mapAtom : a_map.getAtoms() ) {

			IAtom atom = null;
			if ( mapAtom instanceof MAPAtomCyclic ) {
				logger.debug("MAPAtomCyclic is found.");
				// Load cyclic atom
				MAPAtomAbstract mapAtomOrig = ((MAPAtomCyclic)mapAtom).getCyclicAtom();
				atom = mapToAtom.get( mapAtomOrig );
			} else {
				// Create a new atom
				String t_strSymbol = mapAtom.getSymbol();
				atom = ChemObjectProvider.createAtom(t_strSymbol);

				molMod.addAtom(atom);

				// Set aromaticity to atom
				if ( mapAtom.isAromatic() )
					atom.setIsAromatic(true);
			}
			mapToAtom.put(mapAtom, atom);

			// Label stereo as CIP descriptor
			if ( mapAtom.getStereo() != null )
				atom.setProperty(CDKConstants.CIP_DESCRIPTOR, toCIPDescriptor(mapAtom.getStereo()));

			MAPConnection conn = mapAtom.getParentConnection();
			if ( conn == null )
				continue;

			// Create a new bond using parent connection
			IAtom atomParent = mapToAtom.get(conn.getAtom());
			Order order = toOrder(conn.getBondType());
			IBond bond = ChemObjectProvider.createBond(atomParent, atom, order);
			molMod.addBond(bond);

			mapToBond.put(conn, bond);
			mapToBond.put(conn.getReverse(), bond);

			// Set aromaticity to bond
			if ( mapAtom.isAromatic() && conn.getAtom().isAromatic() ) {
				bond.setIsAromatic(true);
				bond.setFlag(CDKConstants.SINGLE_OR_DOUBLE, true);
			}

			// Label stereo as CIP descriptor
			if ( conn.getStereo() != null )
				bond.setProperty(CDKConstants.CIP_DESCRIPTOR, toCIPDescriptor(conn.getStereo()));
		}

		this.m_molMod = molMod;
		this.m_mapToAtom = mapToAtom;
		this.m_mapToBond = mapToBond;
	}

	private Order toOrder(MAPBondType type) {
		switch(type) {
		case SINGLE:
			return Order.SINGLE;
		case DOUBLE:
			return Order.DOUBLE;
		case TRIPLE:
			return Order.TRIPLE;
		case AROMATIC:
		default:
			return Order.UNSET;
		}
	}

	private static String toCIPDescriptor(MAPStereo a_stereo) {
		return String.valueOf(a_stereo.getSymbol()).toUpperCase();
	}

}
