package org.glycoinfo.MolWURCS.exchange.fromWURCS;

import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.util.cdk.ChemObjectProvider;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.BackboneCarbon;
import org.glycoinfo.WURCSFramework.wurcs.graph.CarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor.ModificationType;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IBond.Stereo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackboneToCarbonChain {

	private static Logger logger = LoggerFactory.getLogger(BackboneToCarbonChain.class);

	private BackboneToCarbonChain() {};

	public static CarbonChain convert(Backbone a_bb) {
		CarbonChain cc = new CarbonChain();
		int preOrder = 0;
		for ( BackboneCarbon bc : a_bb.getBackboneCarbons() ) {
			CarbonDescriptor cd = (CarbonDescriptor)bc.getDescriptor();

			// Create CarbonUnit
			CarbonUnit unit = createCarbonUnit(cd);

			// Add carbon stereo
			CarbonUnit.Stereo chiral = (cd.isAnomeric())?
					getAnomerStereo(a_bb) : getCarbonChiralityStereo(cd);
			unit.setStereo(chiral);

			// Set the first CarbonUnit (no need connection info)
			if ( cc.getCarbonUnits().isEmpty() ) {
				cc.addCarbonUnit(unit, null, null);
				preOrder = cd.getBondTypeCarbon1();
				continue;
			}

			Order order = getOrder(preOrder);
			if ( order == Order.UNSET )
				logger.error("UNSET is set");

			// Add double bond stereo
			Stereo stereo = null;
			if ( preOrder == 2 )
				stereo = getDoubleBondStereo(cd.getStereo());

			cc.addCarbonUnit(unit, order, stereo);

			// Get bond info between carbons
			preOrder = ((preOrder != cd.getBondTypeCarbon1())?
				cd.getBondTypeCarbon1() : cd.getBondTypeCarbon2());
		}
		return cc;
	}

	private static CarbonUnit createCarbonUnit(ICarbonDescriptor a_cd) {
		IAtom carbon = ChemObjectProvider.createAtom(6);

		int nH = 0;
		IAtom[] modAtoms = new IAtom[3];
		Order[] modOrders = new Order[3];
		for ( int i=0; i<3; i++ ) {
			ModificationType modType = a_cd.getModificationType(i+1);
			if ( modType == null )
				continue;
			if ( modType == ICarbonDescriptor.H ) {
				nH++;
				continue;
			}
			Order order = getOrder(modType.getOrder());
			if (order == null)
				continue;

			modAtoms[i] = ChemObjectProvider.createAtom(8);
			modOrders[i] = order;
		}
		// Add hydrogens implicitly
		carbon.setImplicitHydrogenCount(nH);

		return new CarbonUnit(carbon,
				modAtoms[0], modOrders[0],
				modAtoms[1], modOrders[1],
				modAtoms[2], modOrders[2]
				);

	}

	private static Order getOrder(int type) {
		for ( Order order : Order.values() ) {
			if ( type == order.numeric() )
				return order;
		}
		return null;
	}

	private static CarbonUnit.Stereo getAnomerStereo(Backbone a_bb) {
		char anom = a_bb.getAnomericSymbol();
		if ( anom == 'x' )
			return CarbonUnit.Stereo.X;
		// Replace anomeric symbol from relative ("a" or "b") to absolute ("u" or "d")
		if ( anom == 'a' || anom == 'b' ) {
			// Check anomeric reference atom
			BackboneCarbon bcConf = null;
			int i=0;
			for ( BackboneCarbon bc : a_bb.getBackboneCarbons() ) {
				if ( !bc.getDescriptor().isChiral() )
					continue;
				i++;
				bcConf = bc;
				if ( i == 4 )
					break;
			}
			if ( bcConf == null ) {
				logger.warn("Absolute configuration of the anomeric carbon cannot be determined because no anomeric reference atom is found. ");
				return null;
			}
			String strStereo = bcConf.getDescriptor().getStereo();
			if ( strStereo == ICarbonDescriptor.X )
				return null;
			if ( strStereo == ICarbonDescriptor.S )
				anom = (anom == 'a')? 'b' : 'a';
			anom = (anom == 'a')? 'd' : 'u';
		}
		return (anom == 'u')? CarbonUnit.Stereo.S : CarbonUnit.Stereo.R;
	}

	private static CarbonUnit.Stereo getCarbonChiralityStereo(ICarbonDescriptor a_cd) {
		// Ignore non-sp3 carbon
		if ( a_cd.getHybridOrbital() != ICarbonDescriptor.SP3 )
			return null;
		// null if carbons have two or more hydrogens
		if ( a_cd.getModificationType(1).isHydrogen() )
			return null;
		// For carbons have no or unknown stereo 
		if ( a_cd.getStereo() == null || a_cd.getStereo() == ICarbonDescriptor.X )
			return CarbonUnit.Stereo.X;

		return ( a_cd.getStereo().toUpperCase().equals("S") )?
				CarbonUnit.Stereo.S : CarbonUnit.Stereo.R;
	}

	private static Stereo getDoubleBondStereo(String a_dbStereo) {
		if ( a_dbStereo == null )
			return null;
		switch (a_dbStereo) {
		case ICarbonDescriptor.E:
			return Stereo.E;
		case ICarbonDescriptor.Z:
			return Stereo.Z;
		case ICarbonDescriptor.X:
			return Stereo.E_OR_Z;
		default:
			return null;
		}
	}

}
