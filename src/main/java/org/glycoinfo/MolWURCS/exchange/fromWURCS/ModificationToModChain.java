package org.glycoinfo.MolWURCS.exchange.fromWURCS;

import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.map.MAPFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPStar;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModificationToModChain {

	private static Logger logger = LoggerFactory.getLogger(ModificationToModChain.class);

	private ModificationToModChain() {}

	public static ModChain convert(Modification a_mod) {
		MAPGraph graph = toMAPGraph(a_mod);
		if ( graph == null )
			return null;

		MAPGraphToAtomContainer MAPToAC = new MAPGraphToAtomContainer(graph);
		IAtomContainer molMAP = MAPToAC.getAtomContainer();

		ModChain modChain = new ModChain(molMAP);

		// Map MAPAtom ID to IAtom
		boolean isRemovedStereoOnAromatic = false;
		for ( int i=0; i<graph.getAtoms().size(); i++ ) {
			MAPAtomAbstract atom = graph.getAtoms().get(i);

			IAtom atomMapped = MAPToAC.getMappedAtom(atom);
			modChain.setIDToAtom(i+1, atomMapped);

			MAPConnection conn = atom.getParentConnection();
			if ( conn == null )
				continue;

			if ( conn.getStereo() == null )
				continue;
			// Do not add stereo if aromatic bond
			if ( atom.isAromatic() && conn.getAtom().isAromatic() ) {
				isRemovedStereoOnAromatic = true;
				continue;
			}
		}
		if ( isRemovedStereoOnAromatic )
			logger.warn("Stereochemistry on aromatic ring(s) was removed.");

		// Map star indices to carbons
		for ( MAPStar star : graph.getStars() ) {
			IAtom carbon = MAPToAC.getMappedAtom(star);
			MAPConnection connStar = star.getConnection();
			IBond bond = MAPToAC.getMappedBond(connStar);

			modChain.setAtomAsStar(star.getStarIndex(), carbon, bond);
		}

		return modChain;
	}

	private static MAPGraph toMAPGraph(Modification a_mod) {
		String strMAP = a_mod.getMAPCode();
		// Complement omitted MAP
		if (strMAP == null || strMAP.isEmpty())
			strMAP = "*O*";

		try {
			MAPFactory factory = new MAPFactory(strMAP);
			return factory.getMAPGraph();
		} catch (WURCSFormatException e) {
			logger.error("The MAP has error: "+strMAP, e);
		}
		return null;
	}
}
