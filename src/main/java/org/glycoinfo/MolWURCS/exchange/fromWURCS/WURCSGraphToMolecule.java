package org.glycoinfo.MolWURCS.exchange.fromWURCS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.Vector2d;

import org.glycoinfo.MolWURCS.exchange.ConverterUtils;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.glycoinfo.MolWURCS.util.analysis.MoleculeNormalizer;
import org.glycoinfo.MolWURCS.util.analysis.StereochemistryUtils;
import org.glycoinfo.MolWURCS.util.cdk.ChemObjectProvider;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.DirectionDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor.ModificationType;
import org.glycoinfo.WURCSFramework.wurcs.graph.LinkagePosition;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IBond.Stereo;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry.Conformation;
import org.openscience.cdk.interfaces.IStereoElement;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.stereo.DoubleBondStereochemistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WURCSGraphToMolecule {

	private Logger logger = LoggerFactory.getLogger(WURCSGraphToMolecule.class);

	private static class CarbonUnitInfo {
		private CarbonUnit m_cu;
		private ICarbonDescriptor m_cd;

		private List<BondInfo> m_lBondInfo;

		public CarbonUnitInfo(CarbonUnit a_cu, ICarbonDescriptor a_cd) {
			this.m_cu = a_cu;
			this.m_cd = a_cd;
			this.m_lBondInfo = new ArrayList<>();
		}

		public CarbonUnit getCarbonUnit() {
			return this.m_cu;
		}

		public ICarbonDescriptor getCarbonDescriptor() {
			return this.m_cd;
		}

		public void addBondInfo(BondInfo a_info) {
			if ( a_info == null )
				return;
			this.m_lBondInfo.add(a_info);
		}

		public List<BondInfo> getBondInfo() {
			return this.m_lBondInfo;
		}
	}

	private static class BondInfo {
		private IAtom m_atomStarOrig;
		private IBond.Order m_orderMod;
		private IAtom m_atomMod;
		private DirectionDescriptor m_dd;
		private boolean m_bIsRing;
		private ModChain m_mc;

		public static final BondInfo OH =
				new BondInfo(IBond.Order.SINGLE, ChemObjectProvider.createAtom(8), null);

		private BondInfo(IBond.Order a_order, IAtom a_atom, DirectionDescriptor a_dd) {
			this.m_atomStarOrig = null;
			this.m_orderMod = a_order;
			this.m_atomMod = a_atom;
			this.m_dd = a_dd;
			this.m_mc = null;
		}

		public BondInfo(IBond a_bond, boolean a_bIsReverse, DirectionDescriptor a_dd, ModChain a_mc, boolean a_bIsRing) {
			this.m_atomStarOrig = (a_bIsReverse)? a_bond.getEnd() : a_bond.getBegin();
			this.m_orderMod = a_bond.getOrder();
			this.m_atomMod = (a_bIsReverse)? a_bond.getBegin() : a_bond.getEnd();
			this.m_dd = a_dd;
			this.m_mc = a_mc;
			this.m_bIsRing = a_bIsRing;
		}

		public IAtom getStarOriginal() {
			return this.m_atomStarOrig;
		}

		public IBond.Order getOrder() {
			return this.m_orderMod;
		}

		public IAtom getModAtom() {
			return this.m_atomMod;
		}

		public DirectionDescriptor getDirection() {
			return this.m_dd;
		}

		public boolean isBondToRingOnAnomer() {
			return this.m_bIsRing;
		}

		public ModChain getModChain() {
			return this.m_mc;
		}

		public String toString() {
			return "bond: "+this.m_atomMod.getSymbol()+", is ring: "+this.m_bIsRing;
		}
	}

	private IAtomContainer m_mol;

	public WURCSGraphToMolecule() {}

	public IAtomContainer getMolecule() {
		return this.m_mol;
	}

	public void start(WURCSGraph a_graph) {
		// Create carbon chains from backbones
		Map<Backbone, CarbonChain> mapBBToCC = new HashMap<>();
		for ( Backbone t_bb : a_graph.getBackbones() )
			mapBBToCC.put(t_bb, BackboneToCarbonChain.convert(t_bb));

		// Create sub graphs from modifications
		Map<Modification, ModChain> mapMToMC = new HashMap<>();
		for ( Modification t_mod : a_graph.getModifications() )
			mapMToMC.put(t_mod, ModificationToModChain.convert(t_mod));

		// Extract modification bonds and map to backbone carbons
		Map<CarbonUnit, CarbonUnitInfo> mapCUToInfo = new HashMap<>();
		Set<IBond> lUsedModBonds = new HashSet<>();
		for ( Backbone t_bb : a_graph.getBackbones() ) {
			CarbonChain t_cc = mapBBToCC.get(t_bb);

			for ( WURCSEdge t_edge : t_bb.getEdges() ) {
				Modification mod = t_edge.getModification();
				ModChain t_mc = mapMToMC.get(mod);

				// Connect backbone and modification
				LinkagePosition t_lip = t_edge.getLinkages().getFirst();

				int iBPos = t_lip.getBackbonePosition();
				CarbonUnit unit = t_cc.getCarbonUnit(iBPos);
				ICarbonDescriptor cd = t_bb.getBackboneCarbons().get(iBPos-1).getDescriptor();

				if ( !mapCUToInfo.containsKey(unit) )
					mapCUToInfo.put(unit, new CarbonUnitInfo(unit, cd));
				CarbonUnitInfo cuInfo = mapCUToInfo.get(unit);

				int iMPos = t_lip.getModificationPosition();
				List<IBond> lModBonds = t_mc.getBondsFromStar(iMPos);

				IBond bondToMod = null;
				for ( IBond bond : lModBonds ) {
					if ( lUsedModBonds.contains(bond) )
						continue;
					bondToMod = bond;
					break;
				}
				if ( bondToMod == null ) {
					logger.error("The carbon in the MAP cannot be specified: "+mod.getMAPCode()+" vs pos "+iMPos);
					continue;
				}
				boolean isReverse = ( t_mc.getStars().contains( bondToMod.getBegin() ) )?
						false : true;

				// Map DirectionDescriptor to modification bond
				BondInfo bInfo = new BondInfo(bondToMod, isReverse, t_lip.getDirection(), t_mc,
						cd.isAnomeric() && isRingModification(mod));
				// Map modification bond to CarbonUnit
				cuInfo.addBondInfo(bInfo);

				lUsedModBonds.add(bondToMod);
			}
		}
		// Replace modification bonds in CarbonUnit
		for ( CarbonUnit unit : mapCUToInfo.keySet() )
			connectModAtomsToCarbonUnit(mapCUToInfo.get(unit));

		// Add all atoms
		IAtomContainer mol = ChemObjectProvider.createAtomContainer();
		for ( CarbonChain cc : mapBBToCC.values() )
			mol.add( toAtomContainer(cc) );
		for ( ModChain mc : mapMToMC.values() )
			mol.add( mc.getAtomContainer() );

		// Set StereoElements built from CIP descriptor labeled on atoms and bonds
		List<IStereoElement> stereos = StereochemistryUtils.getStereoElementsFromLabeledCIPDescriptor(mol);
		for ( IStereoElement element : stereos )
			mol.addStereoElement(element);

		// Add implicit hydrogens
		MoleculeNormalizer molNorm = new MoleculeNormalizer();
		molNorm.setDoRemoveMetalAtoms(false);
		molNorm.setDoOmitCharge(false);
		molNorm.setDoRestoreCharge(true);
		molNorm.setDoCheckTypedAtom(true);
		if ( !molNorm.normalize(mol) ) {
			logger.error("Error in molecule normalization.");
			return;
		}

		// Generate 2d Coordinates
		generate2DCoordinate(mol);

		this.m_mol = mol;
	}

	// TODO: Tentative. Needs to implement on WURCSFramework
	private boolean isRingModification(Modification a_mod) {
		// False if glycosidic
		if ( a_mod.isGlycosidic() ) return false;
		LinkedList<WURCSEdge> edges = a_mod.getEdges();
		// False if not bridge (count of edge is not two)
		if ( edges.size() != 2 ) return false;
		return true;
	}

	private void connectModAtomsToCarbonUnit(CarbonUnitInfo a_cuInfo) {
		CarbonUnit unit = a_cuInfo.getCarbonUnit();
		ICarbonDescriptor cd = a_cuInfo.getCarbonDescriptor();

		List<BondInfo> lBInfo = a_cuInfo.getBondInfo();
		if ( needDirection(cd) ) {
			// Sort with direction
			Collections.sort(lBInfo, new Comparator<BondInfo>() {
				@Override
				public int compare(BondInfo bInfo1, BondInfo bInfo2) {
					int iDD1 = getModNumber(bInfo1.getDirection());
					int iDD2 = getModNumber(bInfo2.getDirection());
					return iDD1 - iDD2;
				}
				private int getModNumber(DirectionDescriptor dd) {
					switch(dd) {
					case U:
					case E:
						return 1;
					case D:
					case Z:
						return 2;
					case T:
						return 3;
					case X:
						return 4;
					default:
						return 5;
					}
				}
			});
			if ( cd.getHybridOrbital() == ICarbonDescriptor.SP3 )
				unit.setStereo(CarbonUnit.Stereo.S);
			else if ( cd.getHybridOrbital() == ICarbonDescriptor.SP2 ) {
				IBond bond = null;
				if ( unit.getPreviousBond() != null && unit.getPreviousBond().getOrder() == IBond.Order.DOUBLE )
					bond = unit.getPreviousBond();
				if ( unit.getNextBond() != null && unit.getNextBond().getOrder() == IBond.Order.DOUBLE )
					bond = unit.getNextBond();
				if ( bond != null )
					bond.setStereo(IBond.Stereo.E);
			}
		} else {
			// Add omitted OH if exists
			addOmittedHydroxylGroup(lBInfo, cd);
			// Sort bonds by the order and atom number
			Collections.sort(lBInfo, new Comparator<BondInfo>() {
				@Override
				public int compare(BondInfo info1, BondInfo info2) {
					// Prioritize ones have higher bond order.
					Order order1 = info1.getOrder();
					Order order2 = info2.getOrder();
					int iComp = order2.numeric() - order1.numeric();
					if ( iComp != 0 )
						return iComp;

					// Prioritize ones connecting to ring modification
					if (  info1.isBondToRingOnAnomer() && !info2.isBondToRingOnAnomer() )
						return -1;
					if ( !info1.isBondToRingOnAnomer() &&  info2.isBondToRingOnAnomer() )
						return 1;

					// Prioritize ones have larger atomic number.
					IAtom atom1 = info1.getModAtom();
					IAtom atom2 = info2.getModAtom();
					iComp = atom2.getAtomicNumber() - atom1.getAtomicNumber();
					return iComp;
				}
			});
		}

		int j = 0;
		for ( int i=0; i<3; i++ ) {
			ModificationType modTypeCD = cd.getModificationType(i+1);
			if ( modTypeCD == null )
				break;
			BondInfo bInfo = lBInfo.get(j);
			ModificationType modType = ConverterUtils.toModificationType(bInfo.getOrder(), bInfo.getModAtom());
			if ( modTypeCD != modType )
				continue;
			// Replace bond to modification
			if ( bInfo == BondInfo.OH ) {
				// Do nothing
			} else if ( i==0 ) {
				unit.setMod1(bInfo.getModAtom(), bInfo.getOrder());
				bInfo.getModChain().replaceStar(bInfo.getStarOriginal(), unit.getCenterAtom(), unit.getBondToMod1());
			} else if ( i==1 ) {
				unit.setMod2(bInfo.getModAtom(), bInfo.getOrder());
				bInfo.getModChain().replaceStar(bInfo.getStarOriginal(), unit.getCenterAtom(), unit.getBondToMod2());
			} else if ( i==2 ) {
				unit.setMod3(bInfo.getModAtom(), bInfo.getOrder());
				bInfo.getModChain().replaceStar(bInfo.getStarOriginal(), unit.getCenterAtom(), unit.getBondToMod3());
			}
			j++;
			if ( lBInfo.size() <= j )
				break;
		}

		// Set stereo
		if ( unit.getStereo() != null && !cd.isChiral() )
			return;
		String strStereo = cd.getStereo();
		if ( strStereo == null || strStereo.equals("X") )
			return;
		unit.setStereo( (strStereo.toUpperCase().equals("S"))?
				CarbonUnit.Stereo.S : CarbonUnit.Stereo.R
			);
	}

	private boolean needDirection(ICarbonDescriptor a_cd) {
		if ( a_cd.isAnomeric() )
			return false; // 'a'
		int nH = a_cd.getNumberOfHydrogens();
		int nMod = a_cd.getNumberOfModifications();
		int nUMod = a_cd.getNumberOfUniqueModifications();
		if ( nH > 0 ) {
			nMod -= nH;
			nUMod--;
		}
		if ( nMod != nUMod )
			return true; // 'M', 'c', 'C', 'N'(terminal)
		return false; // Other
	}

	private void addOmittedHydroxylGroup(List<BondInfo> lBInfo, ICarbonDescriptor a_cd) {
		if ( lBInfo.size() == (a_cd.getNumberOfModifications() - a_cd.getNumberOfHydrogens() ) )
			return;
		// Count modification type "-X"
		int countX1 = 0;
		for ( int i=0; i<a_cd.getNumberOfModifications(); i++ )
			if ( a_cd.getModificationType(i+1) == ICarbonDescriptor.X1 )
				countX1++;
		if ( countX1 < 2 )
			return;

		// Count how many omitted OH
		for ( BondInfo bInfo : lBInfo )
			if ( ConverterUtils.toModificationType(bInfo.getOrder(), bInfo.getModAtom())
					== ICarbonDescriptor.X1 )
				countX1--;
		// Add OH
		for ( int i=0; i<countX1; i++ )
			lBInfo.add(BondInfo.OH);
	}

	private IAtomContainer toAtomContainer(CarbonChain a_cc) {
		IAtomContainer chain = ChemObjectProvider.createAtomContainer();
		// Adds carbons first
		for ( CarbonUnit unit : a_cc.getCarbonUnits() ) {
			chain.addAtom( unit.getCenterAtom() );
			// Adds carbon connections first
			if ( unit.getPreviousBond() != null )
				chain.addBond(unit.getPreviousBond());
		}

		// Adds modifications
		for ( CarbonUnit unit : a_cc.getCarbonUnits() ) {
			List<IBond> lModBonds = new ArrayList<>();
			lModBonds.add(unit.getBondToMod1());
			lModBonds.add(unit.getBondToMod2());
			lModBonds.add(unit.getBondToMod3());
			for ( IBond bond : lModBonds ) {
				if ( bond == null )
					continue;
				chain.addAtom( bond.getOther(unit.getCenterAtom()) );
				chain.addBond( bond );
			}
		}

		// Add tetrahedral chirality
		for ( CarbonUnit unit : a_cc.getCarbonUnits() ) {
			ITetrahedralChirality chiral = unit.createTetrahedralChilality();
			if ( chiral == null )
				continue;
			chain.addStereoElement(chiral);
		}

		// Add double bond stereochemistry
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			if ( cu.getPreviousBond() == null )
				continue;
			IBond bondCC = cu.getPreviousBond();
			if ( bondCC.getOrder() != Order.DOUBLE )
				continue;
			Conformation dbConf = null;
			if ( bondCC.getStereo() == Stereo.E )
				dbConf = Conformation.OPPOSITE;
			if ( bondCC.getStereo() == Stereo.Z )
				dbConf = Conformation.TOGETHER;
			if ( dbConf == null )
				continue;

			CarbonUnit cuPrev = cu.getPreviousUnit();
			IBond bondPrev = ( cuPrev.isTerminal() )?
					cuPrev.getBondToMod1() : cuPrev.getPreviousBond();
			IBond bondNext = ( cu.isTerminal() )?
					cu.getBondToMod1() : cu.getNextBond();

			IBond[] ligandBonds = new IBond[2];
			ligandBonds[0] = bondPrev;
			ligandBonds[1] = bondNext;

			IDoubleBondStereochemistry dbStereo = new DoubleBondStereochemistry(
					bondCC, ligandBonds, dbConf);

			chain.addStereoElement(dbStereo);
		}

		return chain;
	}

	private void generate2DCoordinate(IAtomContainer a_mol) {
		StructureDiagramGenerator sdg = new StructureDiagramGenerator(a_mol);
		try {
			sdg.generateCoordinates(new Vector2d(0, 1));
		} catch (CDKException e) {
			logger.error("Something wrong in 2D coordinate generation.", e);
		}
	}
}
