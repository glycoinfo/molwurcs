package org.glycoinfo.MolWURCS.exchange.toWURCS.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.MolWURCS.exchange.toWURCS.MoleculeToWURCSGraph;
import org.glycoinfo.MolWURCS.exchange.toWURCS.report.ExtractionReport.Report;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

public class MoleculeToWURCSGraphForReport extends MoleculeToWURCSGraph {

	private ExtractionReport.Entry m_report;

	private CarbonChainExtractorForReport m_ccExtractor;
	private ModChainExtractorForReport m_mcExtractor;
	private Map<IAtomContainer, ExtractionReport.Molecule> m_mapMolToReport;
	private Map<WURCSGraph, ExtractionReport.Molecule> m_mapGraphToReport;
	private Map<Modification, ExtractionReport.ModAtomGroup> m_mapModToReport;

	public MoleculeToWURCSGraphForReport() {
		this.m_report = new ExtractionReport.Entry();

		this.m_ccExtractor = null;
		this.m_mcExtractor = null;
		this.m_mapMolToReport = new HashMap<>();
		this.m_mapGraphToReport = new HashMap<>();
		this.m_mapModToReport = new HashMap<>();
	}

	public ExtractionReport.Entry getReport() {
		return this.m_report;
	}

	@Override
	public List<WURCSGraph> start(IAtomContainer a_mol, String a_strTitle) throws WURCSException {
		this.m_report.setTitle(a_strTitle);

		// Calculate mass
		this.m_report.setMass( AtomContainerManipulator.getMass(a_mol) );

		List<WURCSGraph> graphs = super.start(a_mol, a_strTitle);

		if ( !graphs.isEmpty() )
			this.m_report.setHasGlycan(true);

		return graphs;
	}

	@Override
	protected List<IAtomContainer> normalizeMolecule(IAtomContainer a_mol) {
		List<IAtomContainer> mols = super.normalizeMolecule(a_mol);
		if ( mols == null ) {
			this.m_report.setHasNormalizationError(true);
			return null;
		}

		this.m_report.setMoleculeCount(mols.size());
		for ( IAtomContainer mol : mols ) {
			ExtractionReport.Molecule report = new ExtractionReport.Molecule();
			this.m_report.addChild(report);
			this.m_mapMolToReport.put(mol, report);
		}

		return mols;
	}

	@Override
	protected WURCSGraph convert(IAtomContainer a_mol) throws WURCSException {
		WURCSGraph graph = super.convert(a_mol);

		ExtractionReport.Molecule report = this.m_mapMolToReport.get(a_mol);

		// Count carbon groups without condition match
		int nCarbonGroups = 0;
		int nTooSmall = 0;
		int nTooLarge = 0;
		int nHasTooManyBranches = 0;
		int nHasConnToCCyclic = 0;
		int nHasConnToPiCyclic = 0;
		int nHasTooManyModifications = 0;
		for ( Report repo : report.reportChildren ) {
			if ( !(repo instanceof ExtractionReport.CarbonGroup) )
				continue;
			ExtractionReport.CarbonGroup repoCG = (ExtractionReport.CarbonGroup)repo;
			nCarbonGroups++;
			if ( repoCG.bIsTooSmall )
				nTooSmall++;
			if ( repoCG.bIsTooLarge )
				nTooLarge++;
			if ( repoCG.bHasTooManyBranches )
				nHasTooManyBranches++;
			if ( repoCG.bHasConnToCCyclic )
				nHasConnToCCyclic++;
			if ( repoCG.bHasTooManyModifications )
				nHasTooManyModifications++;
			if ( repoCG.bHasConnToPiCyclic )
				nHasConnToPiCyclic++;
		}
		report.setCarbonChainCount(nCarbonGroups);
		report.setTooSmallCount(nTooSmall);
		report.setTooLargeCount(nTooLarge);
		report.setHasTooManyBranchesCount(nHasTooManyBranches);
		report.setHasConnectionToCarbonCyclicCount(nHasConnToCCyclic);
		report.setHasConnectionToPiCyclicCount(nHasConnToPiCyclic);
		report.setHasTooManyModificationsCount(nHasTooManyModifications);

		if ( graph == null )
			return graph;

		report.setHasGlycan(true);
		// Map report to graph
		this.m_mapGraphToReport.put(graph, report);

		return graph;
	}

	@Override
	protected List<CarbonChain> extractCarbonChain(IAtomContainer a_mol) {
		this.m_ccExtractor = new CarbonChainExtractorForReport(this.m_mapMolToReport.get(a_mol));
		return this.m_ccExtractor.start(a_mol);
	}

	@Override
	protected Backbone convertCarbonChainToBackbone(CarbonChain a_cc) {
		Backbone bb = super.convertCarbonChainToBackbone(a_cc);
		if ( bb == null )
			return null;
		this.m_ccExtractor.getReport(a_cc).setSkeletonCode(bb.getSkeletonCode());
		return bb;
	}

	protected Modification convertModChainToModification(ModChain a_mc) {
		Modification mod = super.convertModChainToModification(a_mc);
		ExtractionReport.ModAtomGroup report = this.m_mcExtractor.getReport(a_mc);
		report.setMAP( mod.getMAPCode() );
		report.setIsOmittable( mod.canOmitMAP() );
		this.m_mapModToReport.put(mod, report);
		return mod;
	}

	@Override
	protected List<ModChain> extractModChain(IAtomContainer a_mol, List<CarbonChain> a_lCarbonChains, boolean a_bDoFilter) {
		this.m_mcExtractor = new ModChainExtractorForReport(this.m_mapMolToReport.get(a_mol));
		return this.m_mcExtractor.start(a_mol, a_lCarbonChains, a_bDoFilter);
	}

	@Override
	protected List<WURCSGraph> separateGraph(WURCSGraph a_graph) throws WURCSException {
		List<WURCSGraph> graphs = super.separateGraph(a_graph);
		// Map separated graphs to report
		if ( !a_graph.equals( graphs.get(0) ) ) {
			ExtractionReport.Molecule report = this.m_mapGraphToReport.get(a_graph);
			for ( WURCSGraph graph : graphs )
				this.m_mapGraphToReport.put(graph, report);
		}
		return graphs;
	}

	@Override
	protected List<WURCSGraph> separateWithAglycone(WURCSGraph a_graph) throws WURCSException {
		List<WURCSGraph> graphs = super.separateWithAglycone(a_graph);
		if ( !a_graph.equals( graphs.get(0) ) ) {
			ExtractionReport.Molecule report = this.m_mapGraphToReport.get(a_graph);
			report.setHasAglycone(true);
			report.setSeparationCount(graphs.size());
			// TODO: update aglycone check
			for ( Modification mod : a_graph.getModifications() )
				if ( mod.isAglycone() ) {
					ExtractionReport.ModAtomGroup reportM = this.m_mapModToReport.get(mod);
					reportM.setIsReducingEnd(true);
					reportM.setIsRemained(false);
				}
		}
		return graphs;
	}

}
