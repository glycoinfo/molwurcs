package org.glycoinfo.MolWURCS.exchange.toWURCS.report;

import org.glycoinfo.MolWURCS.exchange.toWURCS.CarbonChainFilter;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.util.analysis.AtomChainExtractor.AtomChain;
import org.openscience.cdk.interfaces.IAtomContainer;

public class CarbonChainFilterForReport extends CarbonChainFilter {

	private CarbonChainExtractorForReport m_parent;

	public CarbonChainFilterForReport(CarbonChainExtractorForReport a_parent) {
		super();

		this.m_parent = a_parent;
	}

	@Override
	protected boolean checkWithAtomGroupCondition(IAtomContainer a_molCGrp, int a_nAtoms, int a_nBranches) {
		ExtractionReport.CarbonGroup report = this.m_parent.getReport(a_molCGrp);
		report.setAtomCount(a_nAtoms);
		report.setBranchCount(a_nBranches);
		return super.checkWithAtomGroupCondition(a_molCGrp, a_nAtoms, a_nBranches);
	}

	@Override
	protected boolean checkMinimumAtomCount(IAtomContainer a_molCGrp, int a_nAtoms) {
		if ( super.checkMinimumAtomCount(a_molCGrp, a_nAtoms) ) {
			this.m_parent.getReport(a_molCGrp).setIsTooSmall(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkMaximumAtomCount(IAtomContainer a_molCGrp, int a_nAtoms) {
		if ( super.checkMaximumAtomCount(a_molCGrp, a_nAtoms) ) {
			this.m_parent.getReport(a_molCGrp).setIsTooLarge(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkBranchCount(IAtomContainer a_molCGrp, int a_nBranches) {
		if ( super.checkBranchCount(a_molCGrp, a_nBranches) ) {
			this.m_parent.getReport(a_molCGrp).setHasTooManyBranches(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkConnectionToCarbonCyclic(IAtomContainer a_molCGrp, IAtomContainer a_mol) {
		if ( super.checkConnectionToCarbonCyclic(a_molCGrp, a_mol) ) {
			this.m_parent.getReport(a_molCGrp).setHasConnectionToCarbonCyclic(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkConnectionToPiCyclic(IAtomContainer a_molCGrp, IAtomContainer a_mol) {
		if ( super.checkConnectionToPiCyclic(a_molCGrp, a_mol) ) {
			this.m_parent.getReport(a_molCGrp).setHasConnectionToPiCyclic(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkWithAtomChainCondition(AtomChain a_chain, int a_nLength) {
		ExtractionReport.CarbonChain report = this.m_parent.getReport(a_chain);
		report.setLength(a_nLength);
		return super.checkWithAtomChainCondition(a_chain, a_nLength);
	}

	@Override
	protected boolean checkMinimumChainLength(AtomChain a_chain, int a_nLength) {
		if ( super.checkMinimumChainLength(a_chain, a_nLength) ) {
			this.m_parent.getReport(a_chain).setIsTooShort(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkMaximumChainLength(AtomChain a_chain, int a_nLength) {
		if ( super.checkMaximumChainLength(a_chain, a_nLength) ) {
			this.m_parent.getReport(a_chain).setIsTooLong(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkModificationCount(CarbonChain a_cc, int a_nMod, int a_nThreshold) {
		ExtractionReport.CarbonChain report = this.m_parent.getReport(a_cc);
		report.setModificationCount(a_nMod);
		report.setThreshold(a_nThreshold);
		if ( super.checkModificationCount(a_cc, a_nMod, a_nThreshold) ) {
			report.setIsExceededThreshold(true);
			return true;
		}
		// Add some info to CarbonGroup only for main chain
		if ( report.reportParent instanceof ExtractionReport.CarbonGroup )
			((ExtractionReport.CarbonGroup)report.reportParent).setHasTooManyModifications(false);
		return false;
	}

	@Override
	protected boolean checkSmallCyclicEther(CarbonChain a_cc) {
		if ( super.checkSmallCyclicEther(a_cc) ) {
			this.m_parent.getReport(a_cc).setHasSmallCyclic(true);
			return true;
		}
		return false;
	}
	
}
