package org.glycoinfo.MolWURCS.exchange.toWURCS.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.MolWURCS.exchange.toWURCS.CarbonChainBuilder;
import org.glycoinfo.MolWURCS.exchange.toWURCS.CarbonChainExtractor;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.util.analysis.AtomChainExtractor.AtomChain;
import org.openscience.cdk.interfaces.IAtomContainer;

public class CarbonChainExtractorForReport extends CarbonChainExtractor {

	private ExtractionReport.Molecule m_report;

	private Map<IAtomContainer, ExtractionReport.CarbonGroup> m_mapCGrpToReport;
	private Map<AtomChain, ExtractionReport.CarbonChain> m_mapACToReport;
	private Map<CarbonChain, ExtractionReport.CarbonChain> m_mapCCToReport;

	public CarbonChainExtractorForReport(ExtractionReport.Molecule a_report) {
		this.m_report = a_report;

		this.m_mapCGrpToReport = new HashMap<>();
		this.m_mapACToReport = new HashMap<>();
		this.m_mapCCToReport = new HashMap<>();
	}

	@Override
	public List<CarbonChain> start(IAtomContainer a_mol) {
		List<CarbonChain> lCCs = super.start(a_mol);
		for ( CarbonChain cc : lCCs )
			this.getReport(cc).setIsExtracted(true);
		return lCCs;
	}

	@Override
	protected CarbonChainFilterForReport createNewFilter() {
		return new CarbonChainFilterForReport(this);
	}

	@Override
	protected List<IAtomContainer> filterCarbonGroups(List<IAtomContainer> a_lCGroups, IAtomContainer a_mol) {
		// Create and map reports for carbon group
		for ( IAtomContainer group : a_lCGroups ) {
			ExtractionReport.CarbonGroup report = new ExtractionReport.CarbonGroup();
			this.m_report.addChild(report);
			this.m_mapCGrpToReport.put(group, report);
		}

		return super.filterCarbonGroups(a_lCGroups, a_mol);
	}

	@Override
	protected List<AtomChain> filterCandidateCarbonChains(List<AtomChain> a_chains, IAtomContainer a_group) {
		// Create and map reports for carbon chains
		for ( AtomChain chain : a_chains ) {
			ExtractionReport.CarbonChain report = new ExtractionReport.CarbonChain();
			this.getReport(a_group).addChild(report);
			this.m_mapACToReport.put(chain, report);
			mapSubChains(chain);
		}
		// Init too many mods flag 
		this.getReport(a_group).setHasTooManyModifications(true);

		return super.filterCandidateCarbonChains(a_chains, a_group);
	}

	private void mapSubChains(AtomChain a_chain) {
		for ( AtomChain chainSub : a_chain.getSubChains() ) {
			ExtractionReport.CarbonChain report = new ExtractionReport.CarbonChain();
			this.getReport(a_chain).addChild(report);
			this.m_mapACToReport.put(chainSub, report);
			mapSubChains(chainSub);
		}
	}

	@Override
	protected CarbonChain convertCarbonChain(AtomChain a_chain, CarbonChainBuilder a_ccBuilder) {
		CarbonChain cc = super.convertCarbonChain(a_chain, a_ccBuilder);
		// Map report for converted carbon chain
		this.m_mapCCToReport.put(cc, this.getReport(a_chain));

		return cc;
	}

	ExtractionReport.CarbonGroup getReport(IAtomContainer a_group) {
		return this.m_mapCGrpToReport.get(a_group);
	}

	ExtractionReport.CarbonChain getReport(AtomChain a_chain) {
		return this.m_mapACToReport.get(a_chain);
	}

	ExtractionReport.CarbonChain getReport(CarbonChain a_chain) {
		return this.m_mapCCToReport.get(a_chain);
	}

}
