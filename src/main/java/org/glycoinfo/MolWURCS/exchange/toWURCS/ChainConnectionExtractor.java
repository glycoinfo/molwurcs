package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.exchange.ConverterUtils;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.om.buildingblock.ChainConnection;
import org.glycoinfo.MolWURCS.om.buildingblock.ChainConnection.DirectionType;
import org.glycoinfo.MolWURCS.util.analysis.CarbonChainUtils;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IBond.Stereo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChainConnectionExtractor {

	public static final Logger logger = LoggerFactory.getLogger(ChainConnectionExtractor.class);

	public static List<ChainConnection> start(List<CarbonChain> a_lCC, List<ModChain> a_lMC) {
		List<ChainConnection> lCConns = new ArrayList<>();
		for ( CarbonChain cc : a_lCC ) {
			for ( ModChain mc : a_lMC ) {
				lCConns.addAll( buildChainConnections(cc, mc) );
			}
		}
		return lCConns;
	}

	public static List<ChainConnection> buildChainConnections(CarbonChain a_cc, ModChain a_mc) {
		List<ChainConnection> lCConns = new ArrayList<>();
		// Check if connection
		int iAnomPos = 0;
		int nCarbons = a_cc.getCarbonUnits().size();
		for ( int i=1; i<=nCarbons; i++ ) {
			CarbonUnit cu = a_cc.getCarbonUnit(i);
			if ( !a_mc.getStars().contains(cu.getCenterAtom()) )
				continue;

			// TODO: reconsider for direction on anomer
			boolean isAnomeric = CarbonChainUtils.isAnomeric(cu, a_cc);
			isAnomeric &= (iAnomPos == 0);
			if ( isAnomeric )
				iAnomPos = i;

			// Check each modification bond
			for ( int iModBondID=1; iModBondID<=3; iModBondID++ ) {
				IBond bondMod =
					(iModBondID == 1)? cu.getBondToMod1() :
					(iModBondID == 2)? cu.getBondToMod2() :
					(iModBondID == 3)? cu.getBondToMod3() : null;

				if ( bondMod == null )
					continue;
				// TODO: check atoms only connecting star atom
				if ( !a_mc.getAtomContainer().contains( bondMod.getEnd() ) )
					continue;
				DirectionType dType = toDirectionType(bondMod, cu, isAnomeric);
				if ( dType == null )
					logger.error("Direction type cannot be specified.");

				IBond bondFromStar = null;
				for ( IBond bond : a_mc.getBondsFromStar() ) {
					if ( !bond.contains( bondMod.getEnd() ) )
						continue;
					if ( !bond.contains( cu.getCenterAtom() ) )
						continue;
					bondFromStar = bond;
					break;
				}

				int iStarIndex = a_mc.getStarIndex(bondFromStar);

				ChainConnection cconn = new ChainConnection(a_cc, a_mc, i, iModBondID, dType, iStarIndex);
				lCConns.add(cconn);
			}
		}
		return lCConns;
	}

	private static DirectionType toDirectionType(IBond a_bondMod, CarbonUnit a_cu, boolean a_bIsAnomeric) {
		// Check direction type
		if ( a_bIsAnomeric || isUnique(a_bondMod, a_cu.getModBonds()) )
			return ChainConnection.DirectionType.NONE;
		if (a_cu.getStereo() != null) {
			switch (a_cu.getStereo()) {
			case R:
				return ChainConnection.DirectionType.CHIRAL_ACC;
			case S:
				return ChainConnection.DirectionType.CHIRAL_CC;
			case X:
				return ChainConnection.DirectionType.UNKNOWN;
			}
			return null;
		} 
		IBond.Stereo stereo = getChainDBStereo(a_cu);
		if ( stereo == null )
			return null;
		switch (stereo) {
		case E:
			return ChainConnection.DirectionType.DBSTEREO_E;
		case Z:
			return ChainConnection.DirectionType.DBSTEREO_Z;
		case E_OR_Z:
			return ChainConnection.DirectionType.UNKNOWN;
		case NONE:
			return ChainConnection.DirectionType.NONE;
		default:
			break;
		}
		return null;
	}

	private static boolean isUnique(IBond a_bondMod, List<IBond> a_lModBonds) {
		// Check the direction of the bond can be omitted
		String strUMod = ConverterUtils.toUniqueMod(a_bondMod);
		for ( IBond bondModOther : a_lModBonds ) {
			if ( bondModOther.equals(a_bondMod) )
				continue;
			String strUModOther = ConverterUtils.toUniqueMod(bondModOther);
			if ( !strUMod.equals(strUModOther) )
				continue;
			return false;
		}
		return true;
	}

	private static Stereo getChainDBStereo(CarbonUnit a_cu) {
		if ( a_cu.getPreviousBond() != null && a_cu.getPreviousBond().getOrder() == Order.DOUBLE )
			return a_cu.getPreviousBond().getStereo();
		if ( a_cu.getNextBond() != null && a_cu.getNextBond().getOrder() == Order.DOUBLE )
			return a_cu.getNextBond().getStereo();
		return null;
	}
}
