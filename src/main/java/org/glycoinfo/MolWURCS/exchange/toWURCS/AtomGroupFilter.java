package org.glycoinfo.MolWURCS.exchange.toWURCS;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

public abstract class AtomGroupFilter {

	private int m_iMinAtomCount;
	private int m_iMaxAtomCount;
	private int m_iMaxBranchCount;

	public AtomGroupFilter() {
		this.m_iMinAtomCount = -1;
		this.m_iMaxAtomCount = -1;
		this.m_iMaxBranchCount = -1;
	}

	// Setters for atom group filter

	public void setMinimumAtomCount(int a_iAtomCount) {
		this.m_iMinAtomCount = a_iAtomCount;
	}

	public void setMaximumAtomCount(int a_iAtomCount) {
		this.m_iMaxAtomCount = a_iAtomCount;
	}

	public void setMaximumBranchCount(int a_iBranchCount) {
		this.m_iMaxBranchCount = a_iBranchCount;
	}

	/**
	 * Returns {@code true} if the given atom group satisfies following requirements:
	 * # of atoms is within the min and max threshold,
	 * # of branches is lower than max threshold,
	 * @param a_molGrp IAtomContainer of target carbon group
	 * @return {@code true} if the given atom group satisfies requirements
	 */
	public boolean checkWithAtomGroupCondition(IAtomContainer a_molGrp) {
		// Count atoms
		int nAtoms = a_molGrp.getAtomCount();

		// Count branches
		int nBranches = 0;
		for ( IAtom atom : a_molGrp.atoms() ) {
			int nConn = a_molGrp.getConnectedBondsCount(atom);
			if ( nConn > 2 )
				nBranches += nConn - 2;
		}

		return this.checkWithAtomGroupCondition(a_molGrp, nAtoms, nBranches);
	}

	protected boolean checkWithAtomGroupCondition(IAtomContainer a_molGrp, int a_nAtoms, int a_nBranches) {
		boolean bCanBeCandidate = true;
		bCanBeCandidate &= !this.checkMinimumAtomCount(a_molGrp, a_nAtoms); // if not nAtoms < nMinAtoms
		bCanBeCandidate &= !this.checkMaximumAtomCount(a_molGrp, a_nAtoms); // if not nAtoms > nMaxAtoms
		bCanBeCandidate &= !this.checkBranchCount(a_molGrp, a_nBranches);   // if not nBranches > nMaxBranches
		return bCanBeCandidate;
	}

	protected boolean checkMinimumAtomCount(IAtomContainer a_molCGrp, int a_nAtoms) {
		return this.m_iMinAtomCount >= 0 && a_nAtoms < this.m_iMinAtomCount;
	}

	protected boolean checkMaximumAtomCount(IAtomContainer a_molCGrp, int a_nAtoms) {
		return this.m_iMaxAtomCount > 0 && a_nAtoms > this.m_iMaxAtomCount;
	}

	protected boolean checkBranchCount(IAtomContainer a_molCGrp, int a_nBranches) {
		return this.m_iMaxBranchCount >= 0 && a_nBranches > this.m_iMaxBranchCount;
	}
}
