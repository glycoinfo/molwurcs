package org.glycoinfo.MolWURCS.exchange.toWURCS.report;

import org.glycoinfo.MolWURCS.exchange.toWURCS.ModChainFilter;
import org.openscience.cdk.interfaces.IAtomContainer;

public class ModChainFilterForReport extends ModChainFilter {

	private ModChainExtractorForReport m_parent;

	public ModChainFilterForReport(ModChainExtractorForReport a_parent) {
		super();

		this.m_parent = a_parent;
	}

	@Override
	protected boolean checkWithAtomGroupCondition(IAtomContainer a_molMGrp, int a_nAtoms, int a_nBranches) {
		ExtractionReport.ModAtomGroup report = this.m_parent.getReport(a_molMGrp);
		report.setAtomCount(a_nAtoms);
		report.setBranchCount(a_nBranches);
		return super.checkWithAtomGroupCondition(a_molMGrp, a_nAtoms, a_nBranches);
	}

	@Override
	protected boolean checkMinimumAtomCount(IAtomContainer a_molMGrp, int a_nAtoms) {
		if ( super.checkMinimumAtomCount(a_molMGrp, a_nAtoms) ) {
			this.m_parent.getReport(a_molMGrp).setIsTooSmall(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkMaximumAtomCount(IAtomContainer a_molMGrp, int a_nAtoms) {
		if ( super.checkMaximumAtomCount(a_molMGrp, a_nAtoms) ) {
			this.m_parent.getReport(a_molMGrp).setIsTooLarge(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkBranchCount(IAtomContainer a_molMGrp, int a_nBranches) {
		if ( super.checkBranchCount(a_molMGrp, a_nBranches) ) {
			this.m_parent.getReport(a_molMGrp).setHasTooManyBranches(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkAllowedAtoms(IAtomContainer a_molGrp) {
		if ( super.checkAllowedAtoms(a_molGrp) ) {
			this.m_parent.getReport(a_molGrp).setContainsDisallowedAtoms(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkRingCount(IAtomContainer a_molGrp, int a_nRingCount) {
		this.m_parent.getReport(a_molGrp).setRingCount(a_nRingCount);
		if ( super.checkRingCount(a_molGrp, a_nRingCount) ) {
			this.m_parent.getReport(a_molGrp).setHasTooManyRings(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkRingSize(IAtomContainer a_molGrp, int a_iRingSizeMax) {
		this.m_parent.getReport(a_molGrp).setMaxRingSize(a_iRingSizeMax);
		if ( super.checkRingCount(a_molGrp, a_iRingSizeMax) ) {
			this.m_parent.getReport(a_molGrp).setHasTooLargeRing(true);
			return true;
		}
		return false;
	}

}
