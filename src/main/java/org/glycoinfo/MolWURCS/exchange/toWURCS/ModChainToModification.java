package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.HashMap;
import java.util.Map;

import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.glycoinfo.WURCSFramework.util.map.MAPFactory;
import org.glycoinfo.WURCSFramework.util.map.analysis.MAPGraphNormalizer;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtom;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPAtomAbstract;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPBondType;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPConnection;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPGraph;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPStar;
import org.glycoinfo.WURCSFramework.wurcs.map.MAPStereo;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

public class ModChainToModification {

	public static Modification convert(ModChain a_mc) {
		// Convert ModChain to MAPGraph
		MAPGraph graph = toMAPGraph(a_mc);
		// Do not normalize again
		MAPFactory factory = new MAPFactory(graph);
		String strMAP = factory.getMAPString();

		Modification mod = new Modification(strMAP);
		return mod;
	}

	private static MAPGraph toMAPGraph(ModChain a_mc) {
		MAPGraph graph = new MAPGraph();

		Map<IBond, MAPStar> mapBondToMAPStar = new HashMap<>();
		Map<IAtom, MAPAtomAbstract> mapAtomToMAPAtom = new HashMap<>();
		IAtomContainer mol = a_mc.getAtomContainer();

		// Add all atoms to MAPGraph
		for ( IAtom atom : mol.atoms() ) {
			MAPAtom mapAtom = new MAPAtom(atom.getSymbol());
			if ( atom.isAromatic() )
				mapAtom.setAromatic();
			graph.addAtom(mapAtom);
			mapAtomToMAPAtom.put(atom, mapAtom);

			// Check stereo
			if ( atom.getProperty(CDKConstants.CIP_DESCRIPTOR) == null )
				continue;
			// Set chirality
			String strCIP = atom.getProperty(CDKConstants.CIP_DESCRIPTOR);
			MAPStereo stereo = toMAPStereo(strCIP);
			mapAtom.setStereo(stereo);
		}
		Map<MAPStar, MAPAtomAbstract> mapStarToModAtom = new HashMap<>();
		for ( IBond bondFromStar : a_mc.getBondsFromStar() ) {
			MAPStar mapStar = new MAPStar();
			mapStar.setStarIndex(a_mc.getStarIndex(bondFromStar));
			graph.addAtom(mapStar);
			mapBondToMAPStar.put(bondFromStar, mapStar);
			IAtom atomMod = ( a_mc.getStars().contains( bondFromStar.getBegin() ) )?
					bondFromStar.getEnd() : bondFromStar.getBegin();
			mapStarToModAtom.put(mapStar, mapAtomToMAPAtom.get(atomMod));
		}

		// Add connections
		for ( IBond bond : mol.bonds() )
			createConnections(bond, mapAtomToMAPAtom);
		for ( IBond bond : a_mc.getBondsFromStar() )
			createConnections(bond, mapBondToMAPStar, mapStarToModAtom);
		// Set MAPStar connection
		for ( MAPStar mapStar : graph.getStars() )
			mapStar.setConnection( mapStar.getConnections().getFirst() );

		// Normalize MAPGraph
		MAPGraphNormalizer mapNorm = new MAPGraphNormalizer(graph);
		// Do not reset stereo
		mapNorm.setDoStereoReset(false);
		mapNorm.start();
		MAPGraph graphNorm = mapNorm.getNormalizedGraph();

		// reset StarIndices
		for ( IBond bondFromStar : a_mc.getBondsFromStar() ) {
			MAPStar mapStar = mapBondToMAPStar.get(bondFromStar);
			// Get new one
			mapStar = (MAPStar)mapNorm.getCopiedAtom(mapStar);
			a_mc.resetStarIndex(mapStar.getStarIndex(), bondFromStar);
		}

		return graphNorm;
	}

	private static MAPStereo toMAPStereo(String strCIP) {
		switch (strCIP) {
		case "R":
			return MAPStereo.RECTUS;
		case "S":
			return MAPStereo.SINISTER;
		case "E":
			return MAPStereo.TRANS;
		case "Z":
			return MAPStereo.CIS;
		case "X":
			return MAPStereo.UNKNOWN;
		default: // include case "NONE"
			return null;
		}
	}

	private static void createConnections(IBond bond,
			Map<IBond, MAPStar> mapBondToMAPStar,
			Map<MAPStar, MAPAtomAbstract> mapStarToModAtom) {

		MAPStar mapStar = mapBondToMAPStar.get(bond);
		MAPAtomAbstract mapAtomMod = mapStarToModAtom.get(mapStar);

		createConnections(bond, mapStar, mapAtomMod);
	}

	private static void createConnections(IBond bond,
			Map<IAtom, MAPAtomAbstract> mapAtomToMAPAtom) {

		IAtom atom1 = bond.getBegin();
		IAtom atom2 = bond.getEnd();

		MAPAtomAbstract mapAtom1 = mapAtomToMAPAtom.get(atom1);
		MAPAtomAbstract mapAtom2 = mapAtomToMAPAtom.get(atom2);

		createConnections(bond, mapAtom1, mapAtom2);
	}

	private static void createConnections(IBond bond,
			MAPAtomAbstract mapAtom1, MAPAtomAbstract mapAtom2
			) {
		
		MAPConnection mapConnFrom1 = new MAPConnection(mapAtom2);
		MAPConnection mapConnFrom2 = new MAPConnection(mapAtom1);
		mapConnFrom1.setReverse(mapConnFrom2);
		mapConnFrom2.setReverse(mapConnFrom1);

		MAPBondType type = toMAPBondType(bond);
		mapConnFrom1.setBondType(type);
		mapConnFrom2.setBondType(type);

		mapAtom1.addChildConnection(mapConnFrom1);
		mapAtom2.addChildConnection(mapConnFrom2);

		if ( bond.getProperty(CDKConstants.CIP_DESCRIPTOR) == null )
			return;
		String strCIP = bond.getProperty(CDKConstants.CIP_DESCRIPTOR);
		// Do not set unknown stereo on aromatic bond
		if ( mapAtom1.isAromatic() && mapAtom2.isAromatic() && strCIP.equals("X") )
			return;
		MAPStereo stereo = toMAPStereo(strCIP);
		mapConnFrom1.setStereo(stereo);
		mapConnFrom2.setStereo(stereo);
	}

	private static MAPBondType toMAPBondType(IBond bond) {
		switch (bond.getOrder()) {
		case SINGLE:
			return MAPBondType.SINGLE;
		case DOUBLE:
			return MAPBondType.DOUBLE;
		case TRIPLE:
			return MAPBondType.TRIPLE;
		default:
			return ( bond.isAromatic() )? MAPBondType.SINGLE : MAPBondType.UNKNOWN;
		}
	}
}
