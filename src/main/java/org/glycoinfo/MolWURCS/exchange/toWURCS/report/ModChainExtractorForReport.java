package org.glycoinfo.MolWURCS.exchange.toWURCS.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.MolWURCS.exchange.toWURCS.ModChainExtractor;
import org.glycoinfo.MolWURCS.exchange.toWURCS.ModChainFilter;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

public class ModChainExtractorForReport extends ModChainExtractor {

	private ExtractionReport.Molecule m_report;

	private Map<IAtomContainer, ExtractionReport.ModAtomGroup> m_mapMGrpToReport;
	private Map<ModChain, ExtractionReport.ModAtomGroup> m_mapMCToReport;

	public ModChainExtractorForReport(ExtractionReport.Molecule a_report) {
		this.m_report = a_report;

		this.m_mapMGrpToReport = new HashMap<>();
		this.m_mapMCToReport = new HashMap<>();
	}

	@Override
	protected ModChainFilter createNewFilter() {
		return new ModChainFilterForReport(this);
	}

	@Override
	public List<ModChain> start(IAtomContainer a_mol, List<CarbonChain> a_lCCs, boolean a_bDoFilter) {
		List<ModChain> lMCs = super.start(a_mol, a_lCCs, a_bDoFilter);
		for ( ModChain mc : lMCs )
			this.getReport(mc).setIsRemained(true);

		return lMCs;
	}

	@Override
	protected List<IAtomContainer> extractCandidateModificationGroup(IAtomContainer a_mol, Set<IAtom> a_setCCAtoms) {
		List<IAtomContainer> lModMols = super.extractCandidateModificationGroup(a_mol, a_setCCAtoms);

		// Create and map reports for modification atom group before filtering
		for ( IAtomContainer group : lModMols ) {
			ExtractionReport.ModAtomGroup report = new ExtractionReport.ModAtomGroup();
			this.m_report.addChild(report);
			this.m_mapMGrpToReport.put(group, report);
		}

		return lModMols;
	}

	@Override
	protected ModChain createModChain(IAtomContainer a_molMod) {
		ModChain mc = super.createModChain(a_molMod);
		// Map report to ModChain
		this.m_mapMCToReport.put(mc, this.getReport(a_molMod));
		return mc;
	}

	ExtractionReport.ModAtomGroup getReport(IAtomContainer a_group) {
		return this.m_mapMGrpToReport.get(a_group);
	}

	ExtractionReport.ModAtomGroup getReport(ModChain a_mc) {
		return this.m_mapMCToReport.get(a_mc);
	}
}
