package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.ChainConnection;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.glycoinfo.MolWURCS.util.analysis.MoleculeNormalizer;
import org.glycoinfo.MolWURCS.util.analysis.StereochemistryUtils;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.graph.WURCSGraphNormalizer;
import org.glycoinfo.WURCSFramework.util.graph.WURCSGraphSeparatorWithAglycone;
import org.glycoinfo.WURCSFramework.util.graph.WURCSGraphSeparatorWithAglycone.WURCSGraphForAglycone;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorCollectConnectingBackboneGroups;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoleculeToWURCSGraph {

	private static final Logger logger = LoggerFactory.getLogger(MoleculeToWURCSGraph.class);

	private boolean m_bOutWithAglycone;

	public MoleculeToWURCSGraph() {
		this.m_bOutWithAglycone = true;
	}

	public void setOutputWithAglycone(boolean a_bOutWithAglycone) {
		this.m_bOutWithAglycone = a_bOutWithAglycone;
	}

	public List<WURCSGraph> start(IAtomContainer a_mol) throws WURCSException {
		return start(a_mol, a_mol.getTitle());
	}

	/**
	 * Converts an input molecule to a list of WURCSGraph. After the molecular
	 * normalization, the molecule is separated into smaller molecules
	 * if they does not connect each other.
	 * Every molecule is processed to extract sugars.
	 * Extracted sugars are separated if the molecule can be separated by
	 * non-sugar moiety. After the processes, WURCSGraph(s) are generated
	 * if sugar(s) are detected.
	 * When the normalization and extraction process failed, returns empty list.
	 * @param a_mol Input molecule to be converted
	 * @param a_strTitle title of the input molecule
	 * @return List of WURCSGraph which 
	 * @throws WURCSException
	 */
	public List<WURCSGraph> start(IAtomContainer a_mol, String a_strTitle) throws WURCSException {
		logger.info("Start process for molecule: {}", a_strTitle);

		List<WURCSGraph> lGraphs = new ArrayList<>();

		// Normalize molecule
		List<IAtomContainer> mols = normalizeMolecule(a_mol);
		// Return empty if error in normalization
		if ( mols == null )
			return lGraphs;

		for ( IAtomContainer mol : mols ) {
			WURCSGraph graph = convert(mol);
			if ( graph == null )
				continue;

			// Separate graph with connecting residues
			List<WURCSGraph> graphs = separateGraph(graph);

			for ( WURCSGraph graphSep : graphs ) {
				// Return graphs with aglycone
				// TODO: Test options
				if ( this.m_bOutWithAglycone ) {
					lGraphs.add(graphSep);
					continue;
				}

				lGraphs.addAll( separateWithAglycone(graphSep) );
			}
		}
		if ( lGraphs.size() > 1 )
			logger.info("Input molecule is separated into {} parts in total: {}", lGraphs.size(), a_strTitle);

		return lGraphs;
	}

	protected List<IAtomContainer> normalizeMolecule(IAtomContainer a_mol) {
		MoleculeNormalizer norm = new MoleculeNormalizer();
		norm.setDoAromatize(true);
		if ( !norm.normalize(a_mol) )
			return null;

		List<IAtomContainer> mols = norm.getMolecules();
		if ( mols.isEmpty() )
			logger.warn("No atom in the molecule except for metal and hydrogen atoms.");

		return mols;
	}

	protected WURCSGraph convert(IAtomContainer a_mol) throws WURCSException {
		// Reset stereo manager
		StereoManager.clearOld();

		List<CarbonChain> lCarbonChains = extractCarbonChain(a_mol);
		if ( lCarbonChains.isEmpty() ) {
			logger.warn("No carbon chain in the molecule.");
			return null;
		}

		// Label stereochemistry for each atom or bond based on CIP rule
		StereochemistryUtils.labelCIPDescriptor(a_mol);

		// Map CarbonChain to converted Backbone
		Map<CarbonChain, Backbone> mapCCToB = new HashMap<>();
		for ( CarbonChain cc : lCarbonChains ) {
			Backbone bb = convertCarbonChainToBackbone(cc);
			if ( bb == null )
				return null;
			mapCCToB.put(cc, bb);
		}

		List<ModChain> lModChains = extractModChain(a_mol, lCarbonChains, !this.m_bOutWithAglycone);
		// Finish conversion when no modification
		if ( lModChains.isEmpty() ) {
			WURCSGraph graph = new WURCSGraph();
			for ( CarbonChain cc : lCarbonChains )
				graph.addBackbone( mapCCToB.get(cc) );
			WURCSGraphNormalizer wNorm = new WURCSGraphNormalizer();
			wNorm.start(graph);
			return graph;
		}

		// Map ModChains to converted Modification
		Map<ModChain, Modification> mapMCToM = new HashMap<>();
		for ( ModChain mc : lModChains )
			mapMCToM.put(mc, convertModChainToModification(mc));
		//TODO: Test mod chains

		List<ChainConnection> lCConns = ChainConnectionExtractor.start(lCarbonChains, lModChains);

		// Connect building blocks
		WURCSGraph graph = new WURCSGraph();
		for ( ChainConnection cconn : lCConns ) {
			Backbone b = mapCCToB.get( cconn.getCarbonChain() );
			Modification m = mapMCToM.get( cconn.getModChain() );

			WURCSEdge edge = ChainConnectionToWURCSEdge.convert(cconn);
			graph.addResidues(b, edge, m);
		}

		WURCSGraphNormalizer wNorm = new WURCSGraphNormalizer();
		wNorm.start(graph);

		return graph;
	}

	protected List<CarbonChain> extractCarbonChain(IAtomContainer a_mol) {
		return new CarbonChainExtractor().start(a_mol);
	}

	protected Backbone convertCarbonChainToBackbone(CarbonChain a_cc) {
		Backbone bb = CarbonChainToBackbone.convert(a_cc);
		for ( int i=0; i<bb.getLength(); i++ ) {
			if ( bb.getBackboneCarbons().get(i).getDescriptor().getChar() != '?' )
				continue;
			logger.error("The atom type \"{}\" is not defined as backbone carbon for now.",
					a_cc.getCarbonUnit(i+1).getCenterAtom().getAtomTypeName());
			return null;
		}
		return bb;
	}

	protected List<ModChain> extractModChain(IAtomContainer a_mol, List<CarbonChain> a_lCarbonChains, boolean a_bDoFilter) {
		return new ModChainExtractor().start(a_mol, a_lCarbonChains, a_bDoFilter);
	}

	protected Modification convertModChainToModification(ModChain a_mc) {
		return ModChainToModification.convert(a_mc);
	}

	/**
	 * TODO: move to WURCSFramework
	 * @param a_graph
	 * @return
	 * @throws WURCSException
	 */
	protected List<WURCSGraph> separateGraph(WURCSGraph a_graph) throws WURCSException {
		WURCSVisitorCollectConnectingBackboneGroups BBCollector = new WURCSVisitorCollectConnectingBackboneGroups();
		BBCollector.start(a_graph);
		List<HashSet<Backbone>> lBBGrps = BBCollector.getBackboneGroups();

		List<WURCSGraph> lGraphs = new ArrayList<>();
		if ( lBBGrps.size() == 1 ) {
			lGraphs.add(a_graph);
			return lGraphs;
		}

		for ( Set<Backbone> setBB : lBBGrps ) {
			WURCSGraph graphNew = new WURCSGraph();
			for ( Backbone bb : setBB ) {
				for ( WURCSEdge edge : bb.getEdges() )
					graphNew.addResidues(bb, edge, edge.getModification());
				if ( bb.getEdges().isEmpty() )
					graphNew.addBackbone(bb);
			}
			lGraphs.add(graphNew);
		}
		logger.info("A molecule is separated into {} connecting groups.", lGraphs.size());

		return lGraphs;
	}

	protected List<WURCSGraph> separateWithAglycone(WURCSGraph a_graph) throws WURCSException {
		List<WURCSGraph> lGraphs = new ArrayList<>();

		// Separate graph with aglycone
		WURCSGraphForAglycone graphA = WURCSGraphSeparatorWithAglycone.start(a_graph);
		if ( !graphA.isSeparated() ) {
			lGraphs.add(a_graph);
			return lGraphs;
		}

		WURCSGraphNormalizer norm = new WURCSGraphNormalizer();
		for ( WURCSGraph graphSep : graphA.getSeparatedGraphs() ) {
			norm.start(graphSep);
			lGraphs.add(graphSep);
		}

		// Log if input molecule is separated with aglycone
		if ( graphA.hasAglycone() )
			logger.info("A molecule is separated with aglycone into {} part(s).", lGraphs.size());

		return lGraphs;
	}
}
