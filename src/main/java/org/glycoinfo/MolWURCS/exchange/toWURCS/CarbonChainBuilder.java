package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.vecmath.Vector2d;

import org.glycoinfo.MolWURCS.exchange.ConverterUtils;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.openscience.cdk.geometry.GeometryUtil;
import org.openscience.cdk.geometry.cip.CIPTool;
import org.openscience.cdk.geometry.cip.ILigand;
import org.openscience.cdk.geometry.cip.VisitedAtoms;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomType.Hybridization;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry.Conformation;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.interfaces.ITetrahedralChirality.Stereo;
import org.openscience.cdk.stereo.DoubleBondStereochemistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarbonChainBuilder {

	private static Logger logger = LoggerFactory.getLogger(CarbonChainBuilder.class);

	private IAtomContainer m_mol;
	private StereoManager m_stereoManager;

	public CarbonChainBuilder(IAtomContainer a_mol) {
		this.m_mol = a_mol;
		this.m_stereoManager = StereoManager.of(a_mol);
	}

	/**
	 * Creates CarbonChain object from the specified carbon chain.
	 * All atoms in the carbon chain must be contained in the IAtomContainer specified at constructor.
	 * @param a_chain LinkedList of atoms indicating carbon chain
	 * @return CarbonChain converted from the specified carbon chain.
	 *  ({@code null} if the carbon chain is {@code null} or 
	 *  is not contained in the IAtomContainer specified at constructor)
	 */
	public CarbonChain createCarbonChain(LinkedList<IAtom> a_chain) {
		if ( a_chain == null )
			return null;

		CarbonChain cc = new CarbonChain();
		Map<CarbonUnit, IBond> mapUnitToPrevBondOrig = new HashMap<>();
		int nLength = a_chain.size();
		for ( int i=0; i<nLength; i++ ) {
			IAtom center = a_chain.get(i);
			if ( !this.m_mol.contains(center) ) {
				logger.error("All atoms in the carbon chain must be contained in the molecule specified at constructor.");
				return null;
			}

			IBond bondPrev = (i==0)? null : center.getBond(a_chain.get(i-1));
			IBond bondNext = (i==nLength-1)? null : center.getBond(a_chain.get(i+1));

			List<IBond> modBonds = new ArrayList<>();
			for ( IBond bond : this.m_mol.getConnectedBondsList(center) ) {
				if ( bond.equals(bondPrev) || bond.equals(bondNext) )
					continue;
				modBonds.add(bond);
			}

			// Collect bonds connecting to a ring atom for prior bonds
			List<IBond> priorBonds = new ArrayList<>();
			for ( IBond bondToMod : modBonds ) {
				IAtom atomMod = bondToMod.getOther(center);
				boolean bIsBridged = false;
				for ( IAtom atomConn : this.m_mol.getConnectedAtomsList(atomMod) ) {
					if ( atomConn.equals(center) )
						continue;
					if ( !a_chain.contains(atomConn) )
						continue;
					bIsBridged = true;
					break;
				}
				if ( bIsBridged )
					priorBonds.add(bondToMod);
			}
			// Order modification bonds
			if ( modBonds.size() > 1 )
				modBonds = orderModBonds(center, modBonds, priorBonds);

			IBond bondMod1 = ( modBonds.size() < 1 || modBonds.size() > 3 )? null : modBonds.get(0);
			IBond bondMod2 = ( modBonds.size() < 2 || modBonds.size() > 3 )? null : modBonds.get(1);
			IBond bondMod3 = ( modBonds.size() < 3 || modBonds.size() > 3 )? null : modBonds.get(2);

			CarbonUnit cu = new CarbonUnit(center, bondMod1, bondMod2, bondMod3);

			if ( bondPrev == null ) {
				cc.addCarbonUnit(cu, null, null);
				continue;
			}

			mapUnitToPrevBondOrig.put(cu, bondPrev);
			cc.addCarbonUnit(cu, bondPrev.getOrder(), bondPrev.getStereo());

		}

		// Set chiral
		for ( CarbonUnit cu : cc.getCarbonUnits() ) {
			IAtom center = cu.getCenterAtom();
			if ( center.getHybridization() != Hybridization.SP3 )
				continue;
			setChiralStereo(cu);
		}

		// Set double bond stereo
		for ( CarbonUnit cu : mapUnitToPrevBondOrig.keySet() ) {
			IBond bondOrig = mapUnitToPrevBondOrig.get(cu);
			if ( bondOrig.getOrder() != Order.DOUBLE )
				continue;
			if ( bondOrig.getStereo() != IBond.Stereo.E_Z_BY_COORDINATES )
				continue;
			resetBondStereo(cu, mapUnitToPrevBondOrig);
		}

		return cc;
	}

	private List<IBond> orderModBonds(IAtom a_atomCenter, List<IBond> a_lModBonds, List<IBond> a_lPriorBonds) {
		// Sort with CIP rule
		List<IBond> lModBondsOrdered = orderModBondsWithCIPRule(a_atomCenter, a_lModBonds);

		// Reorder with the uniqueness of the modifications
		ConverterUtils.orderModificationBonds(lModBondsOrdered, a_lPriorBonds);

		return lModBondsOrdered;
	}

	private List<IBond> orderModBondsWithCIPRule(IAtom a_atomCenter, List<IBond> a_lModBonds) {
		int iChiralAtom = this.m_mol.indexOf(a_atomCenter);
		int nLigands = a_lModBonds.size();
		ILigand[] ligands = new ILigand[nLigands];
		Map<ILigand, IBond> mapLigandToBond = new HashMap<>();
		for ( int i=0; i<nLigands; i++ ) {
			IBond bond = a_lModBonds.get(i);
			int iLigandAtom = this.m_mol.indexOf(bond.getOther(a_atomCenter));
			ligands[i] = CIPTool.defineLigand(this.m_mol, new VisitedAtoms(), iChiralAtom, iLigandAtom);
			mapLigandToBond.put(ligands[i], bond);
		}
		// Order ligands (lower order comes first)
		ligands = CIPTool.order(ligands);

		List<IBond> lModBondsOrdered = new ArrayList<>();
		for ( int i=nLigands-1; i>=0; i-- )
			lModBondsOrdered.add( mapLigandToBond.get(ligands[i]) );
		return lModBondsOrdered;
	}

	private void setChiralStereo(CarbonUnit a_cu) {
		ITetrahedralChirality chiralOrig = this.m_stereoManager.getTetrahedralChirality(a_cu.getCenterAtom());
		if ( chiralOrig == null ) {
			if ( a_cu.getCenterAtom().getImplicitHydrogenCount() < 2 )
				a_cu.setStereo(CarbonUnit.Stereo.X);
			return;
		}

		IAtom[] ligandsOrig = chiralOrig.getLigands();
		Stereo stereo = chiralOrig.getStereo();

		// Check ligand order
		IAtom[] ligands = a_cu.createLigands();
		if ( ligands == null ) {
			logger.warn("No chirality on the carbon unit.");
			return;
		}

		boolean unmatch = false;
		int[] orders = new int[4];
		for ( int i=0; i<4; i++ ) {
			orders[i] = -1;
			for ( int j=0; j<4; j++ ) {
				if ( !ligandsOrig[i].equals(ligands[j]) )
					continue;
				orders[i] = j;
				break;
			}
			if ( orders[i] == -1 )
				unmatch = true;
		}
		if ( unmatch )
			logger.warn("Something wrong in stereo extraction for carbon chain.");
		boolean isEvenPerm = isEvenPermutation(orders);

		if ( !isEvenPerm )
			stereo = stereo.invert();
		a_cu.setStereo(CarbonUnit.Stereo.toStereo(stereo));
	}

	/**
	 * Calculates permutations that the specified orders is even permutation or not
	 * @param orders List of the order numbers
	 * @return {@code true} if the orders is even permutations
	 */
	private static boolean isEvenPermutation(int[] orders) {
		int c=0;
		for ( int i=0; i<orders.length; i++ ) {
			if ( orders[i] == i )
				continue;
			for ( int j=i+1; j<orders.length; j++ ) {
				if ( orders[j] != i )
					continue;
				orders[j] = orders[i];
				orders[i] = i;
				c++;
				break;
			}
		}
		return (c%2) == 0;
	}

	private void resetBondStereo(CarbonUnit a_cu, Map<CarbonUnit, IBond> mapUnitToPrevBondOrig) {
		IDoubleBondStereochemistry dbStereo =
				getDoubleBondStereochemistry(a_cu, mapUnitToPrevBondOrig);
		if ( dbStereo == null ) {
			a_cu.getPreviousBond().setStereo(IBond.Stereo.NONE);
			return;
		}

		IBond bondDB = a_cu.getPreviousBond();
		CarbonUnit cuPre = a_cu.getPreviousUnit();

		IBond[] ligandBonds = dbStereo.getBonds();
		Conformation conf = dbStereo.getStereo();

		// Check connectivities
		IBond bondPrev = (ligandBonds[0].contains(cuPre.getCenterAtom()))?
				ligandBonds[0] : ligandBonds[1];
		IBond bondNext = (ligandBonds[0].contains(a_cu.getCenterAtom()))?
				ligandBonds[0] : ligandBonds[1];
		boolean isConnPrev = (cuPre.isTerminal())?
				cuPre.getBondToMod1().contains( bondPrev.getOther(cuPre.getCenterAtom()) ) :
				bondPrev.contains( cuPre.getPreviousUnit().getCenterAtom() );
		boolean isConnNext = (a_cu.isTerminal())?
				a_cu.getBondToMod1().contains( bondNext.getOther(a_cu.getCenterAtom()) ) :
				bondNext.contains( a_cu.getNextUnit().getCenterAtom() );
		if ( isConnPrev != isConnNext )
			conf = conf.invert();

		IBond.Stereo stereoNew = (conf == Conformation.OPPOSITE)?
				IBond.Stereo.E : IBond.Stereo.Z;
		bondDB.setStereo(stereoNew);
	}

	private IDoubleBondStereochemistry getDoubleBondStereochemistry(
			CarbonUnit a_cu, Map<CarbonUnit, IBond> mapUnitToPrevBondOrig) {
		IBond bondPrevOrig = mapUnitToPrevBondOrig.get(a_cu);
		IDoubleBondStereochemistry dbStereo = this.m_stereoManager.getDoubleBondStereochemistry(bondPrevOrig);
		if ( dbStereo != null )
			return dbStereo;

		if ( a_cu.getPreviousUnit().isTerminal() && a_cu.getPreviousUnit().getModBonds().isEmpty() )
			return null;
		if ( a_cu.isTerminal() && a_cu.getModBonds().isEmpty() )
			return null;

		// Calculate double bond stereo from 2d coordinate
		IBond[] ligandBonds = new IBond[2];
		ligandBonds[0] = (!a_cu.getPreviousUnit().isTerminal())?
				mapUnitToPrevBondOrig.get(a_cu.getPreviousUnit()) :
				a_cu.getPreviousUnit().getBondToMod1();
		ligandBonds[1] = (!a_cu.isTerminal())?
				mapUnitToPrevBondOrig.get(a_cu.getNextUnit()) :
				a_cu.getBondToMod1();
		IDoubleBondStereochemistry.Conformation conf = calcConf(bondPrevOrig, ligandBonds);
		dbStereo = new DoubleBondStereochemistry(bondPrevOrig, ligandBonds, conf);

		return dbStereo;
	}

	private IDoubleBondStereochemistry.Conformation calcConf(IBond center, IBond[] ligandBonds) {
		IAtom atomA1 = center.getBegin();
		IAtom atomB1 = center.getEnd();

		IAtom atomA2 = ( ligandBonds[0].contains(atomA1) )?
				ligandBonds[0].getOther(atomA1) :
				ligandBonds[1].getOther(atomA1);
		IAtom atomB2 = ( ligandBonds[0].contains(atomB1) )?
				ligandBonds[0].getOther(atomB1) :
				ligandBonds[1].getOther(atomB1);

		Vector2d vecA = GeometryUtil.calculatePerpendicularUnitVector(atomA1.getPoint2d(), atomA2.getPoint2d());
		Vector2d vecB = GeometryUtil.calculatePerpendicularUnitVector(atomB1.getPoint2d(), atomB2.getPoint2d());
		Vector2d vecAB = GeometryUtil.calculatePerpendicularUnitVector(atomA1.getPoint2d(), atomB1.getPoint2d());

		boolean bPositiveCrossA = vecAB.getX()*vecA.getY() > vecAB.getY()*vecA.getX();
		boolean bPositiveCrossB = vecAB.getX()*vecB.getY() > vecAB.getY()*vecB.getX();

		return (bPositiveCrossA == bPositiveCrossB)?
				IDoubleBondStereochemistry.Conformation.TOGETHER :
				IDoubleBondStereochemistry.Conformation.OPPOSITE;
	}

}
