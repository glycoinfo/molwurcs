package org.glycoinfo.MolWURCS.exchange.toWURCS;

import org.glycoinfo.MolWURCS.om.buildingblock.ChainConnection;
import org.glycoinfo.MolWURCS.om.buildingblock.ChainConnection.DirectionType;
import org.glycoinfo.WURCSFramework.wurcs.graph.DirectionDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.LinkagePosition;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;

public class ChainConnectionToWURCSEdge {

	public static WURCSEdge convert(ChainConnection a_cconn) {
		WURCSEdge edge = new WURCSEdge();
		edge.addLinkage( toLinkagePosition(a_cconn) );
		return edge;
	}

	public static LinkagePosition toLinkagePosition(ChainConnection a_cconn) {

		int iBPos = a_cconn.getCarbonPosition();
		int iDirection = a_cconn.getModIDOnCarbonUnit();
		DirectionType dType = a_cconn.getDirectionType();

		DirectionDescriptor dd = getDirectionDescriptor(iDirection, dType);
		boolean isOmittableDD = ( dType == ChainConnection.DirectionType.NONE );

		int iMPos = a_cconn.getStarIndex();
		boolean isOmittableMPos = (iMPos == 0);

		return new LinkagePosition(iBPos, dd, isOmittableDD, iMPos, isOmittableMPos);
	}

	private static DirectionDescriptor getDirectionDescriptor(int iDirection, DirectionType dType) {
		switch (dType) {
		case NONE:
			return DirectionDescriptor.N;
		case CHIRAL_CC:
			return ( iDirection == 1 )? DirectionDescriptor.U :
				   ( iDirection == 2 )? DirectionDescriptor.D :
				   ( iDirection == 3 )? DirectionDescriptor.T : null;
		case CHIRAL_ACC:
			return ( iDirection == 1 )? DirectionDescriptor.D :
				   ( iDirection == 2 )? DirectionDescriptor.U :
				   ( iDirection == 3 )? DirectionDescriptor.T : null;
		case DBSTEREO_E:
			return ( iDirection == 1 )? DirectionDescriptor.E :
				   ( iDirection == 2 )? DirectionDescriptor.Z : null;
		case DBSTEREO_Z:
			return ( iDirection == 1 )? DirectionDescriptor.Z :
				   ( iDirection == 2 )? DirectionDescriptor.E : null;
		case UNKNOWN:
			return DirectionDescriptor.X;
		}
		return null;
	}
}
