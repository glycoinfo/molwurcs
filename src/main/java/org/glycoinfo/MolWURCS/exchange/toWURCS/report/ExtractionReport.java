package org.glycoinfo.MolWURCS.exchange.toWURCS.report;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtractionReport {

	private static Logger logger = LoggerFactory.getLogger(ExtractionReport.class);

	private static final String HEAD = "###";
	private static final char COMMA = ',';
	private static final String LINE_SEPARATOR = System.lineSeparator();

	private static Map<Class<? extends Report>, List<Field>> mapClassToFields;
	private static Map<Class<? extends Report>, String> mapClassToTag;
	private static String strReportHeader;
	static {
		mapClassToTag = new HashMap<>();
		mapClassToTag.put(Entry.class, "E");
		mapClassToTag.put(Molecule.class, "M");
		mapClassToTag.put(CarbonGroup.class, "CG");
		mapClassToTag.put(CarbonChain.class, "CC");
		mapClassToTag.put(ModAtomGroup.class, "MG");

		mapClassToFields = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		for ( Class<? extends Report> clazz : mapClassToTag.keySet() ) {
			List<Field> lFields = new ArrayList<>();
			try {
				for ( Field f : clazz.getDeclaredFields() ) {
					String name = f.getName();
					if ( name.equals("id")
					  || name.equals("bIsOmittable")
					  || name.equals("reportParent")
					  || name.equals("reportChildren") )
						continue;
					f.setAccessible(true);
					lFields.add(f);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			mapClassToFields.put(clazz, lFields);

			// Build string for header
			sb.append(HEAD);
			sb.append(mapClassToTag.get(clazz)).append(COMMA);
			sb.append("id").append(COMMA);
			if ( clazz != Entry.class )
				sb.append("parentId").append(COMMA);
			for ( Field f : mapClassToFields.get(clazz) )
				sb.append(f.getName()).append(COMMA);
			sb.append(LINE_SEPARATOR);
		}
		strReportHeader = sb.toString();
	}

	public static void writeReportHeader(BufferedWriter a_writer) {
		try {
			a_writer.write(strReportHeader);
			a_writer.flush();
		} catch (IOException e) {
			logger.error("Error in writing report header", e);
		}
	}

	public static abstract class Report {
		protected int id;

		protected boolean bIsOmittable;

		protected Report reportParent;
		protected List<Report> reportChildren;

		Report() {
			this.bIsOmittable = false;

			this.reportParent = null;
			this.reportChildren = new ArrayList<>();
		}

		void setIsOmittable(boolean a_bIsOmittable) {
			this.bIsOmittable = a_bIsOmittable;
		}

		void addChild(Report a_reportChild) {
			a_reportChild.reportParent = this;
			this.reportChildren.add(a_reportChild);
			a_reportChild.id = this.reportChildren.size();
		}

		String getTag() {
			return mapClassToTag.get(this.getClass());
		}

		private String getFullId() {
			String id = String.valueOf(this.id);
			Report parent = this.reportParent;
			if ( parent != null )
				id = parent.getFullId()+"_"+id;
			return id;
		}

		StringBuilder getLog() {
			// For omittable
			if ( this.bIsOmittable )
				return new StringBuilder();
			// Collect field objects
			List<Object> lFields = new ArrayList<>();
			try {
				for ( Field f : mapClassToFields.get(this.getClass()) )
					lFields.add(f.get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}

			// Log fields
			StringBuilder sb = new StringBuilder(HEAD);
			sb.append(this.getTag()).append(COMMA);
			sb.append(this.getFullId()).append(COMMA);
			if ( this.reportParent != null )
				sb.append(this.reportParent.getFullId()).append(COMMA);
			for ( Object o : lFields )
				sb.append(o).append(COMMA);
			sb.append(LINE_SEPARATOR);

			// Log children
			for ( Report child : reportChildren )
				sb.append(child.getLog());

			return sb;
		}


		public void write(BufferedWriter a_writer) {
			try {
				a_writer.write(this.getLog().toString());
				a_writer.flush();
			} catch (IOException e) {
				logger.error("Error in writing report", e);
			}
		}

	}

	private static int idEntryCurrent = 0;

	public static class Entry extends Report {

		String strTitle;
		boolean bHasNormalizationError;
		double dMass;
		int nMols;
		boolean bHasGlycan;

		Entry() {
			super();

			this.id = ++idEntryCurrent;
			this.strTitle = null;
			this.bHasNormalizationError = false;
			this.nMols = 0;
			this.bHasGlycan = false;
		}

		void setTitle(String a_strTitle) {
			this.strTitle = a_strTitle;
		}

		void setHasNormalizationError(boolean a_bHasError) {
			this.bHasNormalizationError = a_bHasError;
		}

		void setMass(double a_dMass) {
			this.dMass = a_dMass;
		}

		void setMoleculeCount(int a_nMols) {
			this.nMols = a_nMols;
		}

		void setHasGlycan(boolean a_bHasGlycan) {
			this.bHasGlycan = a_bHasGlycan;
		}
	}

	static class Molecule extends Report {
		boolean bHasGlycan;
		boolean bHasAglycone;
		int nSeparations;
		int nCarbonGroups;
		int nTooSmall;
		int nTooLarge;
		int nHasTooManyBranches;
		int nHasConnToCCyclic;
		int nHasConnToPiCyclic;
		int nHasTooManyModifications;

		Molecule() {
			this.bHasGlycan = false;
			this.bHasAglycone = false;
			this.nSeparations = 0;
			this.nCarbonGroups = 0;
			this.nTooSmall = 0;
			this.nTooLarge = 0;
			this.nHasTooManyBranches = 0;
			this.nHasConnToCCyclic = 0;
			this.nHasConnToPiCyclic = 0;
			this.nHasTooManyModifications = 0;
		}

		void setHasGlycan(boolean a_bHasGlycan) {
			this.bHasGlycan = a_bHasGlycan;
		}

		void setHasAglycone(boolean a_bHasAglycone) {
			this.bHasAglycone = a_bHasAglycone;
		}

		void setSeparationCount(int a_nSeparations) {
			this.nSeparations = a_nSeparations;
		}

		void setCarbonChainCount(int a_nCarbonChains) {
			this.nCarbonGroups = a_nCarbonChains;
		}

		void setTooSmallCount(int a_nTooSmallCount) {
			this.nTooSmall = a_nTooSmallCount;
		}

		void setTooLargeCount(int a_nTooLargeCount) {
			this.nTooLarge = a_nTooLargeCount;
		}

		void setHasTooManyBranchesCount(int a_nHasTooManyBranches) {
			this.nHasTooManyBranches = a_nHasTooManyBranches;
		}

		void setHasConnectionToCarbonCyclicCount(int a_nHasConnToCCylic) {
			this.nHasConnToCCyclic = a_nHasConnToCCylic;
		}

		void setHasConnectionToPiCyclicCount(int a_nHasConnToPiCyclic) {
			this.nHasConnToPiCyclic = a_nHasConnToPiCyclic;
		}

		void setHasTooManyModificationsCount(int a_nHasTooManyModifications) {
			this.nHasTooManyModifications = a_nHasTooManyModifications;
		}
	}

	static class CarbonGroup extends Report {
		int nAtoms;
		int nBranches;

		boolean bIsTooSmall;
		boolean bIsTooLarge;
		boolean bHasTooManyBranches;
		boolean bHasConnToCCyclic;
		boolean bHasConnToPiCyclic;
		boolean bHasTooManyModifications;

		CarbonGroup() {
			this.nAtoms = 0;
			this.nBranches = 0;

			this.bIsTooSmall = false;
			this.bIsTooLarge = false;
			this.bHasTooManyBranches = false;
			this.bHasConnToCCyclic = false;
			this.bHasConnToPiCyclic = false;
			this.bHasTooManyModifications = false;
		}

		void setAtomCount(int a_nAtoms) {
			this.nAtoms = a_nAtoms;
		}

		void setBranchCount(int a_nBranches) {
			this.nBranches = a_nBranches;
		}

		void setIsTooSmall(boolean a_bIsTooSmall) {
			this.bIsTooSmall = a_bIsTooSmall;
		}

		void setIsTooLarge(boolean a_bIsTooLarge) {
			this.bIsTooLarge = a_bIsTooLarge;
		}

		void setHasTooManyBranches(boolean a_bHasTooManyBranches) {
			this.bHasTooManyBranches = a_bHasTooManyBranches;
		}

		void setHasConnectionToCarbonCyclic(boolean a_bHasConnToCCyclic) {
			this.bHasConnToCCyclic = a_bHasConnToCCyclic;
		}

		void setHasConnectionToPiCyclic(boolean a_bHasConnToPiCyclic) {
			this.bHasConnToPiCyclic = a_bHasConnToPiCyclic;
		}

		void setHasTooManyModifications(boolean a_bHasTooManyModifications) {
			this.bHasTooManyModifications = a_bHasTooManyModifications;
		}
	}

	static class CarbonChain extends Report {
		int nLength;

		int nModification;
		int nThreshold;
		boolean bIsExceeded;

		boolean bIsTooShort;
		boolean bIsTooLong;

		boolean bHasSmallCyclic;
		boolean bIsExtracted;
		String strSkeletonCode;

		CarbonChain() {
			this.nLength = 0;

			this.nModification = 0;
			this.nThreshold = 0;
			this.bIsExceeded = false;

			this.bIsTooShort = false;
			this.bIsTooLong = false;

			this.bHasSmallCyclic = false;
			this.bIsExtracted = false;
			this.strSkeletonCode = null;
		}

		@Override
		String getTag() {
			String tag = super.getTag();
			if ( this.reportParent instanceof CarbonChain )
				tag += "Sub";
			return tag;
		}

		void setLength(int a_nLength) {
			this.nLength = a_nLength;
		}

		void setModificationCount(int a_nModification) {
			this.nModification = a_nModification;
		}

		void setThreshold(int a_nThreshold) {
			this.nThreshold = a_nThreshold;
		}

		void setIsExceededThreshold(boolean a_bIsExceeded) {
			this.bIsExceeded = a_bIsExceeded;
		}

		void setIsTooShort(boolean a_bIsTooShort) {
			this.bIsTooShort = a_bIsTooShort;
		}

		void setIsTooLong(boolean a_bIsTooLong) {
			this.bIsTooLong = a_bIsTooLong;
		}

		void setHasSmallCyclic(boolean a_bHasSmallCyclic) {
			this.bHasSmallCyclic = a_bHasSmallCyclic;
		}

		void setIsExtracted(boolean a_bIsExtracted) {
			this.bIsExtracted = a_bIsExtracted;
		}

		void setSkeletonCode(String a_strSkeletonCode) {
			this.strSkeletonCode = a_strSkeletonCode;
		}
	}

	static class ModAtomGroup extends Report {
		int nAtoms;
		int nBranches;

		boolean bIsTooSmall;
		boolean bIsTooLarge;
		boolean bHasTooManyBranches;

		int nRings;
		int iMaxRingSize;

		boolean bHasTooLargeRing;
		boolean bHasTooManyRings;
		boolean bContainsDisallowedAtoms;

		boolean bIsReducingEnd;

		boolean bIsRemained;
		String strMAP;

		ModAtomGroup() {
			this.nAtoms = 0;
			this.nBranches = 0;

			this.bIsTooSmall = false;
			this.bIsTooLarge = false;
			this.bHasTooManyBranches = false;
			this.bContainsDisallowedAtoms = false;

			this.nRings = 0;
			this.iMaxRingSize = 0;

			this.bHasTooLargeRing = false;
			this.bHasTooManyRings = false;

			this.bIsReducingEnd = false;

			this.bIsRemained = false;
			this.strMAP = null;
		}

		@Override
		String getTag() {
			return "MG";
		}

		void setAtomCount(int a_nAtoms) {
			this.nAtoms = a_nAtoms;
		}

		void setBranchCount(int a_nBranches) {
			this.nBranches = a_nBranches;
		}

		void setIsTooSmall(boolean a_bIsTooSmall) {
			this.bIsTooSmall = a_bIsTooSmall;
		}

		void setIsTooLarge(boolean a_bIsTooLarge) {
			this.bIsTooLarge = a_bIsTooLarge;
		}

		void setHasTooManyBranches(boolean a_bHasTooManyBranches) {
			this.bHasTooManyBranches = a_bHasTooManyBranches;
		}

		void setRingCount(int a_nRings) {
			this.nRings = a_nRings;
		}

		void setMaxRingSize(int a_iMaxRingSize) {
			this.iMaxRingSize = a_iMaxRingSize;
		}

		void setHasTooLargeRing(boolean a_bHasTooLargeRing) {
			this.bHasTooLargeRing = a_bHasTooLargeRing;
		}

		void setHasTooManyRings(boolean a_bHasTooManyRings) {
			this.bHasTooManyRings = a_bHasTooManyRings;
		}

		void setContainsDisallowedAtoms(boolean a_bContainsDisallowedAtoms) {
			this.bContainsDisallowedAtoms = a_bContainsDisallowedAtoms;
		}

		void setIsReducingEnd(boolean a_bIsReducingEnd) {
			this.bIsReducingEnd = a_bIsReducingEnd;
		}

		void setIsRemained(boolean a_bIsRemained) {
			this.bIsRemained = a_bIsRemained;
		}

		void setMAP(String a_strMAP) {
			this.strMAP = a_strMAP;
		}
	}

	// Filter conditions
	int nMinCarbons;
	int nMaxCarbons;
	int nMaxBranches;
	int nMinLength;
	int nMaxLength;

	int nMinModAtoms;
	int nMaxModAtoms;
	int nMaxModBranches;
	int[] aAllowedAtomNums;
	int nMaxRingSize;
	int nMaxRings;
}
