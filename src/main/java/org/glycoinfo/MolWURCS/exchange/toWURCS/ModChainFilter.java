package org.glycoinfo.MolWURCS.exchange.toWURCS;

import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IRingSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModChainFilter extends AtomGroupFilter {

	private static final Logger logger = LoggerFactory.getLogger(ModChainFilter.class);

	private int[] m_aAllowedAtomNums;

	private int m_iMaxRingCount;
	private int m_iMaxRingSize;

	private int m_iMaxStarCount;

	public ModChainFilter() {
		super();

		this.m_aAllowedAtomNums = null;

		this.m_iMaxRingCount = -1;
		this.m_iMaxRingSize = -1;

		this.m_iMaxStarCount = -1;
	}

	public void setAllowedAtomicNumbers(int... a_aAtomNums) {
		this.m_aAllowedAtomNums = a_aAtomNums;
	}

	public void setMaximumRingCount(int a_iMaxRingCount) {
		this.m_iMaxRingCount = a_iMaxRingCount;
	}

	public void setMaximumRingSize(int a_iMaxRingSize) {
		this.m_iMaxRingSize = a_iMaxRingSize;
	}

	public void setMaximumStarCount(int a_iMaxStarCount) {
		this.m_iMaxStarCount = a_iMaxStarCount;
	}

	/**
	 * {@inheritDoc}
	 * the number of rings is lower than max threshold,
	 * the most large ring size is lower than max threshold,
	 */
	@Override
	public boolean checkWithAtomGroupCondition(IAtomContainer a_molGrp) {
		boolean bCanBeCandidate = true;
		bCanBeCandidate &= super.checkWithAtomGroupCondition(a_molGrp);
		bCanBeCandidate &= !this.checkAllowedAtoms(a_molGrp);
		bCanBeCandidate &= this.checkRings(a_molGrp);
		return bCanBeCandidate;
	}

	/**
	 * Returns {@code true} if the given atom group contains atoms which are not
	 * allowed to be constituent element of a modification.
	 * @param a_molGrp IAtomContainer of modification
	 * @return {@code true} if the given atom group contains atoms which are not
	 * allowed to be constituent element of a modification
	 */
	protected boolean checkAllowedAtoms(IAtomContainer a_molGrp) {
		if ( this.m_aAllowedAtomNums == null )
			return false;
		for ( IAtom atom : a_molGrp.atoms() ) {
			// Find atom which is not allowed as a part of modification
			boolean isAllowed = false;
			for ( int iAtomNum : this.m_aAllowedAtomNums ) {
				if ( atom.getAtomicNumber() != iAtomNum )
					continue;
				isAllowed = true;
				break;
			}
			if ( !isAllowed )
				return true;
		}
		return false;
	}

	/**
	 * Returns {@code true} if the given atom group satisfies the following requirements:
	 * the number of rings is over max threshold,
	 * the most large ring size is over max threshold.
	 * @param a_molGrp IAtomContainer of modification
	 * @return {@code true} if the given atom group satisfies the requirements
	 */
	protected boolean checkRings(IAtomContainer a_molGrp) {
		// Collect SSSR (smallest set of smallest rings)
		Cycles cycles = Cycles.mcb(a_molGrp);
		IRingSet rings = cycles.toRingSet();

		// Check count and maximum size
		int nRings = rings.getAtomContainerCount();
		int iRingSizeMax = 0;
		for ( IAtomContainer ring : rings.atomContainers() )
			if ( iRingSizeMax < ring.getAtomCount() )
				iRingSizeMax = ring.getAtomCount();

		boolean bCanBeCandidate = true;
		bCanBeCandidate &= !this.checkRingCount(a_molGrp, nRings);
		bCanBeCandidate &= !this.checkRingSize(a_molGrp, iRingSizeMax);
		return bCanBeCandidate;
	}

	protected boolean checkRingCount(IAtomContainer a_molGrp, int a_nRingCount) {
		return this.m_iMaxRingCount >= 0 && a_nRingCount > this.m_iMaxRingCount;
	}

	protected boolean checkRingSize(IAtomContainer a_molGrp, int a_iRingSizeMax) {
		return this.m_iMaxRingSize >= 0 && a_iRingSizeMax > this.m_iMaxRingSize;
	}

	/**
	 * Returns {@code true} if number of connections to backbone carbons on the
	 * given modification is over max threshold.
	 * @param a_molGrp IAtomContainer of modification
	 * @param a_nStars int of number of connections to backbone carbons
	 * @return {@code true} if number of connections to backbone carbons on the
	 * given modification is over max threshold
	 */
	protected boolean checkStarCount(IAtomContainer a_molGrp, int a_nStars) {
		return this.m_iMaxStarCount >= 0 && a_nStars > this.m_iMaxStarCount;
	}
}
