package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.exchange.ConverterUtils;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.util.analysis.CarbonChainUtils;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.BackboneCarbon;
import org.glycoinfo.WURCSFramework.wurcs.graph.CarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor.ModificationType;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomType.Hybridization;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IBond.Stereo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarbonChainToBackbone {

	private static Logger logger = LoggerFactory.getLogger(CarbonChainToBackbone.class);

	private CarbonChainToBackbone() {}

	public static Backbone convert(CarbonChain a_cc) {
		Backbone bb = new Backbone();
		int i=0;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			i++;
			boolean isAnomeric = CarbonChainUtils.isAnomeric(cu, a_cc);
			isAnomeric &= (bb.getAnomericPosition() == 0);

			ICarbonDescriptor cd = convert(cu, isAnomeric);
			BackboneCarbon bc = new BackboneCarbon(bb, cd);
			bb.addBackboneCarbon(bc);

			if ( isAnomeric )
				bb.setAnomericPosition(i);
		}

		if ( bb.getAnomericPosition() > 0 ) {
			char cAnom = getAnomericSymbol(a_cc, bb.getAnomericPosition());
			bb.setAnomericSymbol(cAnom);
		}

		return bb;
	}

	public static ICarbonDescriptor convert(CarbonUnit a_cu, boolean a_bIsAnomeric) {
		// For hybrid orbital
		String strOrbital = getHybridOrbital(a_cu.getCenterAtom().getHybridization());

		// For bond orders between backbone carbons
		int iOrder1 = (a_cu.getPreviousBond() == null)? 0 :
			a_cu.getPreviousBond().getOrder().numeric();
		int iOrder2 = (a_cu.getNextBond() == null)? 0 :
			a_cu.getNextBond().getOrder().numeric();
		// Swap orders if previous one is lower
		if ( iOrder1 < iOrder2 ) {
			int tmp = iOrder1;
			iOrder1 = iOrder2;
			iOrder2 = tmp;
		}

		// For modifications
		List<IBond> modBonds = a_cu.getModBonds();
		// Add bonds for implicit hydrogens explicitly
		modBonds.addAll( getImplicitHydrogenBonds(a_cu) );

		// Count unique modification types
		int nUniqMod = ConverterUtils.countUniqueMod(modBonds);
		// Force # of unique modification types for anomeric carbon
		if ( a_bIsAnomeric )
			nUniqMod = (a_cu.isTerminal())? 3 : 2;

		// Order modification bonds
		ConverterUtils.orderModificationBonds(modBonds);
		// Add null if # of bonds is less than 3
		if ( modBonds.size() < 3 ) {
			int nNull = 3 - modBonds.size();
			for ( int i=0; i<nNull; i++ )
				modBonds.add(null);
		}

		// For modification types
		ModificationType[] mods = new ModificationType[3];
		mods[0] = ConverterUtils.toModificationType(modBonds.get(0));
		mods[1] = ConverterUtils.toModificationType(modBonds.get(1));
		mods[2] = ConverterUtils.toModificationType(modBonds.get(2));

		// For stereo
		String strStereo = null;
		if ( a_cu.getStereo() != null )
			strStereo = a_cu.getStereo().name();
		else if ( iOrder1 == 2 )
			strStereo = getDBStereo(a_cu);

		ICarbonDescriptor cd = CarbonDescriptor.forCarbonSituation(strOrbital,
			iOrder1, iOrder2, nUniqMod, strStereo, a_bIsAnomeric,
			mods[0], mods[1], mods[2]);

		if ( cd.getChar() == '?' )
			logger.warn("Unknown CarbonDescriptor is asigned for atom type \"{}\"",
					a_cu.getCenterAtom().getAtomTypeName());

		return cd;
	}

	private static String getHybridOrbital(Hybridization h) {
		switch(h) {
		case SP1:
			return ICarbonDescriptor.SP;
		case SP2:
			return ICarbonDescriptor.SP2;
		case SP3:
			return ICarbonDescriptor.SP3;
		default:
			return ICarbonDescriptor.SPX;
		}
	}

	private static List<IBond> getImplicitHydrogenBonds(CarbonUnit a_cu) {
		IAtom center = a_cu.getCenterAtom();

		List<IBond> newBonds = new ArrayList<>();
		Integer hCount = center.getImplicitHydrogenCount();
		if ( hCount == null )
			hCount = 0;

		// Add hydrogens to cancel negative formal charge
		if ( a_cu.getCenterAtom().getFormalCharge() < 0 )
			hCount -= a_cu.getCenterAtom().getFormalCharge();
		if ( hCount == 0 )
			return newBonds;

		for ( int i=0; i<hCount; i++ ) {
			IAtom hydrogen = center.getBuilder().newInstance(IAtom.class, "H");
//			hydrogen.setAtomTypeName("H");
//			hydrogen.setImplicitHydrogenCount(0);
			newBonds.add(center.getBuilder().newInstance(IBond.class, center, hydrogen,
					Order.SINGLE));
		}
		return newBonds;
	}

	private static char getAnomericSymbol(CarbonChain a_cc, int a_iAnomPos) {
		CarbonUnit cuAnom = a_cc.getCarbonUnit(a_iAnomPos);
		char cAnom = 'x';
		if ( cuAnom.getStereo() == CarbonUnit.Stereo.X )
			return cAnom;

		// Find anomeric reference atom
		CarbonUnit cuAnomRef = null;
		int i=0;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			if ( cu.equals(cuAnom) )
				continue;
			if ( cu.getStereo() == null )
				continue;
			i++;
			cuAnomRef = cu;
			if ( i == 4 )
				break;
		}

		// Return absolute anomeric symbol if no stereo on the reference atom
		if ( cuAnomRef == null || cuAnomRef.getStereo() == CarbonUnit.Stereo.X )
			return (cuAnom.getStereo() == CarbonUnit.Stereo.S)?
					'u' : 'd';

		return (cuAnom.getStereo() == cuAnomRef.getStereo())?
				'a' : 'b';
	}

	private static String getDBStereo(CarbonUnit cu) {
		IBond bondDB =
			( cu.getPreviousBond() == null || cu.getPreviousBond().getOrder() != Order.DOUBLE )?
			cu.getNextBond() : cu.getPreviousBond();

		IBond.Stereo stereo = bondDB.getStereo();
		if ( stereo == null )
			return ICarbonDescriptor.X;
		if ( stereo == Stereo.E_OR_Z )
			return ICarbonDescriptor.X;
		if ( stereo == Stereo.NONE )
			return ICarbonDescriptor.N;
		if ( stereo == Stereo.E_Z_BY_COORDINATES ) {
			logger.warn("The double bond has not been determined by coordinates.");
			return ICarbonDescriptor.X;
		}

		return ( stereo == Stereo.E )?
				ICarbonDescriptor.E : ICarbonDescriptor.Z;
	}
}
