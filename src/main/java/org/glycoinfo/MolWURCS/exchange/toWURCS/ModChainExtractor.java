package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IChemObject;
import org.openscience.cdk.interfaces.IStereoElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModChainExtractor {

	private static final Logger logger = LoggerFactory.getLogger(ModChainExtractor.class);

	public static final int MIN_ATOM_COUNT = 1;
	public static final int MAX_ATOM_COUNT = -1;
	public static final int MAX_BRANCH_COUNT = 4;
	/** H, C, N, O, P, S */
	public static final int[] ALLOWED_ATOMIC_NUMBERS = {1, 6, 7, 8, 15, 16};
	public static final int MAX_RING_COUNT = -1;
	public static final int MAX_RING_SIZE = 9;

	public static final int MAX_STAR_COUNT = -1;

	private ModChainFilter m_mcFilter;

	public ModChainExtractor() {
		this.m_mcFilter = createNewFilter();

		this.m_mcFilter.setMinimumAtomCount(MIN_ATOM_COUNT);
		this.m_mcFilter.setMaximumAtomCount(MAX_ATOM_COUNT);
		this.m_mcFilter.setMaximumBranchCount(MAX_BRANCH_COUNT);
		this.m_mcFilter.setAllowedAtomicNumbers(ALLOWED_ATOMIC_NUMBERS);
		this.m_mcFilter.setMaximumRingCount(MAX_RING_COUNT);
		this.m_mcFilter.setMaximumRingSize(MAX_RING_SIZE);

		this.m_mcFilter.setMaximumStarCount(MAX_STAR_COUNT);
	}

	protected ModChainFilter createNewFilter() {
		return new ModChainFilter();
	}

	public List<ModChain> start(IAtomContainer a_mol, List<CarbonChain> a_lCCs, boolean a_bDoFilter) {
		Set<IAtom> setCCAtoms = new HashSet<>();
		// Collect carbons containing carbon chains
		for ( CarbonChain cc : a_lCCs )
			for ( CarbonUnit cu : cc.getCarbonUnits() )
				setCCAtoms.add(cu.getCenterAtom());

		// Extract modification group
		List<IAtomContainer> lModMols = extractCandidateModificationGroup(a_mol, setCCAtoms);

		// Collect stereochemistries for each modification
		Map<IAtomContainer, List<IStereoElement>> mapModToStereos = new HashMap<>();
		for ( IStereoElement stereo : a_mol.stereoElements() ) {
			for ( IAtomContainer molMod : lModMols ) {
				IChemObject focus = stereo.getFocus();
				if ( focus instanceof IAtom ) {
					IAtom atomChiral = (IAtom)focus;
					if ( !molMod.contains(atomChiral) )
						continue;
				}
				if ( focus instanceof IBond ) {
					IBond bondStereo = (IBond)focus;
					if ( !molMod.contains(bondStereo) )
						continue;
				}
				if ( !mapModToStereos.containsKey(molMod) )
					mapModToStereos.put(molMod, new ArrayList<>());
				mapModToStereos.get(molMod).add(stereo);
			}
		}

		// Add stars for each container
		Map<IAtomContainer, List<IBond>> mapMolToStarBonds = new HashMap<>();
		Map<IBond, IAtom> mapBondToStar = new HashMap<>();
		for ( IAtom atomStar : setCCAtoms ) {
			for ( IBond bond : a_mol.getConnectedBondsList(atomStar) ) {
				IAtom atomMod = bond.getOther(atomStar);
				if ( setCCAtoms.contains(atomMod) )
					continue;
				for ( IAtomContainer molMod : lModMols ) {
					if ( !molMod.contains(atomMod) )
						continue;
					// Add star atom and the bond to container
					molMod.addAtom(atomStar);
					molMod.addBond(bond);
					mapBondToStar.put(bond, atomStar);
					// Map star and container
					if ( !mapMolToStarBonds.containsKey(molMod) )
						mapMolToStarBonds.put(molMod, new ArrayList<>());
					mapMolToStarBonds.get(molMod).add(bond);
					break;
				}
			}
		}

		// Set stereochemistry for each modification
		for ( IAtomContainer molMod : mapModToStereos.keySet() )
			molMod.setStereoElements( mapModToStereos.get(molMod) );

		// Filter candidate modifications
		if ( a_bDoFilter )
			lModMols = filter(lModMols, mapMolToStarBonds);

		// Create ModChains
		List<ModChain> lMCs = new ArrayList<>();
		for ( IAtomContainer molMod : lModMols ) {
			List<IBond> lBondsFromStar = mapMolToStarBonds.get(molMod);
			if ( lBondsFromStar == null ) {
				logger.warn("Separated modification is found.");
				continue;
			}

			ModChain mc = createModChain(molMod);
			lMCs.add(mc);

			// Set stars
			for ( IBond bondFromStar : lBondsFromStar ) {
				IAtom atomStar = mapBondToStar.get(bondFromStar);
				// Set StarIndice as zero at first
				// Each StarIndex will be calculated and set properly in MAPGraph normalization
				mc.setAtomAsStar(0, atomStar, bondFromStar);
			}
		}

		return lMCs;
	}

	protected List<IAtomContainer> extractCandidateModificationGroup(IAtomContainer a_mol, Set<IAtom> a_setCCAtoms) {
		// Collect modification atoms
		LinkedList<IAtom> modAtoms = new LinkedList<>();
		for ( IAtom atom : a_mol.atoms() ) {
			if ( a_setCCAtoms.contains(atom) )
				continue;
			modAtoms.add(atom);
		}

		// Split molecules to connected atom groups as modifications
		List<IAtomContainer> lModMols = new ArrayList<>();
		while ( !modAtoms.isEmpty() ) {
			IAtom atomMod = modAtoms.getFirst();
			IAtomContainer molMod = getConnectingGraph(atomMod, a_mol, a_setCCAtoms);
			lModMols.add(molMod);

			for ( IAtom atom : molMod.atoms() )
				modAtoms.remove(atom);
		}

		return lModMols;
	}

	private static IAtomContainer getConnectingGraph(IAtom a_atomStart, IAtomContainer a_mol, Set<IAtom> lIgnoreAtoms) {
		IAtomContainer molSub = a_mol.getBuilder().newAtomContainer();
		molSub.addAtom(a_atomStart);
		LinkedList<IAtom> lConnAtoms = new LinkedList<>();
		lConnAtoms.add(a_atomStart);
		while ( !lConnAtoms.isEmpty() ) {
			IAtom atom = lConnAtoms.removeFirst();
			for ( IBond bond : a_mol.getConnectedBondsList(atom) ) {
				IAtom atomConn = bond.getOther(atom);
				if ( lIgnoreAtoms.contains(atomConn) )
					continue;
				if ( !molSub.contains(atomConn) ) {
					lConnAtoms.add(atomConn);
					molSub.addAtom(atomConn);
				}
				if ( !molSub.contains(bond) )
					molSub.addBond(bond);
			}
		}
		return molSub;
	}

	protected List<IAtomContainer> filter(List<IAtomContainer> a_lModMols,
			Map<IAtomContainer, List<IBond>> a_mapMolToStarBonds) {
		List<IAtomContainer> lModMolsFiltered = new ArrayList<>();
		for ( IAtomContainer a_molMod : a_lModMols ) {
			// Filter by atom group conditions
			if ( !this.m_mcFilter.checkWithAtomGroupCondition(a_molMod) ) {
				logger.info("A modification is removed as aglycone.");
				continue;
			}

			// Filter by # of connections on modification
			int nStarBonds = 0;
			if ( a_mapMolToStarBonds.containsKey(a_molMod) )
				nStarBonds = a_mapMolToStarBonds.get(a_molMod).size();
			if ( this.m_mcFilter.checkStarCount(a_molMod, nStarBonds) ) {
				logger.info("A modification with too many valences is removed.");
				continue;
			}
			lModMolsFiltered.add(a_molMod);
		}

		return lModMolsFiltered;
	}

	protected ModChain createModChain(IAtomContainer a_molMod) {
		return new ModChain(a_molMod);
	}
}
