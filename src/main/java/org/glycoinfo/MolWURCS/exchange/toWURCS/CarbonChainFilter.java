package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.List;
import java.util.Set;

import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.util.analysis.AtomChainExtractor.AtomChain;
import org.glycoinfo.MolWURCS.util.analysis.CarbonChainUtils;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarbonChainFilter extends AtomGroupFilter {

	private static Logger logger = LoggerFactory.getLogger(CarbonChainFilter.class);

	private int m_iMinChainLength;
	private int m_iMaxChainLength;

	private boolean m_bAllowSmallCyclicEther;
	private boolean m_bAllowAromaticModAtom;
	private Set<IAtom> m_setPiCyclicAtoms;
	private Set<IAtom> m_setCarbonCyclicAtoms;

	public CarbonChainFilter() {
		super();

		this.m_iMinChainLength = -1;
		this.m_iMaxChainLength = -1;

		this.m_bAllowSmallCyclicEther = true;
		this.m_bAllowAromaticModAtom = true;
	}

	public void setPiCyclicAtoms(Set<IAtom> atoms) {
		this.m_setPiCyclicAtoms = atoms;
	}

	public Set<IAtom> getPiCyclicAtoms() {
		return this.m_setPiCyclicAtoms;
	}

	public void setCarbonCyclicAtoms(Set<IAtom> atoms) {
		this.m_setCarbonCyclicAtoms = atoms;
	}

	public Set<IAtom> getCarbonCyclicAtoms() {
		return this.m_setCarbonCyclicAtoms;
	}

	// Setters for carbon chain length filter

	public void setMinimumChainLength(int a_iMinChainLength) {
		this.m_iMinChainLength = a_iMinChainLength;
	}

	public void setMaximumChainLength(int a_iMaxChainLength) {
		this.m_iMaxChainLength = a_iMaxChainLength;
	}

	// Setters for carbon chain condition filter

	public void setAllowSmallCylcicEther(boolean disallow) {
		this.m_bAllowSmallCyclicEther = disallow;
	}

	public void setAllowAromaticModification(boolean disallow) {
		this.m_bAllowAromaticModAtom = disallow;
	}

	/**
	 * Returns {@code true} if the given carbon group satisfies following requirements:
	 * there is no connection to carbon cylic,
	 * there is no connection to pi-cylic atom (if not allowed).
	 * @param a_molCGrp IAtomContainer of target carbon group
	 * @param a_mol IAtomContainer of molecule containing target carbon group
	 * @return {@code true} if the given carbon group satisfies requirements
	 */
	public boolean checkWithCarbonGroupCondition(IAtomContainer a_molCGrp, IAtomContainer a_mol) {
		boolean bCanBeCandidate = true;
		bCanBeCandidate &= !this.checkConnectionToCarbonCyclic(a_molCGrp, a_mol); // if not has the connection
		bCanBeCandidate &= !this.checkConnectionToPiCyclic(a_molCGrp, a_mol);     // if not has the connection
		return bCanBeCandidate;
	}

	protected boolean checkConnectionToCarbonCyclic(IAtomContainer a_molCGrp, IAtomContainer a_mol) {
		for ( IAtom atom : a_molCGrp.atoms() ) {
			for ( IAtom atomConn : a_mol.getConnectedAtomsList(atom) ) {
				// Ignore atoms in carbon group
				if ( a_molCGrp.contains(atomConn) )
					continue;
				if ( this.m_setCarbonCyclicAtoms.contains(atomConn) )
					return true;
			}
		}
		return false;
	}

	protected boolean checkConnectionToPiCyclic(IAtomContainer a_molCGrp, IAtomContainer a_mol) {
		// return false if pi-cyclic connection is allowed
		if ( this.m_bAllowAromaticModAtom )
			return false;
		for ( IAtom atom : a_molCGrp.atoms() ) {
			for ( IAtom atomConn : a_mol.getConnectedAtomsList(atom) ) {
				// Ignore atoms in carbon group
				if ( a_molCGrp.contains(atomConn) )
					continue;
				if ( this.m_setPiCyclicAtoms.contains(atomConn) )
					return true;
			}
		}
		return false;
	}

	/**
	 * Returns {@code true} if the given chain satisfies following requirements:
	 * the length of main chain is within the min and max threshold.
	 * @param a_chain AtomChain of target carbon chain
	 * @return {@code true} if the given chain satisfies requirements
	 */
	public boolean checkWithAtomChainCondition(AtomChain a_chain) {
		int nLength = a_chain.getMainChain().size();

		return this.checkWithAtomChainCondition(a_chain, nLength);
	}

	protected boolean checkWithAtomChainCondition(AtomChain a_chain, int a_nLength) {
		boolean bCanBeCandidate = true;
		bCanBeCandidate &= !checkMinimumChainLength(a_chain, a_nLength);
		bCanBeCandidate &= !checkMaximumChainLength(a_chain, a_nLength);
		return bCanBeCandidate;
	}

	protected boolean checkMinimumChainLength(AtomChain a_chain, int a_nLength) {
		return this.m_iMinChainLength >= 0 && a_nLength < this.m_iMinChainLength;
	}

	protected boolean checkMaximumChainLength(AtomChain a_chain, int a_nLength) {
		return this.m_iMaxChainLength >= 0 && a_nLength > this.m_iMaxChainLength;
	}

	/**
	 * Returns {@code true} if the given carbon chain satisfies requirements.
	 * @see #checkWithBackboneCondition(CarbonChain, CarbonChain)
	 * @param a_cc CarbonChain of target carbon chain
	 * @param a_ac AtomChain of original chain of the target
	 * @return {@code true} if the given carbon chain satisfies requirements
	 */
	public boolean checkWithBackboneCondition(CarbonChain a_cc) {
		return this.checkWithBackboneCondition(a_cc, null);
	}

	/**
	 * Returns {@code true} if the given carbon chain satisfies following requirements:
	 * the # of modifications is equal or lower than threshold based on the chain length
	 * (the threshold is 1 for ~4, 2 for 5, 3 for 6, 4 for 7, or 5 for 8~),
	 * there is no small cyclic ether on the carbon chain (if not allowed).
	 * @param a_cc CarbonChain of target carbon chain
	 * @param a_ccParent CarbonChain of parent carbon chain of target
	 * @return {@code true} if the given carbon chain satisfies requirements
	 */
	public boolean checkWithBackboneCondition(CarbonChain a_cc, CarbonChain a_ccParent) {
		boolean bCanBeBackbone = true;

		// Check # of modification on the carbon chain ( nMod > nThreshold )
		bCanBeBackbone &= !checkModificationCount(a_cc, countModifications(a_cc, a_ccParent), getThreshold(a_cc));
		// Check small cyclic ether ( true if it has the cyclic ether )
		bCanBeBackbone &= !checkSmallCyclicEther(a_cc);

		return bCanBeBackbone;
	}

	private int getThreshold(CarbonChain a_cc) {
		int iValue = a_cc.getCarbonUnits().size() - 3;
		// 1 (len <= 4), len-3 (4 < len < 8), 5 (8 <= len)
		return Math.max(1, Math.min(iValue, 5));
	}

	private int countModifications(CarbonChain a_cc, CarbonChain a_ccParent) {
		int nAnomer = 0;
		int nPAnomer = 0;
		boolean hasAnomerOnHead = false;
		int nRingTotal = 0;
		int nTotalMods = 0;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			// Count modification for each bond type
			int nModS = cu.countModBondsWith(Order.SINGLE, null);
			int nModD = cu.countModBondsWith(Order.DOUBLE, null);
			int nModT = cu.countModBondsWith(Order.TRIPLE, null);

			// Reduce counts for branched carbon (only for single bond)
			int nCS = cu.countModBondsWith(Order.SINGLE, 6);
			nModS -= nCS;

			int nMod = nModS + nModD*2 + nModT*3;

			// Count no modification (deoxy)
			if ( nMod == 0 )
				nMod++;

			// Count multiple bond on carbon chain
			int nMultiBond = 0;
			if ( cu.getNextBond() != null ) {
				Order order = cu.getNextBond().getOrder();
				nMultiBond += ( order.numeric() == 0 )? 1 : order.numeric()-1;
			}
			if ( cu.getPreviousBond() != null ) {
				Order order = cu.getPreviousBond().getOrder();
				nMultiBond += ( order.numeric() == 0 )? 1 : order.numeric()-1;
			}
			nMod += nMultiBond;

			// Count carboxyl like carbon as additional penalty
			if ( nModS == 1 && nModD == 1 && nMultiBond == 0 )
				nMod++;

			// Count modification making ring
			int nRing = CarbonChainUtils.getRingedCarbonUnits(cu, a_cc).size();
			// Count modification making ring with parent carbon chain
			if ( a_ccParent != null )
				nRing += CarbonChainUtils.getRingedCarbonUnits(cu, a_ccParent).size()*2;
			nRingTotal += nRing;

			// Reduce counts for oxygen modifications
			int nOS = cu.countModBondsWith(Order.SINGLE, 8);
			int nOD = cu.countModBondsWith(Order.DOUBLE, 8);

			nMod -= nOS + nOD*2;

			// Additional count for two or more single bond oxygens
			if ( nOS > 1 )
				nMod += nOS-1;

			boolean hasAnomer = false;
			// Count anomer
			if ( nModS == 2 && nMultiBond == 0 && nRing > 0 ) {
				nAnomer++;
				hasAnomer = true;
			}

			// Count carbolyl and aldehyde (potential anomer)
			if ( nOD == 1 && nModS == 0 && nMultiBond == 0 ) {
				nPAnomer++;
				hasAnomer = true;
			}

			if ( hasAnomer && a_cc.getCarbonUnits().indexOf(cu) < 2 )
				hasAnomerOnHead = true;

			nTotalMods += nMod;
		}
		nTotalMods += nRingTotal/2;

		// Correction for anomer except for branch
		if ( a_ccParent == null ) {
			nTotalMods -= nAnomer*2;
			// Correction for (potential) anomer
			if ( nAnomer + nPAnomer > 0 )
				nTotalMods += (nAnomer + nPAnomer - 1) *2;
			// Penalty for no anomer
			if ( !hasAnomerOnHead )
				nTotalMods++;
		}

		checkModificationInfo(a_cc, a_ccParent, a_cc.getCarbonUnits().size(),
				nRingTotal/2, nAnomer, nPAnomer, nTotalMods);

		return nTotalMods;
	}

	protected void checkModificationInfo(CarbonChain a_cc, CarbonChain a_ccParent,
			int a_nLength, int a_nRings, int a_nAnomer, int a_nPAnomer, int a_nTotalMods) {

		logger.debug("Chain: {}", (a_ccParent == null)? "main" : "sub");
		logger.debug("Length: {}", a_nLength);
		logger.debug("Ring: {}", a_nRings);
		logger.debug("Anomer: {}", a_nAnomer);
		logger.debug("PAnomer: {}", a_nPAnomer);
		logger.debug("Mod total: {}", a_nTotalMods);
	}

	protected boolean checkModificationCount(CarbonChain a_cc, int a_nMod, int a_nThreshold) {
		return a_nMod > a_nThreshold;
	}

	protected boolean checkSmallCyclicEther(CarbonChain a_cc) {
		// Return false if small cyclic ether is allowed
		if ( this.m_bAllowSmallCyclicEther )
			return false;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			List<CarbonUnit> lRingedCUs = CarbonChainUtils.getRingedCarbonUnits(cu, a_cc);
			if ( lRingedCUs.isEmpty() )
				continue;
			int pos1 = a_cc.getCarbonUnits().indexOf(cu);
			for ( CarbonUnit cuRinged : lRingedCUs ) {
				int pos2 = a_cc.getCarbonUnits().indexOf(cuRinged);
				if ( Math.abs(pos1-pos2) < 3 )
					return true;
			}
		}
		return false;
	}

}
