package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry;
import org.openscience.cdk.interfaces.IStereoElement;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.stereo.Stereocenters;

public class StereoManager {

	private static final Map<IAtomContainer, StereoManager> mapMolToManager
		= new HashMap<>();

	private Stereocenters m_stereocenters;

	private Map<IAtom, ITetrahedralChirality> m_mapAtomToChiral;
	private Map<IBond, IDoubleBondStereochemistry> m_mapBondToDBStereo;

	private StereoManager(IAtomContainer a_mol) {
		this.m_stereocenters = Stereocenters.of(a_mol);

		this.m_mapAtomToChiral = new HashMap<>();
		this.m_mapBondToDBStereo = new HashMap<>();
		for ( IStereoElement stereo : a_mol.stereoElements() ) {
			if ( stereo instanceof ITetrahedralChirality ) {
				ITetrahedralChirality chiral = (ITetrahedralChirality)stereo;
				this.m_mapAtomToChiral.put(chiral.getChiralAtom(), chiral);
				continue;
			}
			if ( stereo instanceof IDoubleBondStereochemistry) {
				IDoubleBondStereochemistry dbStereo = (IDoubleBondStereochemistry)stereo;
				this.m_mapBondToDBStereo.put(dbStereo.getFocus(), dbStereo);
			}
		}
	}

	public Stereocenters getStereocenters() {
		return this.m_stereocenters;
	}

	public List<IAtom> getChiralAtoms() {
		return new ArrayList<>( this.m_mapAtomToChiral.keySet() );
	}

	public ITetrahedralChirality getTetrahedralChirality(IAtom a_atom) {
		return this.m_mapAtomToChiral.get(a_atom);
	}

	public List<IBond> getStereoBonds() {
		return new ArrayList<>( this.m_mapBondToDBStereo.keySet() );
	}

	public IDoubleBondStereochemistry getDoubleBondStereochemistry(IBond bond) {
		return this.m_mapBondToDBStereo.get(bond);
	}

	public static StereoManager of(IAtomContainer a_mol) {
		if ( !mapMolToManager.containsKey(a_mol) )
			mapMolToManager.put(a_mol, new StereoManager(a_mol));
		return mapMolToManager.get(a_mol);
	}

	public static void clearOld() {
		mapMolToManager.clear();
	}
}
