package org.glycoinfo.MolWURCS.exchange.toWURCS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.util.analysis.AtomChainExtractor;
import org.glycoinfo.MolWURCS.util.analysis.AtomChainExtractor.AtomChain;
import org.glycoinfo.MolWURCS.util.analysis.AtomGroupExtractor;
import org.glycoinfo.MolWURCS.util.analysis.StructureAnalyzer;
import org.glycoinfo.MolWURCS.util.comparator.CarbonChainComparator;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarbonChainExtractor {

	private static Logger logger = LoggerFactory.getLogger(CarbonChainExtractor.class);

	public static final int MIN_CARBON_COUNT = 5;
	public static final int MAX_CARBON_COUNT = 12;
	public static final int MAX_BRANCH_COUNT = 1;
	public static final int MIN_CHAIN_LENGTH = 4;

	public static final boolean ALLOW_SMALL_CYCLIC = true;
	public static final boolean ALLOW_AROMATIC_MOD = false;

	private CarbonChainFilter m_ccFilter;

	public CarbonChainExtractor() {
		this.m_ccFilter = this.createNewFilter();

		// init filter parameters
		this.m_ccFilter.setMinimumAtomCount(MIN_CARBON_COUNT);
		this.m_ccFilter.setMaximumAtomCount(MAX_CARBON_COUNT);
		this.m_ccFilter.setMaximumBranchCount(MAX_BRANCH_COUNT);

		this.m_ccFilter.setMinimumChainLength(MIN_CHAIN_LENGTH);

		this.m_ccFilter.setAllowSmallCylcicEther(ALLOW_SMALL_CYCLIC);
		this.m_ccFilter.setAllowAromaticModification(ALLOW_AROMATIC_MOD);
	}

	protected CarbonChainFilter createNewFilter() {
		return new CarbonChainFilter();
	}

	public List<CarbonChain> start(IAtomContainer a_mol) {

		// Analyze structure
		StructureAnalyzer sa = new StructureAnalyzer(a_mol);

		// Set special atoms to filter
		this.m_ccFilter.setPiCyclicAtoms(sa.getPiCyclicAtoms());
		this.m_ccFilter.setCarbonCyclicAtoms(sa.getCarbonCyclicAtoms());

		// Extract carbon groups
		List<IAtomContainer> lCGroups = extractCarbonGroups(a_mol, sa);

		// Extract carbon chain groups for each carbon group
		List<List<AtomChain>> lChainGroupsCandidate =
				extractCandidateCarbonChains(lCGroups);

		// Extract carbon chains for backbones
		CarbonChainBuilder ccBuilder = new CarbonChainBuilder(a_mol);
		List<CarbonChain> lCCs = extractCarbonChains(lChainGroupsCandidate, ccBuilder);

		return lCCs;
	}

	private List<IAtomContainer> extractCarbonGroups(IAtomContainer a_mol, StructureAnalyzer a_sa) {
		// Collect atoms to be ignored from connecting carbon group
		Set<IAtom> atomsToBeIgnored = new HashSet<>();
		atomsToBeIgnored.addAll( a_sa.getPiCyclicAtoms() );
		atomsToBeIgnored.addAll( a_sa.getCarbonCyclicAtoms() );
		for ( IAtom atom : a_mol.atoms() )
			if ( atom.isAromatic() )
				atomsToBeIgnored.add(atom);

		// Extractor for connecting carbon group
		AtomGroupExtractor age = new AtomGroupExtractor();
		age.addTargetAtomicNumbers(6);
		age.addIgnoreAtoms(atomsToBeIgnored);
		age.setMinimumAtomCount(3);
		return this.filterCarbonGroups(age.extract(a_mol), a_mol);
	}

	protected List<IAtomContainer> filterCarbonGroups(List<IAtomContainer> a_lCGroups, IAtomContainer a_mol) {
		List<IAtomContainer> lFilteredCGroups = new ArrayList<>();
		for ( IAtomContainer group : a_lCGroups ) {
			// Filter with atom group condition
			if ( !this.m_ccFilter.checkWithAtomGroupCondition(group) )
				continue;
			// Filter with carbon group condition
			if ( !this.m_ccFilter.checkWithCarbonGroupCondition(group, a_mol) )
				continue;
			lFilteredCGroups.add(group);
		}
		return lFilteredCGroups;
	}

	private List<List<AtomChain>> extractCandidateCarbonChains(List<IAtomContainer> a_lGroups) {
		// Extractor for chains
		AtomChainExtractor ace = new AtomChainExtractor();
		// Target carbons
		ace.setTargetAtomicNumbers(6);

		List<List<AtomChain>> lChainGroupsCandidate = new LinkedList<>();
		for ( IAtomContainer group : a_lGroups ) {
			List<AtomChain> lChainGroup =
					this.filterCandidateCarbonChains(ace.extractAtomChain(group), group);
			lChainGroupsCandidate.add(lChainGroup);
		}

		return lChainGroupsCandidate;
	}

	protected List<AtomChain> filterCandidateCarbonChains(List<AtomChain> a_chains, IAtomContainer a_group) {
		List<AtomChain> lChainGroup = new LinkedList<>();
		for ( AtomChain chain : a_chains ) {
			if ( this.m_ccFilter.checkWithAtomChainCondition(chain) )
				lChainGroup.add(chain);
		}
		return lChainGroup;
	}

	private List<CarbonChain> extractCarbonChains(List<List<AtomChain>> a_lChainGroupsCandidate, CarbonChainBuilder a_ccBuilder) {
		logger.debug("Extract carbon chains");
		List<CarbonChain> lCCs = new ArrayList<>();
		for ( List<AtomChain> lChainGroup : a_lChainGroupsCandidate ) {
			List<CarbonChain> lCCCandidates = new ArrayList<>();
			for ( AtomChain chain : lChainGroup ) {
				CarbonChain cc = this.extractCarbonChain(chain, a_ccBuilder);
				if ( cc != null )
					lCCCandidates.add(cc);
			}
			if ( lCCCandidates.isEmpty() )
				continue;

			// Sort carbon chains
			Collections.sort(lCCCandidates, new CarbonChainComparator());

			// Choose the first one of each group
			lCCs.add(lCCCandidates.get(0));
		}
		return lCCs;
	}

	private CarbonChain extractCarbonChain(AtomChain a_chain, CarbonChainBuilder a_ccBuilder) {
		logger.debug("Extract carbon chain");
		CarbonChain cc = convertCarbonChain(a_chain, a_ccBuilder);
		// Filter by main chain with backbone condition
		if ( !this.m_ccFilter.checkWithBackboneCondition(cc) )
			return null;
		// Check subchains
		if ( !a_chain.getSubChains().isEmpty() ) {
			logger.debug("Check sub chain:");
			boolean isCandidate = true;
			for ( AtomChain chainSub : a_chain.getSubChains() ) {
				CarbonChain ccSub = convertCarbonChain(chainSub, a_ccBuilder);
				// Filter sub chain with backbone condition
				if ( this.m_ccFilter.checkWithBackboneCondition(ccSub, cc) )
					continue;
				isCandidate = false;
				break;
			}
			if ( !isCandidate )
				return null;
		}
		return cc;
	}

	protected CarbonChain convertCarbonChain(AtomChain a_chain, CarbonChainBuilder a_ccBuilder) {
		return a_ccBuilder.createCarbonChain(a_chain.getMainChain());
	}
}
