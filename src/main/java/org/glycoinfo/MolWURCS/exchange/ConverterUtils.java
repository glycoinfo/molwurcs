package org.glycoinfo.MolWURCS.exchange;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor.ModificationType;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IBond;

public class ConverterUtils {

	public static ModificationType toModificationType(IBond a_bond) {
		if ( a_bond == null )
			return null;
		return toModificationType(a_bond.getOrder(), a_bond.getEnd());
	}

	public static ModificationType toModificationType(IBond.Order a_order, IAtom a_atom) {
		boolean isHydrogen = (a_atom.getAtomicNumber() == 1);
		int iOrder =a_order.numeric();
		return ICarbonDescriptor.ModificationType.forConnection(iOrder, isHydrogen);
	}

	public static void orderModificationBonds(List<IBond> lModBonds) {
		orderModificationBonds(lModBonds, null);
	}

	public static void orderModificationBonds(List<IBond> lModBonds, final List<IBond> lPriorBonds) {
		// Map and count unique modification bond
		final Map<IBond, String> mapBondToUniqueMod = new HashMap<>();
		final Map<String, Integer> mapUniqueModToCount = new HashMap<>();
		for ( IBond bond : lModBonds ) {
			String strUniqueMod = toUniqueMod(bond);
			mapBondToUniqueMod.put(bond, strUniqueMod);

			if ( !mapUniqueModToCount.containsKey(strUniqueMod) )
				mapUniqueModToCount.put(strUniqueMod, 0);
			int count = mapUniqueModToCount.get(strUniqueMod) + 1;
			mapUniqueModToCount.put(strUniqueMod, count);
		}
		// Record current order
		final List<IBond> lModBondsCurrent = new ArrayList<>(lModBonds);
		// Sort
		Collections.sort(lModBonds, new Comparator<IBond>() {
			@Override
			public int compare(IBond bond1, IBond bond2) {
				int iComp = 0;

				String strUMod1 = mapBondToUniqueMod.get(bond1);
				String strUMod2 = mapBondToUniqueMod.get(bond2);
				if ( !strUMod1.equals(strUMod2) ) {
					// Prioritize major unique modifications
					int nUMod1 = mapUniqueModToCount.get(strUMod1);
					int nUMod2 = mapUniqueModToCount.get(strUMod2);
					iComp = nUMod2 - nUMod1;
					if ( iComp != 0 )
						return iComp;
				}

				// Prioritize higher bond order
				int iOrder1 = bond1.getOrder().numeric();
				int iOrder2 = bond2.getOrder().numeric();
				iComp = iOrder2 - iOrder1;
				if ( iComp != 0 )
					return iComp;

				// Prioritize prior bonds
				if ( lPriorBonds != null && !lPriorBonds.isEmpty() ) {
					if (  lPriorBonds.contains(bond1) && !lPriorBonds.contains(bond2) )
						return -1;
					if ( !lPriorBonds.contains(bond1) && lPriorBonds.contains(bond2) )
						return 1;
				}

				// Keep current order (prioritize smaller index)
				int index1 = lModBondsCurrent.indexOf(bond1);
				int index2 = lModBondsCurrent.indexOf(bond2);
				iComp = index1 - index2;
				if ( iComp != 0 )
					return iComp;

				return 0;
			}
		});

	}

	public static int countUniqueMod(List<IBond> lModBonds) {
		Set<String> setUniqueMods = new HashSet<>();
		for ( IBond bond : lModBonds ) {
			if ( bond == null )
				continue;
			setUniqueMods.add( toUniqueMod(bond) );
		}
		return setUniqueMods.size();
	}

	public static String toUniqueMod(IBond bond) {
		String strUniqueMod = "";
		strUniqueMod += bond.getOrder().numeric();
		strUniqueMod += "-";
		strUniqueMod += bond.getEnd().getAtomicNumber();
		return strUniqueMod;
	}

}
