package org.glycoinfo.MolWURCS.cli;

import java.io.PrintWriter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glycoinfo.MolWURCS.io.formats.ChemFormatType;
import org.openscience.cdk.exception.CDKException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static final Logger logger = LoggerFactory.getLogger(App.class);

	static final String APP_NAME = "MolWURCS";
	static final String APP_VERSION = "0.11.2";

	private static final int STATUS_PARSE_ERROR    = 1;
	private static final int STATUS_ARGUMENT_ERROR = 2;
	private static final int STATUS_RUNTIME_ERROR  = 3;

	public static void main(String[] args) {
		new App().run(args);
	}

	public void run(String[] args) {
		CommandLineParser parser = new DefaultParser();

		try {
			CommandLine cmd = parser.parse(options(), args);

			if ( cmd.hasOption("help") ) {
				printUsage();
				return;
			}
			// Checked required options directly
			if ( !cmd.hasOption(OPTION_INPUT_FORMAT_LONG)
			  || !cmd.hasOption(OPTION_OUTPUT_FORMAT_LONG) )
				throw new ParseException("Missing required options: i, o");

			if ( cmd.getArgList().size() > 1 )
				logger.warn("Two or more input strings are found. Only the first string will be read: {}", cmd.getArgList().get(0));

			Converter converter = getConverter();

			converter
				.setInput( cmd.getArgList().isEmpty()? null : cmd.getArgList().get(0) )
				.setInputFormat( ChemFormatType.forName( cmd.getOptionValue(OPTION_INPUT_FORMAT_LONG) ) )
				.setOutputFormat( ChemFormatType.forName( cmd.getOptionValue(OPTION_OUTPUT_FORMAT_LONG) ) )
				.setOutputWithAglycone( cmd.hasOption(OPTION_OUTPUT_WITH_AGLYCONE_LONG) )
				.setTitlePropertyID( cmd.getOptionValue(OPTION_TITLE_PROPERTY_LONG) )
				.setOutputNoResult( cmd.hasOption(OPTION_OUTPUT_NO_RESULT_LONG) )
				.setOutputReport( cmd.hasOption(OPTION_OUTPUT_REPORT_LONG) )
				.setDoDoubleCheck( cmd.hasOption(OPTION_DOUBLE_CHECK_LONG) )
				.run();
		} catch (ParseException e) {
			logger.error("Parse Error:", e);
			printUsage();
			System.exit(STATUS_PARSE_ERROR);
		} catch (IllegalArgumentException e) {
			logger.error("Argument Error:", e);
			printUsage();
			System.exit(STATUS_ARGUMENT_ERROR);
		} catch (CDKException e) {
			logger.error(e.getMessage(), e);
			System.exit(STATUS_RUNTIME_ERROR);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			System.exit(STATUS_RUNTIME_ERROR);
		}
	}

	Converter getConverter() {
		return new Converter();
	}

	private static final String OPTION_INPUT_FORMAT_LONG = "in";
	private static final String OPTION_INPUT_FORMAT_SHORT = "i";

	private static final String OPTION_OUTPUT_FORMAT_LONG = "out";
	private static final String OPTION_OUTPUT_FORMAT_SHORT = "o";

	private static final String[] ARGUMENTS_INPUT_FORMAT = ChemFormatType.getInputFormatTypes();
	private static final String[] ARGUMENTS_OUTPUT_FORMAT = ChemFormatType.getOutputFormatTypes();

	private static final String OPTION_OUTPUT_WITH_AGLYCONE_LONG = "with-aglycone";

	private static final String OPTION_TITLE_PROPERTY_LONG = "title-property-id";
	private static final String OPTION_TITLE_PROPERTY_SHORT = "p";

	private static final String OPTION_OUTPUT_NO_RESULT_LONG = "output-no-result";
	private static final String OPTION_OUTPUT_NO_RESULT_SHORT = "n";

	private static final String OPTION_OUTPUT_REPORT_LONG = "report";
	private static final String OPTION_OUTPUT_REPORT_SHORT = "r";

	private static final String OPTION_DOUBLE_CHECK_LONG = "double-check";

	private void printUsage() {
		String syntax = String
			.format("%s "
				+ " [--%s]"
				+ " [--%s <PROPERTY_ID>]"
				+ " --%s <FORMAT> --%s <FORMAT>"
				+ " [--%s]"
				+ " [--%s]"
				+ "[<INPUT FORMAT STRING>]",
				APP_NAME,
				OPTION_OUTPUT_WITH_AGLYCONE_LONG,
				OPTION_TITLE_PROPERTY_LONG,
				OPTION_INPUT_FORMAT_LONG,
				OPTION_OUTPUT_FORMAT_LONG,
				OPTION_OUTPUT_NO_RESULT_LONG,
				OPTION_DOUBLE_CHECK_LONG
			);
		String header = "\nOptions:";
		String footer = String.format("\nVersion: %s", APP_VERSION);

		HelpFormatter hf = new HelpFormatter();

		hf.printHelp(new PrintWriter(System.err, true), HelpFormatter.DEFAULT_WIDTH, syntax,
			header, options(), HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, footer);
	}

	private Options options() {
		Options options = new Options();

		options.addOption(Option.builder(OPTION_INPUT_FORMAT_SHORT)
				.longOpt(OPTION_INPUT_FORMAT_LONG)
				.desc("Set input format, required")
				.hasArg()
				.argName("FORMAT")
				.argName("FORMAT=[" + String.join("|", ARGUMENTS_INPUT_FORMAT) + "]")
//				.required()
				.build()
			);

		options.addOption(Option.builder(OPTION_OUTPUT_FORMAT_SHORT)
				.longOpt(OPTION_OUTPUT_FORMAT_LONG)
				.desc("Set output format, required")
				.hasArg()
				.argName("FORMAT")
				.argName("FORMAT=[" + String.join("|", ARGUMENTS_OUTPUT_FORMAT) + "]")
//				.required()
				.build()
			);

		options.addOption(Option.builder()
				.longOpt(OPTION_OUTPUT_WITH_AGLYCONE_LONG)
				.desc("Output WURCS with aglycone,"
					+ " active only when \"wurcs\" is specified as output format")
				.build()
			);

		options.addOption(Option.builder(OPTION_TITLE_PROPERTY_SHORT)
				.longOpt(OPTION_TITLE_PROPERTY_LONG)
				.desc("Use property value as title, which key of the value is <PROPERTY_ID>")
				.hasArg()
				.argName("PROPERTY_ID")
				.build()
			);

		options.addOption(Option.builder(OPTION_OUTPUT_NO_RESULT_SHORT)
				.longOpt(OPTION_OUTPUT_NO_RESULT_LONG)
				.desc("Output result explicitly even if the conversion is failed or not contained result")
				.build()
			);

		options.addOption(Option.builder(OPTION_OUTPUT_REPORT_SHORT)
				.longOpt(OPTION_OUTPUT_REPORT_LONG)
				.desc("Output glycan extraction report,"
					+ " active only when \"wurcs\" is specified as output format")
				.build()
			);

		// TODO: Tentative option. Remove in future.
		options.addOption(Option.builder()
				.longOpt(OPTION_DOUBLE_CHECK_LONG)
				.desc("Double-check output WURCS using WURCS to WURCS convert,"
					+ " not active when \"wurcs\" is set to input and output formats"
					+ " at the same time")
				.build()
			);

		options.addOption(Option.builder("h")
			.longOpt("help")
			.desc("Show usage help")
			.build()
		);

		return options;
	}

}
