package org.glycoinfo.MolWURCS.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import org.glycoinfo.MolWURCS.io.WURCSParser;
import org.glycoinfo.MolWURCS.io.WURCSWriter;
import org.glycoinfo.MolWURCS.io.formats.ChemFormatType;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.io.IChemObjectReader.Mode;
import org.openscience.cdk.io.IChemObjectWriter;
import org.openscience.cdk.io.ISimpleChemObjectReader;
import org.openscience.cdk.io.SMILESWriter;
import org.openscience.cdk.io.iterator.DefaultIteratingChemObjectReader;
import org.openscience.cdk.io.iterator.IIteratingChemObjectReader;
import org.openscience.cdk.io.iterator.IteratingSDFReader;
import org.openscience.cdk.silent.AtomContainer;
import org.openscience.cdk.silent.AtomContainerSet;
import org.openscience.cdk.smiles.SmiFlavor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Converter {

	private static final Logger logger = LoggerFactory.getLogger(Converter.class);

	private String m_strInput;
	private ChemFormatType m_formatInput;
	private ChemFormatType m_formatOutput;

	private String m_strPropID;
	private boolean m_bOutputWithAglycone;
	private boolean m_bOutputNoResult;

	private boolean m_bOutputReport;
	private boolean m_bDoDoubleCheck;

	private StringWriter m_swOutput;

	public Converter() {
		this.m_strInput = null;
		this.m_formatInput = null;
		this.m_formatOutput = null;

		this.m_strPropID = null;
		this.m_bOutputWithAglycone = false;
		this.m_bOutputNoResult = false;
		this.m_bOutputReport = false;
		this.m_bDoDoubleCheck = false;

		this.m_swOutput = null;
	}

	public Converter setInput(String a_strInput) {
		this.m_strInput = a_strInput;
		return this;
	}

	public Converter setInputFormat(ChemFormatType a_formatInput) {
		this.m_formatInput = a_formatInput;
		return this;
	}

	public Converter setOutputFormat(ChemFormatType a_formatOutput) {
		this.m_formatOutput = a_formatOutput;
		return this;
	}

	public Converter setTitlePropertyID(String a_strPropID) {
		this.m_strPropID = a_strPropID;
		return this;
	}

	public Converter setOutputWithAglycone(boolean a_bOutputAglycone) {
		this.m_bOutputWithAglycone = a_bOutputAglycone;
		return this;
	}

	public Converter setOutputNoResult(boolean a_bOuntputNoResult) {
		this.m_bOutputNoResult = a_bOuntputNoResult;
		return this;
	}

	public Converter setOutputReport(boolean a_bOutputReport) {
		this.m_bOutputReport = a_bOutputReport;
		return this;
	}

	public Converter setDoDoubleCheck(boolean a_bDoDoubleCheck) {
		this.m_bDoDoubleCheck = a_bDoDoubleCheck;
		return this;
	}

	public Converter setStringWriterForOutput(StringWriter a_swOutput) {
		this.m_swOutput = a_swOutput;
		return this;
	}

	public void run() throws IOException, CDKException {
		if ( this.m_formatInput == null )
			throw new RuntimeException("Input format must be specified.");
		if ( this.m_formatOutput == null )
			throw new RuntimeException("Output format must be specified.");

		Reader in = getReader(this.m_strInput);

		// Write molecules
		IChemObjectWriter writer = this.m_formatOutput.createWriter();
		// Write to stdout as default
		writer.setWriter(System.out);
		setOptionsForWriter(writer);

		if ( this.m_formatInput.hasIteratingReader() ) {
			processIterative(in, writer);
		} else {
			process(in, writer);
		}

		writer.close();
	}

	private Reader getReader(String strInput) {
		// Read from stdin if no input
		if ( strInput == null )
			return new InputStreamReader(System.in);

		// Read from file if it can be read
		File fileIn = new File(strInput);
		if ( fileIn.exists() && fileIn.canRead() )
			try {
				logger.info("Read input as file: {}", this.m_strInput);
				return new FileReader(fileIn);
			} catch (FileNotFoundException e) {
				return null;
			}

		// Read input string
		logger.info("Read input as string: {}", this.m_strInput);
		return new StringReader(this.m_strInput);
	}

	private void setOptionsForWriter(IChemObjectWriter writer) throws CDKException {
		// Write to StringWriter when it is set
		if ( this.m_swOutput != null )
			writer.setWriter(this.m_swOutput);

		// Add flavor to SMILESWriter to use aromatic symbols
		if ( writer instanceof SMILESWriter )
			((SMILESWriter)writer).setFlavor(SmiFlavor.Default | SmiFlavor.UseAromaticSymbols);

		// For WURCSWriter
		if ( writer instanceof WURCSWriter ) {
			WURCSWriter writerW = (WURCSWriter)writer;
			writerW.setOutputWithAglycone(this.m_bOutputWithAglycone);
			writerW.setTitlePropertyID(this.m_strPropID);
			writerW.setOutputMessage(this.m_bOutputNoResult);
			if ( this.m_formatInput != ChemFormatType.WURCS )
				writerW.setDoDoubleCheck(this.m_bDoDoubleCheck);
			writerW.setOutputReport(this.m_bOutputReport);
			if ( this.m_bOutputReport )
				writerW.writeReportHeader();
		}

		// Set double check flag for WURCSParser
		if ( this.m_formatInput  == ChemFormatType.WURCS
		  && this.m_formatOutput != ChemFormatType.WURCS )
			WURCSParser.doDoubleCheck = this.m_bDoDoubleCheck;
	}

	private void processIterative(Reader in, IChemObjectWriter writer) throws CDKException, IOException {
		IIteratingChemObjectReader<IAtomContainer> reader =
				this.m_formatInput.createIteratingReader(in, new AtomContainer().getBuilder());
		if ( reader == null )
			return;
		// Set relaxed mode
		if ( reader instanceof DefaultIteratingChemObjectReader<?> )
			((DefaultIteratingChemObjectReader<?>)reader).setReaderMode(Mode.RELAXED);
		// Skip error molecule is SDF
		if ( reader instanceof IteratingSDFReader )
			((IteratingSDFReader)reader).setSkip(true);

		if ( !reader.hasNext() ) {
			logger.warn("No molecule in input.");
			return;
		}

		while ( reader.hasNext() ) {
			IAtomContainer mol = reader.next();
			if ( mol == null )
				continue;

			setTitleAsProperty(mol);

			if ( ignoreEmptyResult(mol) )
				continue;

			try {
				if ( writer.accepts(mol.getClass()) )
					writer.write(mol);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		reader.close();
	}

	private void process(Reader in, IChemObjectWriter writer) throws CDKException, IOException {
		// Read molecule from input
		ISimpleChemObjectReader reader = this.m_formatInput.createReader();
		reader.setReader(in);

		// Convert to molecules
		IAtomContainerSet mols = new AtomContainerSet();
		if ( reader.accepts(AtomContainerSet.class) )
			mols = reader.read(mols);
		else if ( reader.accepts(AtomContainer.class) )
			mols.addAtomContainer( reader.read(new AtomContainer()) );
		reader.close();

		// Trim null molecules
		mols.removeAtomContainer(null);
		if ( mols.isEmpty() ) {
			logger.warn("No molecule in input.");
			return;
		}

		for ( IAtomContainer mol : mols.atomContainers() ) {
			if ( mol == null )
				continue;
			setTitleAsProperty(mol);
			if ( ignoreEmptyResult(mol) )
				mols.removeAtomContainer(mol);
		}

		if ( writer.accepts(mols.getClass()) )
			writer.write(mols);
		else if ( writer.accepts(mols.getAtomContainer(0).getClass()) ) {
			if ( mols.getAtomContainerCount() > 1 )
				logger.warn("The output format {} cannot handle two or more structures. Output only the first structure.",
						this.m_formatOutput.getFormatType());
			writer.write(mols.getAtomContainer(0));
		}

	}

	private void setTitleAsProperty(IAtomContainer mol) {
		if ( this.m_strPropID == null )
			return;
		// Do not overwrite current property - Important!
		if ( mol.getProperty(this.m_strPropID) != null )
			return;
		// Do nothing if no title
		if ( mol.getTitle() == null )
			return;
		// Set title as property
		mol.setProperty(this.m_strPropID, mol.getTitle());
	}

	private boolean ignoreEmptyResult(IAtomContainer mol) {
		if ( this.m_bOutputNoResult )
			return false;
		if ( !mol.isEmpty() )
			return false;
		return true;
	}
}
