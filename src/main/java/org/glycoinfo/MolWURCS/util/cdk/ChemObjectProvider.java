package org.glycoinfo.MolWURCS.util.cdk;

import org.openscience.cdk.Atom;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.silent.SilentChemObjectBuilder;

public class ChemObjectProvider {

	private static IChemObjectBuilder builder = SilentChemObjectBuilder.getInstance();

	public static void setBuilder(IChemObjectBuilder a_builder) {
		builder = a_builder;
	}

	public static IChemObjectBuilder getCurrentBuilder() {
		return builder;
	}

	public static IAtomContainer createAtomContainer() {
		return builder.newAtomContainer();
	}

	public static IAtom createAtom() {
		return builder.newAtom();
	}

	public static IAtom createAtom(String symbol) {
		IAtom atom = builder.newAtom();
		atom.setSymbol(symbol);
		return atom;
	}

	public static IAtom createAtom(Integer atomicNumber) {
		IAtom atom = builder.newAtom();
		atom.setAtomicNumber(atomicNumber);
		return atom;
	}

	public static IBond createBond(IAtom atom1, IAtom atom2, Order order) {
		IBond bond = builder.newBond();
		bond.setAtoms(new IAtom[] {atom1, atom2});
		bond.setOrder(order);
		return bond;
	}

	public static IAtomContainer createAlkylChain(int iLength) {
		if ( iLength < 1 )
			return null;
		IAtomContainer chain = builder.newAtomContainer();
		IAtom carbon = new Atom(6);
		chain.addAtom(carbon);
		for ( int i=1; i<iLength; i++ ) {
			IAtom carbonNew = new Atom(6);
			chain.addAtom(carbonNew);
			chain.addBond(i-1, i, Order.SINGLE);
		}
		return chain;
	}
}
