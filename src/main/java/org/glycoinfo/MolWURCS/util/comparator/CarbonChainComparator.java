package org.glycoinfo.MolWURCS.util.comparator;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.glycoinfo.MolWURCS.exchange.toWURCS.CarbonChainToBackbone;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.glycoinfo.MolWURCS.util.analysis.CarbonChainUtils;
import org.glycoinfo.WURCSFramework.util.graph.comparator.BackboneComparator;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;

public class CarbonChainComparator implements Comparator<CarbonChain> {

	private boolean m_bDoCheckAnomer;
	private BackboneComparator m_bbComp;

	private Map<CarbonChain, Backbone> m_mapCCToB;

	public CarbonChainComparator() {
		this.m_bDoCheckAnomer = true;
		this.m_bbComp = new BackboneComparator();

		this.m_mapCCToB = new HashMap<>();
	}

	public void doCheckAnomer(boolean a_bDoCheck) {
		this.m_bDoCheckAnomer = a_bDoCheck;
	}

	private Backbone getBackbone(CarbonChain a_cc) {
		if ( !this.m_mapCCToB.containsKey(a_cc) )
			this.m_mapCCToB.put(a_cc, CarbonChainToBackbone.convert(a_cc));
		return this.m_mapCCToB.get(a_cc);
	}

	@Override
	public int compare(CarbonChain cc1, CarbonChain cc2) {
		int iComp = 0;

		// Compare with anomeric state
		if ( this.m_bDoCheckAnomer ) {
			iComp = compareWithAnomer(cc1, cc2);
			if ( iComp != 0 )
				return iComp;
		}

		// Compare with Backbone
		Backbone bb1 = getBackbone(cc1);
		Backbone bb2 = getBackbone(cc2);
		return this.m_bbComp.compare(bb1, bb2);
	}

	/**
	 * Compare CarbonChains with anomeric state of carbons.
	 */
	private int compareWithAnomer(CarbonChain cc1, CarbonChain cc2) {
		int nLength1 = cc1.getCarbonUnits().size();
		int nLength2 = cc2.getCarbonUnits().size();

		// Prioritize anomeric carbon
		int nLengthShort = Math.min(nLength1, nLength2);
		for ( int i=0; i<nLengthShort; i++ ) {
			CarbonUnit cu1 = cc1.getCarbonUnit(i+1);
			CarbonUnit cu2 = cc2.getCarbonUnit(i+1);

			// For anomeric
			boolean isAnomeric1 = CarbonChainUtils.isAnomeric(cu1, cc1);
			boolean isAnomeric2 = CarbonChainUtils.isAnomeric(cu2, cc2);

			if (  isAnomeric1 && !isAnomeric2 )
				return -1;
			if ( !isAnomeric1 &&  isAnomeric2 )
				return 1;

			// For potentially anomeric
			boolean isPotentiallyAnomeric1 = CarbonChainUtils.isPotentiallyAnomeric(cu1);
			boolean isPotentiallyAnomeric2 = CarbonChainUtils.isPotentiallyAnomeric(cu2);

			if (  isPotentiallyAnomeric1 && !isPotentiallyAnomeric2 )
				return -1;
			if ( !isPotentiallyAnomeric1 &&  isPotentiallyAnomeric2 )
				return 1;

			// TODO: Add ring priority

		}
		return 0;
	}
}
