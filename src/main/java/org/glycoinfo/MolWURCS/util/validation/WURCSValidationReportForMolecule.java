package org.glycoinfo.MolWURCS.util.validation;

import org.glycoinfo.WURCSFramework.util.validation.WURCSValidationReport;

public class WURCSValidationReportForMolecule extends WURCSValidationReport {

	private static enum ConversionType implements Type {
		INCOMPATIBLE("Incompatible");

		private String name;
		private ConversionType(String name) {
			this.name = name;
		}
		@Override
		public String getName() {
			return this.name;
		}
		
	}

	public void addIncompatible(String a_strMessage) {
		addReport(a_strMessage, ConversionType.INCOMPATIBLE, null, null);
	}

	public void addIncompatible(String a_strMessage, String a_strTargetInfo) {
		addReport(a_strMessage, ConversionType.INCOMPATIBLE, a_strTargetInfo, null);
	}

	public void addIncompatible(String a_strMessage, String a_strTargetInfo, Exception e) {
		addReport(a_strMessage, ConversionType.INCOMPATIBLE, a_strTargetInfo, e);
	}

	public boolean hasIncompatible() {
		return hasTypedReports(ConversionType.INCOMPATIBLE);
	}


}
