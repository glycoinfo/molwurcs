package org.glycoinfo.MolWURCS.util.validation;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.WURCSFramework.util.WURCSStringUtils;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorCollectConnectingBackboneGroups;
import org.glycoinfo.WURCSFramework.util.graph.visitor.WURCSVisitorException;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.BackboneCarbon;
import org.glycoinfo.WURCSFramework.wurcs.graph.ICarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.graph.InterfaceRepeat;
import org.glycoinfo.WURCSFramework.wurcs.graph.LinkagePosition;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.glycoinfo.WURCSFramework.wurcs.graph.ModificationAlternative;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSEdge;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;

public class WURCSValidatorForMolecule extends WURCSValidator {

	@Override
	public WURCSValidationReportForMolecule getReport() {
		return (WURCSValidationReportForMolecule)super.getReport();
	}

	@Override
	protected WURCSValidationReportForMolecule getNewReport() {
		return new WURCSValidationReportForMolecule();
	}

	@Override
	protected void validateGraph(WURCSGraph a_graph) throws WURCSVisitorException {
		// Check # of backbone groups
		WURCSVisitorCollectConnectingBackboneGroups t_oGroup = new WURCSVisitorCollectConnectingBackboneGroups();
		t_oGroup.start(a_graph);
		if (t_oGroup.getBackboneGroups().size() > 1)
			this.getReport().addIncompatible("All backbones must be connected in a molecule.", this.getWURCSFactory().getWURCS());

		// Do super
		super.validateGraph(a_graph);
	}

	@Override
	protected void validateBackbone(Backbone a_bb) {
		// Do super
		super.validateBackbone(a_bb);

		String strMS = this.getWURCSFactory().getInfo(a_bb).getMSString();

		// For carbons
		if ( a_bb.hasUnknownLength() )
			this.getReport().addIncompatible("The backbone with unknown carbon length cannot be converted to molecule.", strMS);
		// Check anomeric configuration
		else if ( a_bb.getAnomericPosition() > 0 ) {
			int iAnomPos = a_bb.getAnomericPosition();
			char cAnomSymbol = a_bb.getAnomericSymbol();
			if ( cAnomSymbol == 'a' || cAnomSymbol == 'b' ) {
				// Check stereochemistry on anomeric reference atom
				BackboneCarbon bcAnomRef = null;
				int i=0;
				for ( BackboneCarbon bc : a_bb.getBackboneCarbons() ) {
					if ( a_bb.getBackboneCarbons().indexOf(bc)+1 == iAnomPos )
						continue;
					if ( !bc.getDescriptor().isChiral() )
						continue;
					i++;
					if ( i!=4 )
						continue;
					bcAnomRef = bc;
				}
				if ( bcAnomRef != null ) {
					int iPosAnomSymbol = a_bb.getBackboneCarbons().size()+3;
					int iPosAnomRef = a_bb.getBackboneCarbons().indexOf(bcAnomRef)+1;
					if ( bcAnomRef.getDescriptor().getStereo().equals(ICarbonDescriptor.X) )
						this.getReport().addIncompatible("The anomeric configuration with unknown configuration of anomeric reference atom cannot be converted to molecule.",
							WURCSStringUtils.highlight(strMS, iAnomPos, iPosAnomRef, iPosAnomSymbol));
				}
			}
		}

		List<Integer> lPosRelativeStereos = new ArrayList<>();
		for ( int i=0; i<a_bb.getBackboneCarbons().size(); i++ ) {
			BackboneCarbon t_bc = a_bb.getBackboneCarbons().get(i);

			if ( t_bc.getDescriptor().getHybridOrbital().equals(ICarbonDescriptor.SPX) )
				this.getReport().addIncompatible("The carbon with unknown chemical state cannot be converted to molecule.",
					WURCSStringUtils.highlight(strMS, a_bb.getBackboneCarbons().indexOf(t_bc)+1));

			if ( t_bc.getDescriptor().getStereo() != null ) {
				String strStereo = t_bc.getDescriptor().getStereo();
				// Do not allow relative stereochemistry
				if ( strStereo.equals(ICarbonDescriptor.r) || strStereo.equals(ICarbonDescriptor.s) )
					lPosRelativeStereos.add(i+1);
			}
		}
		if ( !lPosRelativeStereos.isEmpty() ) {
			this.getReport().addIncompatible("The carbon(s) with relative stereochemistry cannot be converted to molecule.",
				WURCSStringUtils.highlight(strMS, lPosRelativeStereos));
		}

		// For edges
		for ( WURCSEdge t_edge : a_bb.getEdges() ) {
			String strMod = this.getWURCSFactory().getInfo( t_edge.getModification() ).getString();
			if ( t_edge.getLinkages().size() > 1 ) {
				this.getReport().addIncompatible("The alternative linkage positions cannot be converted to molecule.", strMod);
				continue;
			}
			LinkagePosition link = t_edge.getLinkages().get(0);
			if ( link.getBackbonePosition() < 0 )
				this.getReport().addIncompatible("The unknown linkage positions cannot be converted to molecule.", strMod);
			if ( link.getProbabilityLower() < 1.0D )
				this.getReport().addIncompatible("The linkage with probability cannot be converted to molecule.", strMod);
		}
	}

	@Override
	protected void validateModification(Modification a_mod) {
		// Do super
		super.validateModification(a_mod);

		String strMod = this.getWURCSFactory().getInfo(a_mod).getString();
		// For any repeating unit 
		if ( a_mod instanceof InterfaceRepeat )
			this.getReport().addIncompatible("The repeating unit cannot be converted to molecule.", strMod);

		// For any alternative linkages
		if ( a_mod instanceof ModificationAlternative )
			this.getReport().addIncompatible("The alternative modification cannot be converted to molecule.", strMod);
	}
}
