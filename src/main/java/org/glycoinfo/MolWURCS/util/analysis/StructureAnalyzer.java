package org.glycoinfo.MolWURCS.util.analysis;

import java.util.HashSet;
import java.util.Set;

import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomType.Hybridization;
import org.openscience.cdk.interfaces.IRingSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for molecule structure analysis to find aromatic, pi cyclic and carbon cyclic atoms and terminal carbons.
 * All atoms must be perceived atom types before using this class.
 * @author MasaakiMatsubara
 *
 */
public class StructureAnalyzer {

	private static Logger logger = LoggerFactory.getLogger(StructureAnalyzer.class);

	private Set<IAtom> m_aPiCyclicAtoms;
	private Set<IAtom> m_aCarbonCyclicAtoms;

	public StructureAnalyzer(IAtomContainer a_oMol) {
		this.m_aPiCyclicAtoms     = new HashSet<>();
		this.m_aCarbonCyclicAtoms = new HashSet<>();
		this.analyze(a_oMol);
	}

	/**
	 * Returns pi cyclic atoms, the member of a ring which all atoms have pi electron(s).
	 * @return Set of pi cyclic atoms
	 */
	public Set<IAtom> getPiCyclicAtoms() {
		return this.m_aPiCyclicAtoms;
	}

	/**
	 * Returns carbon cyclic atoms. The member of a ring which all atoms are carbon.
	 * @return Set of carbon cyclic atoms
	 */
	public Set<IAtom> getCarbonCyclicAtoms() {
		return this.m_aCarbonCyclicAtoms;
	}

	/**
	 * Structure analyze for molecule.
	 * Search and collect atoms of aromatic ring, pi ring, carbon ring and terminal carbons.
	 * @param a_objMol Molecule object for analyze
	 */
	private void analyze(IAtomContainer a_oMol) {
		logger.debug("Start structure analysis for {} ...", a_oMol.getTitle());

		// Collect SSSR (smallest set of smallest rings)
		Cycles cycles = Cycles.mcb(a_oMol);
		IRingSet rings = cycles.toRingSet();

		// Collect ring atoms with aromatic, pi cyclic or carbon cyclic
		for ( IAtomContainer ring : rings.atomContainers() ) {
			boolean isPiCyclic = isPiCyclic(ring);
			boolean isCarbonCyclic = isCarbonCyclic(ring);

			if ( isPiCyclic )
				for ( IAtom atom : ring.atoms() )
					this.m_aPiCyclicAtoms.add(atom);

			if ( isCarbonCyclic )
				for ( IAtom atom : ring.atoms() )
					this.m_aCarbonCyclicAtoms.add(atom);
		}
		if ( !this.m_aPiCyclicAtoms.isEmpty() )
			logger.debug("{} pi cyclic atoms are found.", this.m_aPiCyclicAtoms.size());
		if ( !this.m_aCarbonCyclicAtoms.isEmpty() )
			logger.debug("{} cyclic carbons are found.", this.m_aCarbonCyclicAtoms.size());

		logger.debug("End structure analysis...");
	}

	private static boolean isCarbonCyclic(IAtomContainer a_ring) {
		for ( IAtom atom : a_ring.atoms() )
			if ( atom.getAtomicNumber() != 6 ) // Carbon
				return false;
		return true;
	}

	private static boolean isPiCyclic(IAtomContainer a_ring) {
		for ( IAtom atom : a_ring.atoms() )
			if ( atom.getHybridization() != Hybridization.SP2
			  && atom.getHybridization() != Hybridization.SP1
			  && atom.getHybridization() != Hybridization.PLANAR3)
				return false;
		return true;
	}

}
