package org.glycoinfo.MolWURCS.util.analysis;

import java.util.ArrayList;
import java.util.List;

import org.openscience.cdk.AtomRef;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.geometry.cip.CIPTool;
import org.openscience.cdk.geometry.cip.CIPTool.CIP_CHIRALITY;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry;
import org.openscience.cdk.interfaces.IStereoElement;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.stereo.DoubleBondStereochemistry;
import org.openscience.cdk.stereo.Stereocenters;
import org.openscience.cdk.stereo.TetrahedralChirality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StereochemistryUtils {

	private static final Logger logger = LoggerFactory.getLogger(StereochemistryUtils.class);

	/**
	 * Label CIP stereo descriptor to each atom in the given molecule.
	 * {@link #kekulize(IAtomContainer)} must be performed earlier.
	 * @param a_oMol
	 */
	public static void labelCIPDescriptor(IAtomContainer a_oMol) {
		// Label cip info to atoms and bonds
		CIPTool.label(a_oMol);
		// For unknown stereochemistry
		Stereocenters scs = Stereocenters.of(a_oMol);
		for ( IAtom atom : a_oMol.atoms() ) {
			if ( atom.getProperty(CDKConstants.CIP_DESCRIPTOR) != null )
				continue;
			if ( !scs.isStereocenter( a_oMol.indexOf(atom) ) )
				continue;
			if ( scs.elementType(a_oMol.indexOf(atom)) != Stereocenters.Type.Tetracoordinate )
				continue;
			atom.setProperty(CDKConstants.CIP_DESCRIPTOR, "X");
		}
		for ( IBond bond : a_oMol.bonds() ) {
			if ( bond.getOrder() != IBond.Order.DOUBLE )
				continue;
			if ( bond.getProperty(CDKConstants.CIP_DESCRIPTOR) != null )
				continue;
			if ( !scs.isStereocenter( a_oMol.indexOf(bond.getBegin()) )
			  || !scs.isStereocenter( a_oMol.indexOf(bond.getEnd()  ) ) )
				continue;
			if ( scs.elementType(a_oMol.indexOf(bond.getBegin())) != Stereocenters.Type.Tricoordinate
			  && scs.elementType(a_oMol.indexOf(bond.getEnd()  )) != Stereocenters.Type.Tricoordinate )
				continue;
			bond.setProperty(CDKConstants.CIP_DESCRIPTOR, "X");
		}
	}

	/**
	 * Returns StereoElements built from CIP descriptor
	 * labeled on atoms and bonds of the given molecule.
	 * @param a_oMol
	 */
	public static List<IStereoElement> getStereoElementsFromLabeledCIPDescriptor(IAtomContainer a_oMol) {
		List<IStereoElement> stereos = new ArrayList<>();
		for ( IAtom atom : a_oMol.atoms() ) {
			if ( atom.getProperty(CDKConstants.CIP_DESCRIPTOR) == null )
				continue;
			String strCIP = atom.getProperty(CDKConstants.CIP_DESCRIPTOR);
			CIP_CHIRALITY cip = toCIPChiral(strCIP);
			// Add TetrahedralChirality
			if ( cip != CIP_CHIRALITY.NONE ) {
				ITetrahedralChirality chiral = getChirality(a_oMol, atom, cip);
				stereos.add(chiral);
			}
			// Remove label
			atom.removeProperty(CDKConstants.CIP_DESCRIPTOR);
		}
		for ( IBond bond : a_oMol.bonds() ) {
			if ( bond.getOrder() != IBond.Order.DOUBLE )
				continue;
			if ( bond.getProperty(CDKConstants.CIP_DESCRIPTOR) == null )
				continue;
			String strCIP = bond.getProperty(CDKConstants.CIP_DESCRIPTOR);
			CIP_CHIRALITY cip = toCIPChiral(strCIP);
			if ( cip != CIP_CHIRALITY.NONE ) {
				IDoubleBondStereochemistry dbStereo = getDBStereo(a_oMol, bond, cip);
				stereos.add(dbStereo);
			}
			// Remove label
			bond.removeProperty(CDKConstants.CIP_DESCRIPTOR);
		}
		return stereos;
	}

	private static CIP_CHIRALITY toCIPChiral(String a_stereo) {
		switch(a_stereo) {
		case "R":
			return CIP_CHIRALITY.R;
		case "S":
			return CIP_CHIRALITY.S;
		case "Z":
			return CIP_CHIRALITY.Z;
		case "E":
			return CIP_CHIRALITY.E;
		default:
			return CIP_CHIRALITY.NONE;
		}
	}


	private static ITetrahedralChirality getChirality(IAtomContainer a_mol, IAtom a_atom, CIP_CHIRALITY a_cip) {
		IAtom[] ligands = a_mol.getConnectedAtomsList(a_atom).toArray(new IAtom[4]);
		// Fill center atom for implicit hydrogen
		for ( int i=0; i<4; i++ ) {
			if ( ligands[i] == null )
				ligands[i] = a_atom;
			if ( ligands[i] instanceof AtomRef )
				ligands[i] = ((AtomRef)ligands[i]).deref();
		}
		// Try first
		ITetrahedralChirality chiral = new TetrahedralChirality(
				a_atom, ligands, ITetrahedralChirality.Stereo.CLOCKWISE
			);
		CIP_CHIRALITY cipCulc = CIPTool.getCIPChirality(a_mol, chiral);

		if ( a_cip == cipCulc )
			return chiral;

		// Try second
		chiral = new TetrahedralChirality(
				a_atom, ligands, ITetrahedralChirality.Stereo.ANTI_CLOCKWISE
			);
		cipCulc = CIPTool.getCIPChirality(a_mol, chiral);
		if ( a_cip == cipCulc )
			return chiral;
		logger.error("The stereo cannot be set to the atom.");
		return null;
	}

	private static IDoubleBondStereochemistry getDBStereo(IAtomContainer a_mol, IBond a_bond, CIP_CHIRALITY a_cip) {
		IBond[] ligandBonds = new IBond[2];
		IAtom atom = a_bond.getBegin();
		for ( IBond bondConn : a_mol.getConnectedBondsList(atom) ) {
			if ( bondConn.equals(a_bond) )
				continue;
			ligandBonds[0] = bondConn;
			break;
		}
		atom = a_bond.getEnd();
		for ( IBond bondConn : a_mol.getConnectedBondsList(atom) ) {
			if ( bondConn.equals(a_bond) )
				continue;
			ligandBonds[1] = bondConn;
			break;
		}

		if ( ligandBonds[0] == null || ligandBonds[1] == null ) {
			logger.error("The double bond must have a bond for each side to have stereochemistry.");
			return null;
		}

		// Try first
		IDoubleBondStereochemistry dbStereo = new DoubleBondStereochemistry(
				a_bond, ligandBonds, IDoubleBondStereochemistry.Conformation.TOGETHER
			);
		CIP_CHIRALITY cipCulc = CIPTool.getCIPChirality(a_mol, dbStereo);
		if ( a_cip == cipCulc )
			return dbStereo;

		// Try second
		dbStereo = new DoubleBondStereochemistry(
				a_bond, ligandBonds, IDoubleBondStereochemistry.Conformation.OPPOSITE
			);
		cipCulc = CIPTool.getCIPChirality(a_mol, dbStereo);
		if ( a_cip == cipCulc )
			return dbStereo;

		logger.error("The stereo cannot be set to the bond.");
		return null;
	}

}
