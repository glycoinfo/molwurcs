package org.glycoinfo.MolWURCS.util.analysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

public class AtomChainExtractor {

	public static class AtomChain {
		private int m_id;
		private LinkedList<IAtom> m_lMainChain;
		private List<AtomChain> m_lSubChains;

		private AtomChain(int a_id, LinkedList<IAtom> a_lMain, List<AtomChain> a_lSubs) {
			this.m_id = a_id;
			this.m_lMainChain = a_lMain;
			this.m_lSubChains = a_lSubs;
		}

		public int getID() {
			return this.m_id;
		}

		public LinkedList<IAtom> getMainChain() {
			return this.m_lMainChain;
		}

		public List<AtomChain> getSubChains() {
			return this.m_lSubChains;
		}
	}

	private Set<Integer> m_lTargetAtomNums;

	public AtomChainExtractor() {
		this.m_lTargetAtomNums = new HashSet<>();
	}

	public void setTargetAtomicNumbers(int... a_iAtomicNumbers) {
		for ( int atNum : a_iAtomicNumbers )
			this.m_lTargetAtomNums.add(atNum);
	}

	private List<LinkedList<IAtom>> searchChains(final LinkedList<IAtom> chain, List<IAtom> terminals, Set<IAtom> ignores) {
		List<LinkedList<IAtom>> t_aCCList = new ArrayList<>();
		IAtom tailAtom = chain.getLast();
		if ( !this.m_lTargetAtomNums.contains( tailAtom.getAtomicNumber() ) )
			return t_aCCList;
		if ( ignores.contains( tailAtom ) )
			return t_aCCList;

		// Add chain to list if tail atom is terminal atom and the length is not one.
		if ( chain.size() > 1 && terminals.contains(tailAtom) ) {
			t_aCCList.add( new LinkedList<>(chain) );
			return t_aCCList;
		}

		// Depth search
		for ( IBond bond : tailAtom.bonds() ) {
			IAtom nextAtom = bond.getOther(tailAtom);
			if ( chain.contains(nextAtom) )
				continue;
			chain.addLast(nextAtom);
			t_aCCList.addAll( searchChains(chain, terminals, ignores) );
			chain.removeLast();
		}
		return t_aCCList;
	}

	public LinkedList<AtomChain> extractAtomChain(IAtomContainer a_mol) {
		return extractAtomChain(a_mol, new HashSet<>());
	}

	public LinkedList<AtomChain> extractAtomChain(IAtomContainer a_mol, Set<IAtom> a_setIgnoreAtoms) {
		LinkedList<AtomChain> lChains = new LinkedList<>();
		if ( this.m_lTargetAtomNums.isEmpty() )
			return lChains;

		// Collect terminal atoms
		List<IAtom> lTerminalAtoms = new ArrayList<>();
		for ( IAtom atom : a_mol.atoms() ) {
			if ( !this.m_lTargetAtomNums.contains( atom.getAtomicNumber() ) )
				continue;
			if ( a_setIgnoreAtoms.contains( atom ) )
				continue;
			int nConnTarget = 0;
			for ( IAtom atomConn : a_mol.getConnectedAtomsList(atom) )
				if ( this.m_lTargetAtomNums.contains( atomConn.getAtomicNumber() ) )
					nConnTarget++;
			if ( nConnTarget == 1 )
				lTerminalAtoms.add(atom);
		}
		if ( lTerminalAtoms.isEmpty() )
			return lChains;

		// Collect chains
		return collectAtomChain(lTerminalAtoms, lTerminalAtoms, a_setIgnoreAtoms);
	}

	private LinkedList<AtomChain> collectAtomChain(List<IAtom> terminalsStart, List<IAtom> terminals, Set<IAtom> ignores) {
		int id = 1;
		LinkedList<AtomChain> lChains = new LinkedList<>();
		for ( IAtom atomTerminal : terminalsStart ) {
			LinkedList<IAtom> chainStart = new LinkedList<>();
			chainStart.add(atomTerminal);
			for ( LinkedList<IAtom> chainMain : searchChains(chainStart, terminals, ignores) ) {
				LinkedList<AtomChain> chainsSub = searchBranches(chainMain, terminals, ignores);
				AtomChain ac = new AtomChain(id++, chainMain, chainsSub);
				lChains.add(ac);
			}
		}
		return lChains;
	}

	private LinkedList<AtomChain> searchBranches(LinkedList<IAtom> chainMain, List<IAtom> terminals, Set<IAtom> ignores) {
		// Find branch points
		List<IAtom> terminalsBranch = new ArrayList<>();
		for ( IAtom atom : chainMain ) {
			for ( IBond bond : atom.bonds() ) {
				IAtom atomConn = bond.getOther(atom);
				if ( chainMain.contains(atomConn) )
					continue;
				if ( !this.m_lTargetAtomNums.contains( atomConn.getAtomicNumber() ) )
					continue;
				if ( ignores.contains(atom) )
					continue;
				terminalsBranch.add(atomConn);
			}
		}

		// Add main chain to ignores
		Set<IAtom> ignoresNew = new HashSet<>(ignores);
		ignoresNew.addAll(chainMain);
		// Add start atoms for branch to terminals
		List<IAtom> terminalsNew = new ArrayList<IAtom>(terminals);
		terminalsNew.addAll(terminalsBranch);

		LinkedList<AtomChain> lBranches = collectAtomChain(terminalsBranch, terminalsNew, ignoresNew);
		return lBranches;
	}
}
