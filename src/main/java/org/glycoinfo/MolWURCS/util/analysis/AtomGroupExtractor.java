package org.glycoinfo.MolWURCS.util.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

public class AtomGroupExtractor {

	private Set<Integer> m_lTargetAtomNums;
	private Set<IAtom> m_setIgnoreAtoms;
	private int m_nMinAtoms;

	public AtomGroupExtractor() {
		this.m_lTargetAtomNums = new HashSet<>();
		this.m_setIgnoreAtoms = new HashSet<>();
		this.m_nMinAtoms = 0;
	}

	public void addTargetAtomicNumbers(int... a_iAtomicNumbers) {
		for ( int iAtomNum : a_iAtomicNumbers )
			this.m_lTargetAtomNums.add(iAtomNum);
	}

	public void addIgnoreAtoms(Collection<IAtom> a_setIgnoreAtoms) {
		if ( a_setIgnoreAtoms == null )
			return;
		this.m_setIgnoreAtoms.addAll(a_setIgnoreAtoms);
	}

	public void setMinimumAtomCount(int a_nMinAtoms) {
		this.m_nMinAtoms = a_nMinAtoms;
	}

	public boolean isTargetAtom(IAtom atom) {
		if ( !this.m_lTargetAtomNums.contains( atom.getAtomicNumber() ) )
			return false;
		if ( this.m_setIgnoreAtoms.contains(atom) )
			return false;
		return true;
	}

	public List<IAtomContainer> extract(IAtomContainer a_mol) {
		List<IAtomContainer> lGroups = new ArrayList<>();
		if ( this.m_lTargetAtomNums.isEmpty() )
			return lGroups;

		Set<IAtom> setGroupedAtoms = new HashSet<>();

		for ( IAtom atom : a_mol.atoms() ) {
			if ( !isTargetAtom(atom) )
				continue;
			if ( setGroupedAtoms.contains(atom) )
				continue;

			IAtomContainer group = getConnectingGraph(atom, a_mol);

			for ( IAtom atomGrouped : group.atoms() )
				setGroupedAtoms.add(atomGrouped);

			// Do not collect if the atom count is lower than minimum threshold
			if ( group.getAtomCount() < this.m_nMinAtoms )
				continue;

			lGroups.add(group);
		}

		return lGroups;
	}

	private IAtomContainer getConnectingGraph(IAtom a_atomStart, IAtomContainer a_mol) {
		IAtomContainer molSub = a_mol.getBuilder().newAtomContainer();
		molSub.addAtom(a_atomStart);
		LinkedList<IAtom> lConnAtoms = new LinkedList<>();
		lConnAtoms.add(a_atomStart);
		while ( !lConnAtoms.isEmpty() ) {
			IAtom atom = lConnAtoms.removeFirst();
			for ( IBond bond : a_mol.getConnectedBondsList(atom) ) {
				IAtom atomConn = bond.getOther(atom);
				if ( !isTargetAtom(atomConn) )
					continue;
				if ( !molSub.contains(atomConn) ) {
					lConnAtoms.add(atomConn);
					molSub.addAtom(atomConn);
				}
				if ( !molSub.contains(bond) )
					molSub.addBond(bond);
			}
		}
		return molSub;
	}
}
