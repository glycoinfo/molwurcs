package org.glycoinfo.MolWURCS.util.analysis;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonUnit;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomType.Hybridization;
import org.openscience.cdk.interfaces.IBond.Order;

public class CarbonChainUtils {

	private static final int[] NOS = {7, 8, 16};

	public static int countCarbonsConnectedNOS(CarbonChain a_cc){
		int nNOS = 0;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			if ( cu.countModBondsWith(null, NOS) > 0 )
				nNOS++;
		}
		return nNOS;
	}

	public static int countCarbonsConnectingAtomsOxygen(CarbonChain a_cc) {
		int num = 0;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			// Ignore carboxy group
			if ( isCarboxyLike(cu) )
				continue;
			if ( cu.countModBondsWith(null, 8) > 0 )
				num++;
		}
		return num;
	}

	/**
	 * Returns list of CarbonUnits which form ring in the specified CarbonChain
	 * with the specified CarbonUnit through a modification atom.
	 * @param a_cu CarbonUnit to be checked
	 * @param a_cc CarbonChain containing a_cu
	 * @return List of CarbonUnits connecting the specified CarbonUnit with an atom
	 */
	public static List<CarbonUnit> getRingedCarbonUnits(CarbonUnit a_cu, CarbonChain a_cc) {
		List<CarbonUnit> lConnCUs = new ArrayList<>();
		if ( a_cu == null )
			return lConnCUs;
		for ( CarbonUnit cu : a_cc.getCarbonUnits() ) {
			if ( a_cu.equals(cu) )
				continue;
			for ( IAtom modAtom : a_cu.getModficationAtoms() ) {
				if ( !cu.getModficationAtoms().contains(modAtom) )
					continue;
				lConnCUs.add(cu);
			}
		}
		return lConnCUs;
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * alcohol group (-C-OR or -C(OR)-).
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * alcohol group (-C-OR or -C(OR)-). {@code false} otherwise.
	 */
	public static boolean isAlcoholic(CarbonUnit a_cu) {
		int num1O = a_cu.countModBondsWith(Order.SINGLE, 8);
		int numOther = a_cu.getModBondsCount() - num1O;
		return (num1O == 1 && numOther == 0);
	}

	public static boolean isDeoxy(CarbonUnit a_cu) {
		int numOther = a_cu.getModBondsCount();
		return (numOther == 0);
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * hydrocarbon, which has no modification other than carbon and hydrogen.
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * hydrocarbon. {@code false} otherwise.
	 */
	public static boolean isHydrocarbon(CarbonUnit a_cu) {
		int numC = a_cu.countModBondsWith(null, 6);
		int numOther = a_cu.getModBondsCount() - numC;
		return (numOther == 0);
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * (hemi)acetal group (R1-C(X1)(X2)-H). X1 and X2 are atoms except for C and H.
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * (hemi)acetal group. {@code false} otherwise.
	 */
	public static boolean isAcetalLike(CarbonUnit a_cu) {
		// Terminal
		if ( !a_cu.isTerminal() )
			return false;
		if ( a_cu.getCenterAtom().getHybridization() != Hybridization.SP3 )
			return false;
		int num1Mod  = a_cu.countModBondsWith(Order.SINGLE, true, 1, 6);
		int numOther = a_cu.getModBondsCount() - num1Mod;
		return ( num1Mod == 2 && numOther == 0 );
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * aldehyde group (R1-C(=X)-H). X is an atom except for C and H.
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * aldehyde group. {@code false} otherwise.
	 */
	public static boolean isAldehydeLike(CarbonUnit a_cu) {
		// Terminal
		if ( !a_cu.isTerminal() )
			return false;
		int num2Mod  = a_cu.countModBondsWith(Order.DOUBLE, true, 1, 6);
		int numOther = a_cu.getModBondsCount() - num2Mod;
		return ( num2Mod == 1 && numOther == 0 );
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * (hemi)ketal group (R1-C(X1)(X2)-R2). X1 and X2 are atoms except for C and H.
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * (hemi)ketal group. {@code false} otherwise.
	 */
	public static boolean isKetalLike(CarbonUnit a_cu){
		// Non-terminal
		if ( a_cu.isTerminal() )
			return false;
		if ( a_cu.getCenterAtom().getHybridization() != Hybridization.SP3 )
			return false;
		int num1Mod  = a_cu.countModBondsWith(Order.SINGLE, true, 1, 6);
		int numOther = a_cu.getModBondsCount() - num1Mod;
		return ( num1Mod == 2 && numOther == 0 );
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * ketone group (R1-C(=X)-R2). X is an atom except for C and H.
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * ketone group. {@code false} otherwise.
	 */
	public static boolean isKetoneLike(CarbonUnit a_cu){
		// Non-terminal
		if ( a_cu.isTerminal() )
			return false;
		int num2Mod  = a_cu.countModBondsWith(Order.DOUBLE, true, 1, 6);
		int numOther = a_cu.getModBondsCount() - num2Mod;
		return (num2Mod==1 && numOther == 0 );
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * carboxy group (R1-C(=X1)-X2). X1 and X2 are atoms except for C and H.
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * carboxy group. {@code false} otherwise.
	 */
	public static boolean isCarboxyLike(CarbonUnit a_cu) {
		// Terminal
		if ( !a_cu.isTerminal() )
			return false;
		int num1Mod  = a_cu.countModBondsWith(Order.SINGLE, true, 1, 6);
		int num2Mod  = a_cu.countModBondsWith(Order.DOUBLE, true, 1, 6);
		int numOther = a_cu.getModBondsCount() - num1Mod - num2Mod;
		return ( num1Mod == 1 && num2Mod == 1 && numOther == 0 );
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * acetal or ketal group, and has a ring connection to the other carbon.
	 * @param a_cu CarbonUnit indicating target carbon
	 * @param a_cc CarbonChain containing {@code a_cu}
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * acetal or ketal group, and has a ring connection to the other carbon.
	 * {@code false} otherwise.
	 * @see #isAcetalLike(CarbonUnit)
	 * @see #isKetalLike(CarbonUnit)
	 * @see #getRingedCarbonUnit(CarbonUnit, CarbonChain)
	 */
	public static boolean isAnomeric(CarbonUnit a_cu, CarbonChain a_cc) {
		if ( !a_cc.getCarbonUnits().contains(a_cu) )
			return false;
		if ( !isAcetalLike(a_cu) && !isKetalLike(a_cu) )
			return false;
//		if ( getRingedCarbonUnits(a_cu, a_cc).size() != 1 )
		if ( getRingedCarbonUnits(a_cu, a_cc).isEmpty() )
			return false;
		return true;
	}

	/**
	 * Returns {@code true} if the CarbonUnit seems to be a center carbon of
	 * aldehyde or ketone group.
	 * @param a_cu CarbonUnit indicating target carbon
	 * @return {@code true} if the CarbonUnit seems to be a center carbon of
	 * aldehyde or ketone group, or {@code false} otherwise.
	 * @see #isAldehydeLike(CarbonUnit)
	 * @see #isKetoneLike(CarbonUnit)
	 */
	public static boolean isPotentiallyAnomeric(CarbonUnit a_cu) {
		if ( isCarboxyLike(a_cu) )
			return false;
		return isAldehydeLike(a_cu) || isKetoneLike(a_cu);
	}
}
