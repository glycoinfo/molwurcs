package org.glycoinfo.MolWURCS.util.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.aromaticity.Aromaticity;
import org.openscience.cdk.aromaticity.ElectronDonation;
import org.openscience.cdk.aromaticity.Kekulization;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.config.Elements;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.ConnectivityChecker;
import org.openscience.cdk.graph.CycleFinder;
import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IStereoElement;
import org.openscience.cdk.stereo.Projection;
import org.openscience.cdk.stereo.StereoElementFactory;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for normalize molecule:
 * remove metal, omit charge, omit isotope and add hidden hydrogens
 * @author MasaakiMatsubara
 */
public class MoleculeNormalizer {

	private static Logger logger = LoggerFactory.getLogger(MoleculeNormalizer.class);

	private boolean m_doRemoveMetalAtoms;
	private boolean m_doNeutralizeChargedBond;
	private boolean m_doOmitCharge;
	private boolean m_doRestoreCharge;
	private boolean m_doCheckTypedAtom;
	private boolean m_doKekulize;
	private boolean m_doResetStereo;
	private boolean m_doAromatize;

	private List<IAtomContainer> m_mols;

	private static Aromaticity aromaticity;
	static {
		ElectronDonation model = ElectronDonation.daylight();
		CycleFinder cycles = Cycles.or(Cycles.all(), Cycles.all(5));
		aromaticity = new Aromaticity(model, cycles);
	}

	private static CDKAtomTypeMatcher matcher;
	static {
		matcher = CDKAtomTypeMatcher.getInstance(
			DefaultChemObjectBuilder.getInstance()
		);
	}

	public MoleculeNormalizer() {
		this.m_doRemoveMetalAtoms = true;
		this.m_doNeutralizeChargedBond = true;
		this.m_doOmitCharge = true;
		this.m_doRestoreCharge = false;
		this.m_doCheckTypedAtom = false;
		this.m_doKekulize = true;
		this.m_doAromatize = false;
		this.m_doResetStereo = true;

		this.m_mols = new ArrayList<>();
	}

	public void setDoRemoveMetalAtoms(boolean a_doRemoveMetalAtoms) {
		this.m_doRemoveMetalAtoms = a_doRemoveMetalAtoms;
	}

	public void setDoNeutralizeChargedBond(boolean a_doNeutralizeChargedBond) {
		this.m_doNeutralizeChargedBond = a_doNeutralizeChargedBond;
	}

	public void setDoOmitCharge(boolean a_doOmitCharge) {
		this.m_doOmitCharge = a_doOmitCharge;
	}

	public void setDoRestoreCharge(boolean a_doRestoreCharge) {
		this.m_doRestoreCharge = a_doRestoreCharge;
	}

	public void setDoCheckTypedAtom(boolean a_doCheckTypedAtom) {
		this.m_doCheckTypedAtom = a_doCheckTypedAtom;
	}

	public void setDoKekulize(boolean a_doKekulize) {
		this.m_doKekulize = a_doKekulize;
	}

	public void setDoAromatize(boolean a_doAromatize) {
		this.m_doAromatize = a_doAromatize;
	}

	public void setDoResetStereo(boolean a_doResetStereo) {
		this.m_doResetStereo = a_doResetStereo;
	}

	public List<IAtomContainer> getMolecules() {
		return this.m_mols;
	}

	/**
	 * Normalizes molecule.
	 * @param a_mol IAtomContainer to be normalized
	 * @return {@code true} if normalization is finished without error
	 */
	public boolean normalize(IAtomContainer a_mol) {
		if ( this.m_doRemoveMetalAtoms )
			removeMetalAtoms(a_mol);
		if ( this.m_doNeutralizeChargedBond )
			neutralizeChargedBond(a_mol);
		if ( this.m_doOmitCharge )
			omitCharge(a_mol);
		if ( this.m_doRestoreCharge )
			restoreCharge(a_mol);

		for ( IAtomContainer mol : separate(a_mol) ) {
			if ( this.m_doCheckTypedAtom )
				if ( !checkTypedAtom(a_mol) )
				return false;
			if ( !perceiveAtomTypes(mol) )
				return false;
			if ( hasRadical(mol) )
				return false;
			if ( mol.isEmpty() )
				continue;
			if ( !addMissingHydrogens(mol) )
				return false;
			if ( this.m_doKekulize )
				if ( !kekulize(mol) )
					return false;
			if ( this.m_doResetStereo )
				resetStereoElementsWith2D(mol, true);
			if ( this.m_doAromatize )
				if ( !aromatize(mol) )
					return false;

			this.m_mols.add(mol);
		}
		return true;
	}

	/**
	 * Removes metal atoms.
	 */
	private void removeMetalAtoms(IAtomContainer a_mol){
		List<IAtom> t_aMetals = new ArrayList<>();
		for ( IAtom atom : a_mol.atoms() )
			if ( Elements.isMetal(atom) )
				t_aMetals.add(atom);

		for(IAtom atom : t_aMetals)
			a_mol.removeAtom(atom);

		if ( !t_aMetals.isEmpty() )
			logger.info("One or more metal atoms are removed.");
	}

	/**
	 * Neutralizes single bonds connecting two atoms charged with negative
	 * and positive, respectively, to double bonds with neutralized charges.
	 */
	private void neutralizeChargedBond(IAtomContainer a_mol) {
		boolean bApplied = false;
		for ( IBond bond : a_mol.bonds() ) {
			// Only for single bonds
			if ( bond.getOrder() != Order.SINGLE )
				continue;
			// Ignore metal atoms
			if ( Elements.isMetal( bond.getAtom(0) ) || Elements.isMetal( bond.getAtom(1) ) )
				continue;
			int t_iCharge0 = bond.getAtom(0).getFormalCharge();
			int t_iCharge1 = bond.getAtom(1).getFormalCharge();
			// Ignore zero charges
			if ( t_iCharge0 == 0 || t_iCharge1 == 0 )
				continue;
			// Ignore charge pair with the same signs
			if ( t_iCharge0 < 0 == t_iCharge1 < 0 )
				continue;
			// Neutralize charges
			int t_iChargeNew = t_iCharge0 + t_iCharge1;
			if ( (t_iChargeNew > 0) == (t_iCharge0 > t_iCharge1) ) {
				t_iCharge0 = t_iChargeNew;
				t_iCharge1 = 0;
			} else {
				t_iCharge0 = 0;
				t_iCharge1 = t_iChargeNew;
			}

			// Neutralize the bond and atoms
			bond.setOrder(Order.DOUBLE);
			bond.getAtom(0).setFormalCharge(t_iCharge0);
			bond.getAtom(1).setFormalCharge(t_iCharge1);

			bApplied = true;
		}

		if (bApplied)
			logger.info("Neutralized one or more single bonds connecting atoms"
					+ " with positive and negative charges into double bonds.");
	}

	/**
	 * Sets charge values to zero for all atoms.
	 */
	private void omitCharge(IAtomContainer a_mol) {
		boolean bOmitted = false;
		for ( IAtom atom : a_mol.atoms() )
			if ( atom.getCharge() != null && atom.getCharge() != 0.0D ) {
				atom.setCharge(0.0D);
				bOmitted = true;
			}
		if (bOmitted)
			logger.info("Charges on one or more atoms are changed to zero.");
	}

	private void restoreCharge(IAtomContainer a_mol) {
		try {
			// Prepare charge candidates (+1, -1, +2, -2, +3, and -3)
			List<Integer> lChargeCandidates = new ArrayList<>();
			for ( int i=1; i<=3; i++ )
				for ( int j=0; j<2; j++ )
					lChargeCandidates.add((j==0)? i : -i);

			// Try charges for atoms with unknown atom type
			for ( IAtom atom : a_mol.atoms() ) {
				IAtomType type = matcher.findMatchingAtomType(a_mol, atom);
				if ( !type.getAtomTypeName().equals("X") )
					continue;
				Integer charge = atom.getFormalCharge();
				if (charge != null && charge != 0)
					continue;
				for ( int i : lChargeCandidates ) {
					atom.setFormalCharge(i);
					type = matcher.findMatchingAtomType(a_mol, atom);
					// Continue until matching atom type is found
					if ( type.getAtomTypeName().equals("X") )
						continue;
					logger.info("An atom charge is restored.");
					break;
				}
				// Check untyped
				if ( !type.getAtomTypeName().equals("X") )
					continue;
				logger.warn("An atom can not be typed by charge restoration.");
				// Reset formal charge to zero
				atom.setFormalCharge(0);
			}
		} catch (CDKException e) {
			logger.error("Error in restoring charges.", e);
		}


	}

	private List<IAtomContainer> separate(IAtomContainer a_mol) {
		// Separate molecule to connecting group
		List<IAtomContainer> mols = new ArrayList<>();
		if ( ConnectivityChecker.isConnected(a_mol) )
			mols.add(a_mol);
		else {
			IAtomContainerSet molSet = ConnectivityChecker.partitionIntoMolecules(a_mol);
			for ( IAtomContainer mol : molSet.atomContainers() )
				mols.add(mol);
			logger.info("Partition into {} molecules.", mols.size());
		}
		return mols;
	}

	/**
	 * Perceives atom types.
	 * @return {@code true} if atom types are perceived successfully
	 * @see https://egonw.github.io/cdkbook/appatomtypes.html#cdk-atom-types
	 * @see AtomContainerManipulator#percieveAtomTypesAndConfigureAtoms
	 */
	private boolean perceiveAtomTypes(IAtomContainer a_mol) {
		removeExplicitHydrogens(a_mol);
		try {
			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(a_mol);
		} catch (CDKException e) {
			logger.error("Error in perceiving atom types.", e);
			return false;
		}
		return true;
	}

	private boolean checkTypedAtom(IAtomContainer a_mol) {
		int nUntypedAtoms = 0;
		try {
			for ( IAtom atom : a_mol.atoms() ) {
				IAtomType type = matcher.findMatchingAtomType(a_mol, atom);
				if ( type.getAtomTypeName().equals("X") )
					nUntypedAtoms++;
			}
		} catch (CDKException e) {
			logger.error("Error in matching atom type.");
			return false;
		}
		if ( nUntypedAtoms > 0 ) {
			logger.warn("One or more atoms with unknown types are found.");
			return false;
		}
		return true;
	}

	private boolean hasRadical(IAtomContainer a_mol) {
		for ( IAtom atom : a_mol.atoms() ) {
			if ( atom.getAtomTypeName() == null )
				continue;
			if ( !atom.getAtomTypeName().contains("radical") )
				continue;
			logger.error("Molecule containing radical atom(s) is not supported.");
			return true;
		}
		return false;
	}

	private void removeExplicitHydrogens(IAtomContainer a_mol) {
		// Remove explicit hydrogens without wedged bond
		List<IAtom> lExplicitHydrogens = new ArrayList<>();
		List<IAtom> lWedgedHydrogens = new ArrayList<>();
		for ( IAtom atom : a_mol.atoms() ) {
			// Only hydrogens
			if ( atom.getAtomicNumber() != 1 )
				continue;
			if ( a_mol.getConnectedBondsCount(atom) == 0 )
				continue;
			// Collect ones with wedged bond
			IBond bond = a_mol.getConnectedBondsList(atom).get(0);
			if ( bond.getStereo() != IBond.Stereo.NONE ) {
				lWedgedHydrogens.add(atom);
				continue;
			}
			lExplicitHydrogens.add(atom);
		}
		if ( !lExplicitHydrogens.isEmpty() ) {
			for ( IAtom atomH : lExplicitHydrogens ) {
				for ( IBond bondH : a_mol.getConnectedBondsList(atomH) ) {
					IAtom atomConn = bondH.getOther(atomH);
					Integer implicitHCount = atomConn.getImplicitHydrogenCount();
					atomConn.setImplicitHydrogenCount(implicitHCount+1);
				}
				a_mol.removeAtom(atomH);
			}

			// Reset stereos with 2D coordinate
			resetStereoElementsWith2D(a_mol, false);
		}

		// Do nothing if no wedged hydrogen
		if ( lWedgedHydrogens.isEmpty() )
			return;

		// Care for stereochemistry containing hydrogens
		List<IStereoElement> stereos = new ArrayList<>();
		for ( IStereoElement elem : a_mol.stereoElements() ) {
			Map<IAtom, IAtom> mapHToCenter = new HashMap<>();
			for ( IAtom removedH : lWedgedHydrogens ) {
				if ( !elem.contains(removedH) )
					continue;
				if ( !(elem.getFocus() instanceof IAtom) )
					continue;
				// Only for chiral atom
				mapHToCenter.put(removedH, (IAtom)elem.getFocus());
			}
			if ( mapHToCenter.isEmpty() ) {
				stereos.add(elem);
				continue;
			}
			// Do not care stereocenter connecting two or more hydrogens
			if ( mapHToCenter.size() > 1 )
				continue;
			stereos.add( elem.map(mapHToCenter, new HashMap<>()) );
		}
		// Replace stereo elements
		a_mol.setStereoElements(stereos);

		// Remove explicit wedged hydrogens
		for ( IAtom atomH : lWedgedHydrogens ) {
			for ( IBond bondH : a_mol.getConnectedBondsList(atomH) ) {
				IAtom atomConn = bondH.getOther(atomH);
				Integer implicitHCount = atomConn.getImplicitHydrogenCount();
				atomConn.setImplicitHydrogenCount(implicitHCount+1);
			}
			a_mol.removeAtom(atomH);
		}
	}

	/**
	 * Add missing hydrogens to the given molecule.
	 * {@link #percieveAtomTypes(IAtomContainer)} must be performed earlier.
	 */
	private boolean addMissingHydrogens(IAtomContainer a_mol) {
		try {
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(
					DefaultChemObjectBuilder.getInstance()
				);
			adder.addImplicitHydrogens(a_mol);
			// Do not add hydrogens explicitly ever
//			AtomContainerManipulator.convertImplicitToExplicitHydrogens(a_mol);

		} catch (CDKException e) {
			logger.error("Error in adding missing hydrogens.", e);
			return false;
		}
		return true;
	}

	/**
	 * Kekulize atoms in the given molecule.
	 * {@link #addMissingHydrogens(IAtomContainer)} must be performed earlier.
	 * @param a_mol
	 */
	private boolean kekulize(IAtomContainer a_mol) {
		try {
			Kekulization.kekulize(a_mol);
		} catch (CDKException e) {
			logger.error("Error in kekulization.", e);
			return false;
		}
		return true;
	}

	private void resetStereoElementsWith2D(IAtomContainer a_mol, boolean a_doPreCheck) {
		if ( a_doPreCheck ) {
			boolean hasStereoElement = false;
			for ( IStereoElement elem : a_mol.stereoElements() )
				hasStereoElement = true;
			if ( hasStereoElement )
				return;
		}

		// Reset stereo elements
		StereoElementFactory factory = null;
		if ( a_mol.getAtom(0).getPoint2d() != null )
			factory = StereoElementFactory.using2DCoordinates(a_mol)
					.interpretProjections(Projection.Fischer, Projection.Haworth, Projection.Chair);
		else if ( a_mol.getAtom(0).getPoint3d() != null )
			factory = StereoElementFactory.using3DCoordinates(a_mol);
		else
			return;

		try {
			a_mol.setStereoElements(factory.createAll());
		} catch (Exception e) {
			logger.warn("Error in resetting stereo elements.");
			return;
		}
	}

	/**
	 * Aromatize atoms in the given molecule.
	 * {@link #kekulize(IAtomContainer)} must be performed earlier.
	 * @param a_mol
	 */
	private boolean aromatize(IAtomContainer a_mol) {
		try {
			if ( !aromaticity.apply(a_mol) )
				return true;
			for ( IBond bond : aromaticity.findBonds(a_mol) ) {
				bond.setIsAromatic(true);
				bond.getBegin().setIsAromatic(true);
				bond.getEnd().setIsAromatic(true);
			}
		} catch (CDKException e) {
			logger.error("Error in aromatization.", e);
			return false;
		}
		return true;
	}
}
