package org.glycoinfo.MolWURCS.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.exchange.toWURCS.MoleculeToWURCSGraph;
import org.glycoinfo.MolWURCS.exchange.toWURCS.report.ExtractionReport;
import org.glycoinfo.MolWURCS.exchange.toWURCS.report.MoleculeToWURCSGraphForReport;
import org.glycoinfo.MolWURCS.io.formats.WURCSFormat;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IChemObject;
import org.openscience.cdk.io.DefaultChemObjectWriter;
import org.openscience.cdk.io.formats.IResourceFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WURCSWriter extends DefaultChemObjectWriter {

	private static Logger logger = LoggerFactory.getLogger(WURCSWriter.class);

	private BufferedWriter writer;

	private boolean outputMessage;
	private boolean outputWithAglycone;
	private String strPropID;
	private boolean outputReport;
	/** For verification*/
	private boolean doDoubleCheck;

	/**
	 * Constructs a new WURCSWriter that can write a list of WURCS to a Writer
	 *
	 * @param   out  The Writer to write to
	 */
	public WURCSWriter(Writer out) {
		try {
			if (out instanceof BufferedWriter) {
				this.writer = (BufferedWriter) out;
			} else {
				this.writer = new BufferedWriter(out);
			}
		} catch (Exception exc) {
		}
		this.outputMessage = false;
		this.outputWithAglycone = false;
		this.strPropID = null;
		this.outputReport = false;
		this.doDoubleCheck = false;
	}

	public WURCSWriter(OutputStream output) {
		this(new OutputStreamWriter(output));
	}

	public WURCSWriter() {
		this(new StringWriter());
	}

	@Override
	public IResourceFormat getFormat() {
		return WURCSFormat.getInstance();
	}

	@Override
	public void setWriter(Writer out) throws CDKException {
		if (out instanceof BufferedWriter) {
			this.writer = (BufferedWriter) out;
		} else {
			this.writer = new BufferedWriter(out);
		}
	}

	@Override
	public void setWriter(OutputStream output) throws CDKException {
		setWriter(new OutputStreamWriter(output));
	}

	/**
	 * Set option for outputting WURCS containing aglycone as MAP string.
	 * The WURCS is structurally OK but violate WURCS rule because aglycone part
	 * is not carbohydrate in the rule.
	 * The aglycone will be removed when this option is set as {@code false},
	 * and the WURCS will be separated if the aglycone bridges multiple carbohydrates.
	 * @param output {@code true} for outputting WURCS with aglycone, {@code false} otherwise.
	 */
	public void setOutputWithAglycone(boolean output) {
		this.outputWithAglycone = output;
	}

	/**
	 * Set option for outputting conversion result as a message instead of WURCS string.
	 * The message is output when WURCS can not be converted from molecule
	 * due to conversion error or no monosaccharide in the molecule.
	 * @param output {@code true} for outputting conversion result as a message
	 *  when no WURCS, {@code false} otherwise.
	 */
	public void setOutputMessage(boolean output) {
		this.outputMessage = output;
	}

	/**
	 * Set ID of the property which is used as title of each WURCS output.
	 * If the property is not found in the input molecule,
	 * the title of output is to be the title of the molecule as usual.
	 * @param strPropertyID
	 */
	public void setTitlePropertyID(String strPropertyID) {
		this.strPropID = strPropertyID;
	}

	/**
	 * Set option for outputting glycan extraction results as a table (csv).
	 * @param output
	 */
	public void setOutputReport(boolean output) {
		this.outputReport = output;
	}

	public void writeReportHeader() {
		ExtractionReport.writeReportHeader(writer);
	}

	/**
	 * Set option for double-checking output result using WURCS to WURCS conversion.
	 * @param doCheck
	 */
	public void setDoDoubleCheck(boolean doCheck) {
		this.doDoubleCheck = doCheck;
	}

	/**
	 * Flushes the output and closes this object.
	 */
	@Override
	public void close() throws IOException {
		this.writer.flush();
		this.writer.close();
	}

	@Override
	public boolean accepts(Class<? extends IChemObject> classObject) {
		if (IAtomContainer.class.equals(classObject)) return true;
		if (IAtomContainerSet.class.equals(classObject)) return true;
		Class<?>[] interfaces = classObject.getInterfaces();
		for (int i = 0; i < interfaces.length; i++) {
			if (IAtomContainerSet.class.equals(interfaces[i])) return true;
			if (IAtomContainer.class.equals(interfaces[i])) return true;
		}
		Class superClass = classObject.getSuperclass();
		if (superClass != null) return this.accepts(superClass);
		return false;
	}

	/**
	 * Writes the content from object to output.
	 *
	 * @param   object  IChemObject of which the data is given as output.
	 */
	@Override
	public void write(IChemObject object) throws CDKException {
		if (object instanceof IAtomContainerSet) {
			writeAtomContainerSet((IAtomContainerSet) object);
		} else if (object instanceof IAtomContainer) {
			writeAtomContainer((IAtomContainer) object);
		} else {
			throw new CDKException("Only supported is writing of ChemFile and Molecule objects.");
		}
	}

	/**
	 * Writes a list of molecules to an OutputStream.
	 *
	 * @param   som  MoleculeSet that is written to an OutputStream
	 */
	public void writeAtomContainerSet(IAtomContainerSet som) {
		writeAtomContainer(som.getAtomContainer(0));
		for (int i = 1; i <= som.getAtomContainerCount() - 1; i++) {
//			try {
				writeAtomContainer(som.getAtomContainer(i));
//			} catch (Exception exc) {
//			}
		}
	}

	/**
	 * Writes the content from molecule to output.
	 *
	 * @param   molecule  Molecule of which the data is given as output.
	 */
	public void writeAtomContainer(IAtomContainer molecule) {
		// Get title
		String strTitle = molecule.getTitle();
		if ( this.strPropID != null ) {
			if ( molecule.getProperty(this.strPropID) != null )
				strTitle = molecule.getProperty(this.strPropID);
			else
				logger.warn("No property \"{}\" is found.", this.strPropID);
		}

		// Get WURCSs from molecule
		List<String> lWURCSs = null;
		String strMessage = "Error in conversion";
		try {
			lWURCSs = getWURCSs(molecule, strTitle);
			if ( lWURCSs == null ) {
				strMessage = "No WURCS";
				if ( molecule.getProperty(WURCSParser.BAD_WURCS_MESSAGE) != null )
					strMessage = molecule.getProperty(WURCSParser.BAD_WURCS_MESSAGE);
			} else
				logger.debug("Generated WURCS: {}", lWURCSs);
		} catch (Exception exc) {
			String strError = "Error in WURCS conversion";
			if ( strTitle != null )
				strError += " in "+strTitle;
			strError += " - "+exc.toString();
			logger.error(strError, exc);
		}

		if ( lWURCSs == null ) {
			if ( !this.outputMessage )
				return;
			lWURCSs = new ArrayList<>();
			lWURCSs.add(strMessage);
		}

		// Write WURCS
		try {
			for ( String strWURCS : lWURCSs ) {
				if ( strTitle != null ) {
					this.writer.write(strTitle);
					this.writer.write('\t');
				}
				this.writer.write(strWURCS);
				this.writer.write('\n');
			}
			this.writer.flush();
			logger.debug("file flushed...");
		} catch (IOException exc) {
			logger.error("Error while writing WURCS: {}", exc.getMessage());
		}
	}

	private List<String> getWURCSs(IAtomContainer molecule, String strTitle) throws WURCSException {
		List<WURCSGraph> graphs = convert(molecule, strTitle);
		if ( graphs == null )
			return null;

		// Encode graphs to String
		List<String> lWURCSs = new ArrayList<>();
		for ( WURCSGraph graph : graphs ) {
			WURCSFactory factory = new WURCSFactory(graph, false);
			lWURCSs.add(factory.getWURCS());
		}

		// Double check output WURCSs
		// TODO: This part is tentative process for validating output WURCS
		if ( this.doDoubleCheck ) {
			logger.info("Start double check using WURCS to WURCS.");
			List<String> lValidWURCSs = new ArrayList<>();
			for ( String strWURCS : lWURCSs )
				if ( WURCSConversionChecker.doubleCheck(strWURCS) )
					lValidWURCSs.add(strWURCS);
				else
					logger.error("Error in double check for output WURCS.");
			if ( lValidWURCSs.isEmpty() )
				return null;

			lWURCSs = lValidWURCSs;
		}

		return lWURCSs;
	}

	private List<WURCSGraph> convert(IAtomContainer a_mol, String strTitle) throws WURCSException {
		ExtractionReport.Report reportCurrent = null;
		MoleculeToWURCSGraph converter;
		if ( this.outputReport ) {
			MoleculeToWURCSGraphForReport converterR = new MoleculeToWURCSGraphForReport();
			reportCurrent = converterR.getReport();
			converter = converterR;
		} else
			converter = new MoleculeToWURCSGraph();
		converter.setOutputWithAglycone(this.outputWithAglycone);

		List<WURCSGraph> graphs = converter.start(a_mol, strTitle);

		if ( reportCurrent != null )
			reportCurrent.write(this.writer);

		if ( graphs == null || graphs.isEmpty() )
			return null;

		return graphs;
	}

}
