package org.glycoinfo.MolWURCS.io.formats;

import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.io.IChemObjectWriter;
import org.openscience.cdk.io.ISimpleChemObjectReader;
import org.openscience.cdk.io.ReaderFactory;
import org.openscience.cdk.io.SDFWriter;
import org.openscience.cdk.io.WriterFactory;
import org.openscience.cdk.io.formats.IChemFormat;
import org.openscience.cdk.io.formats.IResourceFormat;
import org.openscience.cdk.io.formats.MDLV2000Format;
import org.openscience.cdk.io.formats.SDFFormat;
import org.openscience.cdk.io.formats.SMILESFormat;
import org.openscience.cdk.io.iterator.IIteratingChemObjectReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ChemFormatType {
	WURCS("wurcs", WURCSFormat.getInstance(), "org.glycoinfo.MolWURCS.io.IteratingWURCSReader"),
	MDLV2000("mdlv2000", MDLV2000Format.getInstance(), null),
	SDF("sdf", SDFFormat.getInstance(), "org.openscience.cdk.io.iterator.IteratingSDFReader"),
	SDFV3000("sdfv3000", SDFFormat.getInstance(), "org.openscience.cdk.io.iterator.IteratingSDFReader"),
	SMILES("smi", SMILESFormat.getInstance(), "org.openscience.cdk.io.iterator.IteratingSMILESReader"),
	;

	private static final Logger logger = LoggerFactory.getLogger(ChemFormatType.class);

	private String m_strType;
	private IChemFormat m_format;
	private String m_strIteratingReader;

	private static ReaderFactory rFactory = new ReaderFactory();
	private static WriterFactory wFactory = new WriterFactory();

	private ChemFormatType(String strType, IResourceFormat format, String strIteratingReader) {
		this.m_strType = strType;
		this.m_format = (IChemFormat) format;
		this.m_strIteratingReader = strIteratingReader;
	}

	public String getFormatType() {
		return this.m_strType;
	}

	public IChemFormat getFormat() {
		return this.m_format;
	}

	public boolean hasIteratingReader() {
		return (this.m_strIteratingReader != null);
	}

	public IIteratingChemObjectReader<IAtomContainer> createIteratingReader(Reader in, IChemObjectBuilder builder) {
		try {
			// make a new instance of this class
			return (IIteratingChemObjectReader<IAtomContainer>)
					this.getClass().getClassLoader().loadClass(this.m_strIteratingReader)
					.getConstructor(Reader.class, IChemObjectBuilder.class)
					.newInstance(in, builder);
		} catch (ClassNotFoundException e) {
			logger.error("Could not find this IteratingChemObjectReader: "+this.m_strIteratingReader, e);
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			logger.error("Could not create this IteratingChemObjectReader: "+this.m_strIteratingReader, e);
		}
		return null;
	}

	/**
	 * Create corresponding IChemObjectReader using ReaderFactory.
	 * @return ISimpleChemObjectReader
	 * @see ReaderFactory
	 */
	public ISimpleChemObjectReader createReader() {
		return rFactory.createReader(this.m_format);
	}

	/**
	 * Create corresponding IChemObjectWriter using WriterFactory.
	 * @return IChemObjectWriter
	 * @see WriterFactory
	 */
	public IChemObjectWriter createWriter() {
		IChemObjectWriter writer = wFactory.createWriter(this.m_format);
		if ( this == SDFV3000 )
			((SDFWriter)writer).setAlwaysV3000(true);
		return writer;
	}

	public static ChemFormatType forName(String a_strName) {
		if ( a_strName == null )
			return null;
		String strName = a_strName.toLowerCase();
		for ( ChemFormatType type : ChemFormatType.values() ) {
			if ( type.m_strType.equals(strName) )
				return type;
		}
		return null;
	}

	public static String[] getInputFormatTypes() {
		List<String> lTypes = new ArrayList<>();
		for ( ChemFormatType type : ChemFormatType.values() ) {
			// Check reader class name
			if ( type.getFormat().getReaderClassName() == null )
				continue;
			lTypes.add(type.m_strType);
		}
		return lTypes.toArray(new String[lTypes.size()]);
	}

	public static String[] getOutputFormatTypes() {
		List<String> lTypes = new ArrayList<>();
		for ( ChemFormatType type : ChemFormatType.values() ) {
			// Check writer class name
			if ( type.getFormat().getWriterClassName() == null )
				continue;
			lTypes.add(type.m_strType);
		}
		return lTypes.toArray(new String[lTypes.size()]);
	}
}
