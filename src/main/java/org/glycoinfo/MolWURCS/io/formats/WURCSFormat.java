package org.glycoinfo.MolWURCS.io.formats;

import org.openscience.cdk.io.formats.AbstractResourceFormat;
import org.openscience.cdk.io.formats.IChemFormat;
import org.openscience.cdk.io.formats.IResourceFormat;
import org.openscience.cdk.tools.DataFeatures;

public class WURCSFormat extends AbstractResourceFormat implements IChemFormat {

	private static IResourceFormat myself = null;

	public WURCSFormat() {}

	public static IResourceFormat getInstance() {
		if (myself == null) myself = new WURCSFormat();
		return myself;
	}

	@Override
	public String getFormatName() {
		return "Web3.0 Unique Representation for Carbohydrate Structure (WURCS)";
	}

	@Override
	public String getPreferredNameExtension() {
		return getNameExtensions()[0];
	}

	@Override
	public String[] getNameExtensions() {
		return new String[]{"wurcs"};
	}

	@Override
	public String getMIMEType() {
		return null;
	}

	@Override
	public boolean isXMLBased() {
		return false;
	}

	@Override
	public String getReaderClassName() {
		return "org.glycoinfo.MolWURCS.io.WURCSReader";
	}

	@Override
	public String getWriterClassName() {
		return "org.glycoinfo.MolWURCS.io.WURCSWriter";
	}

	@Override
	public int getSupportedDataFeatures() {
		return DataFeatures.NONE;
	}

	@Override
	public int getRequiredDataFeatures() {
		return DataFeatures.NONE;
	}

}
