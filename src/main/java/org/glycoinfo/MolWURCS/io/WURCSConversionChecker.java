package org.glycoinfo.MolWURCS.io;

import java.util.List;

import org.glycoinfo.MolWURCS.exchange.toWURCS.MoleculeToWURCSGraph;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WURCSConversionChecker {

	private static Logger logger = LoggerFactory.getLogger(WURCSConversionChecker.class);

	/**
	 * Check if input WURCS is changed in WURCS to WURCS conversion.
	 * @param lOutWURCSs List of output WURCSs
	 * @param strInWURCS String of input WURCS
	 */
	static void checkWithInput(List<String> lOutWURCSs, String strInWURCS) {
		if ( strInWURCS == null )
			return;
		if ( lOutWURCSs == null || lOutWURCSs.isEmpty() )
			return;
		if ( lOutWURCSs.size() > 1 ) {
			logger.warn("The WURCS has separated with aglycone:");
			logger.warn("INPUT:  {}", strInWURCS);
			for ( String strOutWURCS : lOutWURCSs )
				logger.warn("OUTPUT: {}", strOutWURCS);
			return;
		}
		if ( strInWURCS.equals( lOutWURCSs.get(0) ) )
			return;
		logger.warn("The WURCS has been changed:");
		logger.warn("INPUT:  {}", strInWURCS);
		logger.warn("OUTPUT: {}", lOutWURCSs.get(0));
	}

	/**
	 * Double-check WURCS using WURCS to WURCS conversion in normal way.
	 * @param strWURCS String of WURCS for double-check
	 * @return {@code true} if the specified WURCS can be converted to the same WURCS
	 * without any problems, {@code false} otherwise
	 */
	static boolean doubleCheck(String strWURCS) {
		// Parse WURCS
		try {
			IAtomContainer mol = WURCSParser.parseWURCS(strWURCS);
			if ( mol.isEmpty() )
				return false;
			return doubleCheck(mol, strWURCS);
		} catch (WURCSException e) {
			logger.error("Error in parsing WURCS in double check: {}", strWURCS);
			return false;
		}
	}

	/**
	 * Double-check input WURCS converting output molecule to WURCS again.
	 * @param molOut Molecule converted from {@code strInWURCS}
	 * @param strInWURCS String of input WURCS for double-check
	 * @return {@code true} if the specified molecule can be converted to the input WURCS
	 * without any problems, {@code false} otherwise
	 */
	static boolean doubleCheck(IAtomContainer molOut, String strInWURCS) {
		// Re-convert molecule
		try {
			List<WURCSGraph> graphs = new MoleculeToWURCSGraph().start(molOut);
			if ( graphs == null || graphs.size() != 1 )
				return false;

			WURCSFactory factory = new WURCSFactory(graphs.get(0), false);
			String strOutWURCS = factory.getWURCS();
			if ( !strInWURCS.equals(strOutWURCS) ) {
				logger.error("Re-converted WURCS is changed:");
				logger.warn("INPUT:  {}", strInWURCS);
				logger.warn("OUTPUT: {}", strOutWURCS);
				return false;
			}
		} catch (WURCSException e) {
			logger.error("Error in re-conversion of molecule in double check: {}", strInWURCS);
			return false;
		}

		return true;
	}
}
