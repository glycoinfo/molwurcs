package org.glycoinfo.MolWURCS.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.NoSuchElementException;

import org.glycoinfo.MolWURCS.io.formats.WURCSFormat;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.io.formats.IResourceFormat;
import org.openscience.cdk.io.iterator.DefaultIteratingChemObjectReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IteratingWURCSReader extends DefaultIteratingChemObjectReader<IAtomContainer> {

	private static Logger logger = LoggerFactory.getLogger(IteratingWURCSReader.class);

	private BufferedReader           input;

	private boolean                  nextAvailableIsKnown;
	private boolean                  hasNext;
	private IAtomContainer           nextMolecule;
	private final IChemObjectBuilder builder;

	/** Store the problem input as a property. */
	public static final String       BAD_WURCS_INPUT = "bad.wurcs.input";

	/**
	 * Constructs a new IteratingSMILESReader that can read Molecule from a given Reader.
	 *
	 * @param  in  The Reader to read from
	 * @param builder The builder to use
	 * @see org.openscience.cdk.DefaultChemObjectBuilder
	 * @see org.openscience.cdk.silent.SilentChemObjectBuilder
	 */
	public IteratingWURCSReader(Reader in, IChemObjectBuilder builder) {
		setReader(in);
		this.builder = builder;
	}

	/**
	 * Constructs a new IteratingSMILESReader that can read Molecule from a given InputStream and IChemObjectBuilder.
	 *
	 * @param in      The input stream
	 * @param builder The builder
	 */
	public IteratingWURCSReader(InputStream in, IChemObjectBuilder builder) {
		this(new InputStreamReader(in), builder);
	}

	/**
	 * Get the format for this reader.
	 *
	 * @return An instance of {@link org.openscience.cdk.io.formats.WURCSFormat}
	 */
	@Override
	public IResourceFormat getFormat() {
		return WURCSFormat.getInstance();
	}

	@Override
	public void setReader(Reader reader) {
		if (reader instanceof BufferedReader) {
			input = (BufferedReader) reader;
		} else {
			input = new BufferedReader(reader);
		}
		nextMolecule = null;
		nextAvailableIsKnown = false;
		hasNext = false;
	}

	@Override
	public void setReader(InputStream reader) {
		setReader(new InputStreamReader(reader));
	}

	@Override
	public void close() throws IOException {
		if (input != null) input.close();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasNext() {
		if (!nextAvailableIsKnown) {
			hasNext = false;

			// now try to parse the next Molecule
			try {

				final String line = input.readLine();

				if (line == null) {
					nextAvailableIsKnown = true;
					return false;
				}

				hasNext = true;
				nextMolecule = WURCSParser.processLine(line, builder);

			} catch (Exception exception) {
				logger.error("Unexpected problem: {}", exception.getMessage());
				hasNext = false;
			}
			if (!hasNext) nextMolecule = null;
			nextAvailableIsKnown = true;
		}
		return hasNext;
	}

	@Override
	public IAtomContainer next() {
		if (!nextAvailableIsKnown) {
			hasNext();
		}
		nextAvailableIsKnown = false;
		if (!hasNext) {
			throw new NoSuchElementException();
		}
		return nextMolecule;
	}

}
