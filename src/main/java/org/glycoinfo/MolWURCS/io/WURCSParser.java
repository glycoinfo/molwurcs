package org.glycoinfo.MolWURCS.io;

import org.glycoinfo.MolWURCS.exchange.fromWURCS.WURCSGraphToMolecule;
import org.glycoinfo.MolWURCS.util.validation.WURCSValidationReportForMolecule;
import org.glycoinfo.MolWURCS.util.validation.WURCSValidatorForMolecule;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WURCSParser {

	private static Logger logger = LoggerFactory.getLogger(WURCSParser.class);

	/** Store the problem input as a property. */
	public static final String BAD_WURCS_INPUT   = "bad.wurcs.input";
	/** Store the reason of problem input as a property. */
	public static final String BAD_WURCS_MESSAGE = "bad.wurcs.message";

	/** Flag for double check for input WURCS */
	public static boolean doDoubleCheck = false;

	static void processLine(String line, IAtomContainerSet som) {
		IAtomContainer molecule = processLine(line, som.getBuilder());
		som.addAtomContainer(molecule);
	}

	static IAtomContainer processLine(String line, IChemObjectBuilder builder) {
		String strTitle = null;
		String strWURCS = null;
		final String[] split = split(line);
		if ( split[1] != null ) {
			strTitle = split[0];
			strWURCS = split[1];
		} else
			strWURCS = split[0];

		logger.info("Start process for WURCS: {}", (strTitle != null)? strTitle : strWURCS);

		IAtomContainer molecule = null;

		// Validate WURCS
		boolean bHasValidationError = false;
		String strMessage = null;
		WURCSValidationReportForMolecule report = validateWURCS(strWURCS);
		if ( report.hasError() || report.hasUnverifiable() || report.hasIncompatible() ) {
			logger.warn("This WURCS cannot be converted to molecule: {}", strWURCS);
			logger.warn("Because of:");
			if ( !logger.isDebugEnabled() )
				logger.warn(report.getResultsSimple());
			logger.debug(report.getResults());
			strMessage = (report.hasError())? "Error in validation" :
						 (report.hasIncompatible())? "Incompatible for molecule" : null;
			bHasValidationError = true;
		} else if ( report.hasWarning() ) {
			// TODO: Remove this part after validation for MAPGraph is implemented
			for ( String strWarn : report.getWarnings() ) {
				if ( !strWarn.contains("The MAP has been changed by the MAP normalization.") )
					continue;
				logger.warn("This WURCS contains MAP change in normalization.");
				strMessage = "Error in validation";
				bHasValidationError = true;
				break;
			}
		}
		if ( !bHasValidationError ) {
			// Parse WURCS
			try {
				molecule = parseWURCS(strWURCS);
				// Double-check output molecule
				// TODO: This part is tentative process for validate input WURCS
				if ( doDoubleCheck && !WURCSConversionChecker.doubleCheck(molecule, strWURCS) ) {
					logger.error("Error in double check for input WURCS.");
					molecule = null;
				}
			} catch (WURCSException exception) {
				logger.warn("This WURCS could not be parsed: {}", strWURCS);
				logger.warn("Because of: {}", exception.getMessage());
				strMessage = "Error in parsing WURCS";
			} catch (Exception exception) {
				logger.warn("Unexpected problem in parsing WURCS: {}", exception.getMessage());
				exception.printStackTrace();
				strMessage = "Unexpected problem in parsing WURCS";
			}
		}

		// Set empty molecule when WURCS has error
		if ( molecule == null ) {
			molecule = builder.newInstance(IAtomContainer.class, 0, 0, 0, 0);
			molecule.setProperty(BAD_WURCS_INPUT, strWURCS);
			if ( strMessage != null )
				molecule.setProperty(BAD_WURCS_MESSAGE, strMessage);
		} else
			molecule.setProperty("INPUT_WURCS", strWURCS);
		if ( strTitle != null )
			molecule.setTitle(strTitle);
		return molecule;
	}

	/**
	 * Split a line containing WURCS and the following suffix. The suffix follows
	 * any ' ' or '\t' termination characters.
	 *
	 * @param line input line
	 * @return the split Strings (String[0]: WURCS, String[1]: suffix)
	 */
	private static String[] split(final String line) {
		String[] split = new String[2];
		for (int i = 0; i < line.length(); i++) {
			char c = line.charAt(i);
			if (c == ' ' || c == '\t') {
				split[0] = line.substring(0, i).trim();
				split[1] = line.substring( i + 1 ).trim();
				return split;
			}
		}
		split[0] = line;
		return split;
	}

	private static WURCSValidationReportForMolecule validateWURCS(String a_strWURCS) {
		WURCSValidatorForMolecule validator = new WURCSValidatorForMolecule();
		validator.start(a_strWURCS);
		return validator.getReport();
	}

	static IAtomContainer parseWURCS(String a_strWURCS) throws WURCSException {
		WURCSFactory factory = new WURCSFactory(a_strWURCS);
		WURCSGraph graph = factory.getGraph();

		WURCSGraphToMolecule WG2Mol = new WURCSGraphToMolecule();
		WG2Mol.start(graph);

		return WG2Mol.getMolecule();
	}
}
