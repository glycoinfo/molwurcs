package org.glycoinfo.MolWURCS.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import org.glycoinfo.MolWURCS.io.formats.WURCSFormat;
import org.glycoinfo.MolWURCS.util.cdk.ChemObjectProvider;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IChemFile;
import org.openscience.cdk.interfaces.IChemModel;
import org.openscience.cdk.interfaces.IChemObject;
import org.openscience.cdk.interfaces.IChemSequence;
import org.openscience.cdk.io.DefaultChemObjectReader;
import org.openscience.cdk.io.formats.IResourceFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Reader reads files which has one WURCS string on each
 * line, where the format is given as below:
 * <pre>
 * WURCS=2.0/1,1,0/[a2122h-1b_1-5]/1/
 * </pre>
 * If a line is invalid an empty molecule is inserted into the container set.
 * The molecule with have the prop {@link #BAD_WURCS_INPUT} set to the input line
 * that could not be read.
 *
 * <p>For each line a molecule is generated, and multiple IAtomContainers are
 * read as IAtomContainerSet.
 *
 */
public class WURCSReader extends DefaultChemObjectReader {

	private static Logger logger = LoggerFactory.getLogger(WURCSReader.class);

	private BufferedReader input = null;

	/**
	 * Construct a new reader from a Reader and a specified builder object.
	 *
	 * @param input   The Reader object from which to read structures
	 */
	public WURCSReader(Reader input) {
		this.input = new BufferedReader(input);
	}

	public WURCSReader(InputStream input) {
		this(new InputStreamReader(input));
	}

	public WURCSReader() {
		this(new StringReader(""));
	}

	@Override
	public void setReader(Reader input) throws CDKException {
		if (input instanceof BufferedReader) {
			this.input = (BufferedReader) input;
		} else {
			this.input = new BufferedReader(input);
		}
	}

	@Override
	public void setReader(InputStream input) throws CDKException {
		setReader(new InputStreamReader(input));
	}

	@Override
	public IResourceFormat getFormat() {
		return WURCSFormat.getInstance();
	}

	@Override
	public boolean accepts(Class<? extends IChemObject> classObject) {
		if (IAtomContainerSet.class.equals(classObject)) return true;
		if (IChemFile.class.equals(classObject)) return true;
		Class<?>[] interfaces = classObject.getInterfaces();
		for (Class<?> anInterface : interfaces) {
			if (IChemFile.class.equals(anInterface)) return true;
			if (IAtomContainerSet.class.equals(anInterface)) return true;
		}
		Class superClass = classObject.getSuperclass();
		if (superClass != null) return this.accepts(superClass);
		return false;
	}

	@Override
	public void close() throws IOException {
		input.close();
	}

	/**
	 * Reads the content from a WURCS input. It can only return a
	 * {@link IAtomContainerSet} of type {@link IChemFile}.
	 *
	 * @param object class must be of type IAtomContainerSet or IChemFile
	 *
	 * @see IAtomContainerSet
	 * @see IChemFile
	 */
 	@Override
	public <T extends IChemObject> T read(T object) throws CDKException {
		ChemObjectProvider.setBuilder(object.getBuilder());

		if (object instanceof IAtomContainerSet) {
			return (T) readAtomContainerSet((IAtomContainerSet) object);
		} else if (object instanceof IChemFile) {
			IChemFile file = (IChemFile) object;
			IChemSequence sequence = file.getBuilder().newInstance(IChemSequence.class);
			IChemModel chemModel = file.getBuilder().newInstance(IChemModel.class);
			chemModel.setMoleculeSet(readAtomContainerSet(file.getBuilder().newInstance(IAtomContainerSet.class)));
			sequence.addChemModel(chemModel);
			file.addChemSequence(sequence);
			return (T) file;
		} else {
			throw new CDKException("Only supported is reading of IAtomContainerSet or IChemFile objects.");
		}
	}

	/**
	 *  Private method that actually parses the input to read an instance of
	 * IAtomContainerSet object.
	 *
	 * @param som The set of molecules that came from the file
	 * @return An instance of IAtomContainerSet containing the data parsed from input.
	 */
	private IAtomContainerSet readAtomContainerSet(IAtomContainerSet som) {
		try {
			String line = input.readLine().trim();
			while (line != null) {
				logger.debug("Line: ", line);

				WURCSParser.processLine(line, som);

				if (input.ready()) {
					line = input.readLine();
				} else {
					line = null;
				}
			}
		} catch (Exception exception) {
			logger.error("Error while reading WURCS line: {}", exception.getMessage());
		}
		return som;
	}

}
