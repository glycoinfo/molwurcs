package org.glycoinfo.MolWURCS.test;

import java.util.List;

import org.glycoinfo.MolWURCS.exchange.toWURCS.MoleculeToWURCSGraph;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.openscience.cdk.interfaces.IAtomContainer;

public class TestMolToWURCS extends SDFReader {

	public static void main(String[] args) {
		new TestMolToWURCS().run(args);
	}

	@Override
	protected void process(IAtomContainer a_mol) {
		String strTitle = a_mol.getTitle();
		if ( strTitle != null && !strTitle.isEmpty() )
			System.out.println(strTitle+":");
		try {
			List<WURCSGraph> graphs = new MoleculeToWURCSGraph().start(a_mol);
			if ( graphs == null || graphs.isEmpty() )
				return;
			for ( WURCSGraph graph : graphs ) {
				WURCSFactory factory = new WURCSFactory(graph);
				System.out.println(factory.getWURCS());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
