package org.glycoinfo.MolWURCS.test;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.io.formats.ChemFormatType;
import org.openscience.cdk.depict.DepictionGenerator;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.io.IChemObjectWriter;
import org.openscience.cdk.io.ISimpleChemObjectReader;
import org.openscience.cdk.silent.AtomContainerSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestWURCSToMol {

	private static Logger logger = LoggerFactory.getLogger(TestWURCSToMol.class);

	public static void main(String[] args) {
		List<String> lWURCSs;
		try {
			lWURCSs = Files.readAllLines( Paths.get("src/test/resources/TestStructures.wurcs") );
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

//		List<String> lWURCSs = new ArrayList<>();
//		lWURCSs.add("WURCS=2.0/2,3,2/[a6122A-1b_1-5_2*NCC/3=O][Aad21122dEefFzZnN-2a_2-6_5*NCC/3=O]/1-1-2/a4-b1_b4-c2");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1b_1-5_6*O(C^EC^ZC^ZC^ZC^ZC^E$3)]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[adC1212112h-1b_1-11_3d-9]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1b_1-5_4n1-6n2*1OC^RO*2/3C/3CO/7=O]/1/");
//		lWURCSs.add("WURCS=2.0/1,3,3/[a2122h-1b_1-5]/1-1-1/a4n3-b2n1-c6n2*1O(C^EC^ZC^EC^EC^EC^ZC^ZC^ZC^EC^EC^E$8/9^ZC^ZC^EC^E$3/14^E$5/7^ZC^EC^EC^EC$6)/20O*2/18O/15O*3/13O/11O/10O_a3-b1_b3-c1");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1d_1-5]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1d_1-?]/1/");
//		lWURCSs.add("WURCS=2.0/3,7,6/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-1-3-1/a4-b1_b4-c1_c3-d1_c6-f1_d4-e1_f4-g1");
//		lWURCSs.add("WURCS=2.0/1,3,3/[adddh-1x]/1-1-1/a1-a5-b1*N*/2*_b1-b5-c1*N*/2*_a1-c1-c5*N*/2*");
//		lWURCSs.add("WURCS=2.0/1,3,3/[cdddh]/1-1-1/a1x-a5-b1x*N*/2*_b1x-b5-c1x*N*/2*_a1x-c1x-c5*N*/2*");
//		lWURCSs.add("WURCS=2.0/1,1,0/[Ad2d111h_3-7_5-6*(CCC^ZCC^EC$2)/6NSC/9=O/9=O/3O*_1*NCCNCC$2/5C]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[Add21dc_4n2-7x1*1OC^S*2/3C=^ECC^SCCCCC/7O]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[Add21dc_4n2-7x1*1OC^S*2/3CC]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2112h-1a_1-5_4-6*OC^XO*/3CO/6=O/3C]/1/");
//		lWURCSs.add("WURCS=2.0/1,2,1/[h1122h]/1-1/a6-b1*OPO*/3O/3=O");
//		lWURCSs.add("WURCS=2.0/4,4,3/[a2122h-1b_1-5][Aad21122h-2a_2-6_5*NCCO/3=O][a2122h-1a_1-5][Aadxxxxxh-2a_2-6_5*NCCO/3=O_8*OSO/3=O/3=O]/1-2-3-4/a6-b2_b8-c1_c6-d2");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2122h-1b_1-5_1*S]/1/");
//		lWURCSs.add("WURCS=2.0/6,12,11/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a2C22h-1b_1-5_2*NCC/3=O][a2112m-1a_1-5]/1-1-2-3-1-4-5-4-6-3-1-4/a4-b1_b4-c1_c3-d1_c6-j1_d2-e1_d4-g1_e4-f1_g3u-h1_g3d-i1_j2-k1_k4-l1");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2112h-1b_1-5_6*OCCCCCCCCC=^ZCCCCCCCCC/3=O]/1/");
//		lWURCSs.add("WURCS=2.0/1,1,0/[a2112h-1b_1-5_3n2-4n1*1OC^RO*2/3CO/6=O/3C]/1/");

		IAtomContainerSet mols = null;
		try {
			ISimpleChemObjectReader reader = ChemFormatType.WURCS.createReader();
			reader.setReader( new StringReader(String.join("\n", lWURCSs)) );
			if ( !reader.accepts(AtomContainerSet.class) )
				return;
			mols = reader.read(new AtomContainerSet());
			reader.close();
		} catch (CDKException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if ( mols == null )
			return;

		try {
			new DepictionGenerator()
			  .withSize(800, 800)
			  .withMargin(0.1)
			  .withZoom(3.0)
			  .withAtomColors()
			  .depict(mols.atomContainers())
			  .writeTo("RenderMolecule.png");
		} catch (IOException | CDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// write mol
		IChemObjectWriter writer = ChemFormatType.SDF.createWriter();
		try {
			writer.setWriter(System.out);
			if ( writer.accepts(mols.getClass()) )
				writer.write(mols);
			else if ( writer.accepts(mols.getAtomContainer(0).getClass()) )
				writer.write(mols.getAtomContainer(0));
			writer.close();
		} catch (CDKException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
