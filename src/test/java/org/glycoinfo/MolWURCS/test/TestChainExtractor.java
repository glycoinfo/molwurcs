package org.glycoinfo.MolWURCS.test;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.MolWURCS.exchange.toWURCS.CarbonChainExtractor;
import org.glycoinfo.MolWURCS.exchange.toWURCS.CarbonChainToBackbone;
import org.glycoinfo.MolWURCS.exchange.toWURCS.ModChainExtractor;
import org.glycoinfo.MolWURCS.exchange.toWURCS.ModChainToModification;
import org.glycoinfo.MolWURCS.om.buildingblock.CarbonChain;
import org.glycoinfo.MolWURCS.om.buildingblock.ModChain;
import org.glycoinfo.MolWURCS.util.analysis.MoleculeNormalizer;
import org.glycoinfo.WURCSFramework.wurcs.graph.Backbone;
import org.glycoinfo.WURCSFramework.wurcs.graph.Modification;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

public class TestChainExtractor extends SDFReader {

	public static void main(String[] args) {
		new TestChainExtractor().run(args);
	}

	@Override
	protected void process(IAtomContainer mol) {
		MoleculeNormalizer molNorm = new MoleculeNormalizer();
		molNorm.setDoAromatize(true);
		molNorm.normalize(mol);

		// Check stereo info
		for ( IAtom atom : mol.atoms() ) {
			if ( atom.getProperty(CDKConstants.CIP_DESCRIPTOR) == null )
				continue;
			System.out.println(String.format("%d%s: %s",
					mol.indexOf(atom), atom.getSymbol(),
					atom.getProperty(CDKConstants.CIP_DESCRIPTOR)) );
		}

		List<CarbonChain> lCCs = new CarbonChainExtractor().start(mol);
		if ( lCCs == null || lCCs.isEmpty() )
			return;

		for ( CarbonChain cc : lCCs ) {
			Backbone bb = CarbonChainToBackbone.convert(cc);
			System.out.println(String.format("%s-%d%c",
					bb.getSkeletonCode(), bb.getAnomericPosition(), bb.getAnomericSymbol()));
		}

		List<ModChain> lMCs = new ModChainExtractor().start(mol, lCCs, true);
		if ( lMCs == null || lMCs.isEmpty() )
			return;

		for ( ModChain mc : lMCs ) {
			System.out.println(getStarIndices(mc));
			Modification mod = ModChainToModification.convert(mc);
			System.out.println(mod.getMAPCode());
			System.out.println(getStarIndices(mc));
		}
	}

	private List<Integer> getStarIndices(ModChain mc) {
		List<Integer> lStarIndices = new ArrayList<>();
		for ( IBond bondFromStar : mc.getBondsFromStar() ) {
			int iStarIndex = mc.getStarIndex(bondFromStar);
			if ( lStarIndices.contains(iStarIndex) )
				continue;
			lStarIndices.add(iStarIndex);
		}
		return lStarIndices;
	}
}
