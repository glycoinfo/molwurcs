package org.glycoinfo.MolWURCS.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.glycoinfo.MolWURCS.util.analysis.MoleculeNormalizer;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IMolecularFormula;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.io.iterator.IteratingSDFReader;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCDKIO {

	private static Logger logger = LoggerFactory.getLogger(TestCDKIO.class);

	public static void main(String[] args) {
		for ( String file : args ) {
			logger.debug("Read {} ...", file);
			readFile(file);
		}
	}

	private static void readFile(String file) {
		File sdfile = new File(file);
		String dir = sdfile.getParent();
		FileReader reader = null;
		try {
			/* CDK does not automatically understand gzipped files */
			reader = new FileReader(sdfile);
		} catch (FileNotFoundException e) {
			System.err.println(file+" not found");
			return;
		}

		IteratingSDFReader sdfiter = new IteratingSDFReader(reader,
				DefaultChemObjectBuilder.getInstance());

		while (sdfiter.hasNext()) {
			IAtomContainer mol = sdfiter.next();

			// Molecular formula
			IMolecularFormula molForm = MolecularFormulaManipulator.getMolecularFormula(mol);
			logger.info( MolecularFormulaManipulator.getString(molForm) );

			for ( IBond bond : mol.bonds() ) {
				logger.info("Bond({}): {}-{}, {}, is aromatic? {}", bond.getIndex(),
						bond.getAtom(0).getSymbol(), bond.getAtom(1).getSymbol(),
						bond.getOrder().name(), bond.isAromatic() );
			}
			// Normalize
			MoleculeNormalizer normer = new MoleculeNormalizer();
			normer.normalize(mol);
			
			for ( IBond bond : mol.bonds() ) {
				logger.info("Bond({}): {}-{}, {}, is aromatic? {}", bond.getIndex(),
						bond.getAtom(0).getSymbol(), bond.getAtom(1).getSymbol(),
						bond.getOrder().name(), bond.isAromatic() );
			}

			// Collect structures
//			StructureAnalyzer analer = new StructureAnalyzer(mol);
//			analer.getAromaticAtoms().forEach(atom -> {
//				logger.info("Aromatic atom: {}", atom.getIndex());
//			});
			try {
				// set coordinates
				StructureDiagramGenerator sdg = new StructureDiagramGenerator(mol);
				sdg.generateCoordinates();
			} catch (CDKException e) {
				e.printStackTrace();
			}

			// Out as MDLMOL
			String title = mol.getTitle();
			if ( title == null || title.isEmpty() )
				title = sdfile.getName().replaceAll("\\..+$", "")+"-out";
			String outfile = dir+File.separator+title+".mol";
			try {
				MDLV2000Writer writer = new MDLV2000Writer(new FileWriter(new File(outfile)));
				writer.writeMolecule(mol);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
