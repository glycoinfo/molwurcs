package org.glycoinfo.MolWURCS.test;

import java.util.Set;

import org.glycoinfo.MolWURCS.util.analysis.MoleculeNormalizer;
import org.openscience.cdk.aromaticity.Aromaticity;
import org.openscience.cdk.aromaticity.ElectronDonation;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.CycleFinder;
import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;

public class TestAromaticity extends SDFReader {

	public static void main(String[] args) {
		new TestAromaticity().run(args);
	}

	private Aromaticity aromaticity;

	public TestAromaticity() {
		ElectronDonation model = ElectronDonation.daylight();
		CycleFinder cycles = Cycles.or(Cycles.all(), Cycles.all(5));
		this.aromaticity = new Aromaticity(model, cycles);
	}

	@Override
	protected void process(IAtomContainer a_mol) {
		System.out.println(a_mol);

		MoleculeNormalizer molNorm = new MoleculeNormalizer();
//		molNorm.setDoKeklize(false);
//		molNorm.setDoAromatize(false);
		molNorm.normalize(a_mol);

		try {
			if ( this.aromaticity.apply(a_mol) ) {
				Set<IBond> bonds = this.aromaticity.findBonds(a_mol);
				for ( IBond bond : bonds ) {
					System.out.println(a_mol.indexOf(bond));
				}
			}
		} catch (CDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



}
