package org.glycoinfo.MolWURCS.test;

import java.util.Iterator;

import org.glycoinfo.MolWURCS.util.analysis.MoleculeNormalizer;
import org.openscience.cdk.geometry.cip.CIPTool;
import org.openscience.cdk.graph.SpanningTree;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry;
import org.openscience.cdk.interfaces.IMolecularFormula;
import org.openscience.cdk.interfaces.IRingSet;
import org.openscience.cdk.interfaces.IStereoElement;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCDK extends SDFReader {

	private static Logger logger = LoggerFactory.getLogger(TestCDK.class);

	public static void main(String[] args) {
		new TestCDK().run(args);
	}

	protected void process(IAtomContainer a_mol) {
		String title = a_mol.getTitle();
		if ( title == null || title.isEmpty() )
			title = this.getCurrentFileName();
		logger.info("Read {} ...", title);

		// Molecular formula
		IMolecularFormula molForm = MolecularFormulaManipulator.getMolecularFormula(a_mol);
		logger.info( MolecularFormulaManipulator.getString(molForm) );

		// Count rings
		SpanningTree tree = new SpanningTree(a_mol);
		IRingSet rings = tree.getAllRings();
		int i=0;
		for ( IAtomContainer ring : rings.atomContainers() ) {
			logger.info("Ring #{}", ++i);
			ring.atoms().forEach( atom -> {
				logger.info("Ring atom: {}", a_mol.indexOf(atom));
			});
		}
		IAtomContainer fragments = tree.getCyclicFragmentsContainer();
		for ( IAtom atom : fragments.atoms() ) {
			logger.info("Cyclic fragment atom: {}", a_mol.indexOf(atom));
		}

		for ( IBond bond : a_mol.bonds() ) {
			logger.info("Bond({}): {}-{}, {}, is aromatic? {}", bond.getIndex(),
					bond.getAtom(0).getSymbol(), bond.getAtom(1).getSymbol(),
					bond.getOrder().name(), bond.isAromatic() );
		}
		// Normalize
		MoleculeNormalizer normer = new MoleculeNormalizer();
		normer.normalize(a_mol);
		for ( IBond bond : a_mol.bonds() ) {
			logger.info("Bond({}): {}-{}, {}, is aromatic? {}", bond.getIndex(),
					bond.getAtom(0).getSymbol(), bond.getAtom(1).getSymbol(),
					bond.getOrder().name(), bond.isAromatic() );
		}

		// Count heavy atoms
		int numHeavies = 0;
		for (IAtom atom : a_mol.atoms()) {
			if (atom.getAtomicNumber() > 1)
				numHeavies++;
		}

		CIPTool.label(a_mol);
		logger.info("Heavy atoms in {}: {}", title, numHeavies);
		Iterator<IStereoElement> itr = a_mol.stereoElements().iterator();
		while (itr.hasNext()) {
			IStereoElement elem = itr.next();
			if ( elem instanceof ITetrahedralChirality ) {
				ITetrahedralChirality chiral = (ITetrahedralChirality)elem;
				IAtom center = chiral.getChiralAtom();
				logger.info("{}", center.getProperties());
				logger.info("{}({})", center.getAtomTypeName(), center.getIndex());
				for ( IAtom atom : chiral.getCarriers() )
					logger.info("{}({})", atom.getAtomTypeName(), atom.getIndex());
			} else if ( elem instanceof IDoubleBondStereochemistry ) {
				IDoubleBondStereochemistry stereo = (IDoubleBondStereochemistry)elem;
			}
			logger.info("{}:{}\n", elem.toString(), elem.getConfig());
		}
	}

}
