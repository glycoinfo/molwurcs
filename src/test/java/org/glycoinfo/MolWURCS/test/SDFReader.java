package org.glycoinfo.MolWURCS.test;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.IChemObjectReader.Mode;
import org.openscience.cdk.io.iterator.IteratingSDFReader;

public abstract class SDFReader {

	private String m_strCurrentFileName;
	private int m_iCurrentMoleculeCount;

	public String getCurrentFileName() {
		return this.m_strCurrentFileName;
	}

	public int getCurrentMoleculeCount() {
		return this.m_iCurrentMoleculeCount;
	}

	public void run(String[] args) {
		if ( args.length != 0 ) {
			for ( String filename : args )
				readFile(new File(filename));
			return;
		}

		// Use FileDialog
		FileDialog dialog = new FileDialog((Frame)null, "", FileDialog.LOAD);
		dialog.setMultipleMode(true);
		dialog.setVisible(true);
		for ( File file : dialog.getFiles() )
			readFile(file);
		dialog.dispose();
	}

	private void readFile(File file) {
		FileReader reader = null;
		try {
			/* CDK does not automatically understand gzipped files */
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.err.println(file+" not found");
			return;
		}

		this.m_strCurrentFileName = file.getAbsolutePath();

		IteratingSDFReader sdfiter = new IteratingSDFReader(reader,
				DefaultChemObjectBuilder.getInstance());
		sdfiter.setSkip(true);
		sdfiter.setReaderMode(Mode.RELAXED);

		int i=0;
		while (sdfiter.hasNext()) {
			IAtomContainer mol = sdfiter.next();

			this.m_iCurrentMoleculeCount = i++;
			process(mol);
		}

		try {
			sdfiter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	abstract protected void process(IAtomContainer a_mol);
}
