package org.glycoinfo.MolWURCS.test.cdk;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.MACCSFingerprinter;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.smiles.SmilesParser;

public class TestFingerprint {

	public static void main(String[] args) {
		for (String s : args) {
			readString(s);
		}
	}

	private static void readString(String s) {
		SmilesParser sp = new SmilesParser(DefaultChemObjectBuilder.getInstance());
		try {
			IAtomContainer mol = sp.parseSmiles(s);
			MACCSFingerprinter fp = new MACCSFingerprinter(DefaultChemObjectBuilder.getInstance());
			System.out.println(s + ": " + fp.getBitFingerprint(mol).asBitSet());
		} catch (CDKException e) {
			e.printStackTrace();
		}
	}
}
