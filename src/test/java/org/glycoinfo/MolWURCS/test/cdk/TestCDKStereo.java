package org.glycoinfo.MolWURCS.test.cdk;

import java.io.File;
import java.io.FileWriter;

import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.geometry.cip.CIPTool;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IBond.Stereo;
import org.openscience.cdk.interfaces.IDoubleBondStereochemistry;
import org.openscience.cdk.interfaces.IMolecularFormula;
import org.openscience.cdk.interfaces.IStereoElement;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;

public class TestCDKStereo {

	public static class Carbohydrate {
		private IAtomContainer mol;

		public Carbohydrate(int length) {
			this.mol = createMainChain(length);
		}

		public IAtomContainer getMolecule() {
			return this.mol;
		}

		public static IAtomContainer createMainChain(int length) {
			IAtomContainer mol = new AtomContainer();

			// Carbon chain
			for ( int i=0; i<length; i++ ) {
				IAtom C = new Atom("C");
				C.setProperty("name", "C"+(i+1));
				mol.addAtom(C);

				if ( i == 0 )
					continue;
				mol.addBond(i-1, i, Order.SINGLE);
			}

			// Add oxygens
			for ( int i=0; i<length; i++ ) {
				IAtom O = new Atom("O");
				O.setProperty("name", "O"+(i+1));
				mol.addAtom(O);

				mol.addBond(i, i+length, Order.SINGLE);
			}

			return mol;
		}

		public void addRing(int posStart, int posEnd) {
			if ( posStart < 0 || posEnd < 0 )
				return;
			this.mol.addBond(posStart-1, posEnd+5, Order.SINGLE);
		}

		public void addCarbonyl(int pos) {
			if ( pos < 0 )
				return;
			IBond bond = this.mol.getBond(4+pos);
			bond.setOrder(Order.DOUBLE);
		}

		public void setStereo(int pos, Stereo stereo) {
			if ( pos < 0 )
				return;
			IBond bond = this.mol.getBond(4+pos);
			bond.setStereo(stereo);
			
		}
	}

	public static void main(String[] args) {
		Carbohydrate carbo = new Carbohydrate(6);
		carbo.addRing(1, 5);

		manipulateAtoms(carbo.getMolecule());

		carbo.setStereo(1, Stereo.UP);
		carbo.setStereo(2, Stereo.DOWN);
		carbo.setStereo(3, Stereo.UP);
		carbo.setStereo(4, Stereo.DOWN);
		carbo.setStereo(5, Stereo.DOWN);

		checkAtoms(carbo.getMolecule());
	}

	public static void manipulateAtoms(IAtomContainer mol) {
		try {
			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);

			// Add missing hydrogens
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(
					DefaultChemObjectBuilder.getInstance()
				);
			adder.addImplicitHydrogens(mol);
//			AtomContainerManipulator.convertImplicitToExplicitHydrogens(mol);

			StructureDiagramGenerator sdg = new StructureDiagramGenerator(mol);
			sdg.generateCoordinates();
		} catch (CDKException e) {
			e.printStackTrace();
		}

	}

	public static void checkAtoms(IAtomContainer mol) {
		// Molecular formula
		IMolecularFormula molForm = MolecularFormulaManipulator.getMolecularFormula(mol);
		System.out.println( MolecularFormulaManipulator.getString(molForm) );
		System.out.println();

		try {
			MDLV2000Writer writer = new MDLV2000Writer(new FileWriter(new File("test.mol")));
			writer.writeMolecule(mol);
		} catch (Exception e) {
			e.printStackTrace();
		}

		molForm = MolecularFormulaManipulator.getMolecularFormula(mol);
		System.out.println( MolecularFormulaManipulator.getString(molForm) );
		System.out.println();

		for ( IAtom atom : mol.atoms() ) {
			System.out.println(atom.getAtomTypeName()+":"+atom.getProperty("name"));
		}
		System.out.println();

		CIPTool.label(mol);
		for ( IStereoElement elem : mol.stereoElements() ) {
			if ( elem instanceof ITetrahedralChirality ) {
				ITetrahedralChirality chiral = (ITetrahedralChirality)elem;
				IAtom center = chiral.getChiralAtom();
				System.out.println(center.getProperties());
				System.out.println(center.getAtomTypeName()+"("+center.getIndex()+")");
				for ( IAtom atom : chiral.getCarriers() ) {
					System.out.println(atom.getAtomTypeName()+"("+atom.getIndex()+")");
				}
				System.out.println();
			} else if ( elem instanceof IDoubleBondStereochemistry ) {
				IDoubleBondStereochemistry stereo = (IDoubleBondStereochemistry)elem;
			}
			System.out.println( elem.toString()+":"+elem.getConfig());
		}
	}

}
