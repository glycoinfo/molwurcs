package org.glycoinfo.MolWURCS.test.cdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.config.Elements;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.silent.Atom;
import org.openscience.cdk.silent.AtomContainer;

public class TestChargeCorrection {

	public static void main(String[] args) throws CDKException {
		CDKAtomTypeMatcher matcher = CDKAtomTypeMatcher.getInstance(
				  DefaultChemObjectBuilder.getInstance()
				);

		IAtomContainer molecule = createMethoxycalbene();
		for ( IAtom atom : molecule.atoms() ) {
			IAtomType type = matcher.findMatchingAtomType(molecule, atom);
			System.out.println("Atom("+molecule.indexOf(atom)+"): "+type.getAtomTypeName());
		}

		List<Integer> lChargeCandidates = new ArrayList<>();
		for ( int i=1; i<=3; i++ )
			for ( int j=0; j<2; j++ )
				lChargeCandidates.add((j==0)? i : -i);
		Map<IAtom, List<Integer>> mapAtomToMatchedCharges = new HashMap<>();
		for ( IAtom atom : molecule.atoms() ) {
			IAtomType type = matcher.findMatchingAtomType(molecule, atom);
			if ( !type.getAtomTypeName().equals("X") )
				continue;
			Integer charge = atom.getFormalCharge();
			if (charge != null && charge != 0)
				continue;
			List<Integer> lMatchedCharges = new ArrayList<>();
			for ( int i : lChargeCandidates ) {
				atom.setFormalCharge(i);
				type = matcher.findMatchingAtomType(molecule, atom);
				if ( type.getAtomTypeName().equals("X") )
					continue;
				System.out.println("Atom("+molecule.indexOf(atom)+") type("+atom.getFormalCharge()+"): "+type.getAtomTypeName());
				lMatchedCharges.add(i);
			}
			mapAtomToMatchedCharges.put(atom, lMatchedCharges);
		}

		for ( IAtom atom : molecule.atoms() ) {
			if ( !mapAtomToMatchedCharges.containsKey(atom)) {
				IAtomType type = matcher.findMatchingAtomType(molecule, atom);
				System.out.println("Atom("+molecule.indexOf(atom)+"): "+type.getAtomTypeName());
				continue;
			}
			for ( Integer i : mapAtomToMatchedCharges.get(atom) ) {
				atom.setFormalCharge(i);
				IAtomType type = matcher.findMatchingAtomType(molecule, atom);
				System.out.println("Atom("+molecule.indexOf(atom)+") type("+atom.getFormalCharge()+"): "+type.getAtomTypeName());
			}
		}
	}

	private static IAtomContainer createAmmonia() {
		IAtomContainer molecule = new AtomContainer();
		IAtom atom = new Atom(Elements.NITROGEN);
		molecule.addAtom(atom);
		IAtom hydrogen = new Atom(Elements.HYDROGEN);
		molecule.addAtom(hydrogen);
		molecule.addBond(0,1,Order.SINGLE);
		hydrogen = new Atom(Elements.HYDROGEN);
		molecule.addAtom(hydrogen);
		molecule.addBond(0,2,Order.SINGLE);
		hydrogen = new Atom(Elements.HYDROGEN);
		molecule.addAtom(hydrogen);
		molecule.addBond(0,3,Order.SINGLE);
		hydrogen = new Atom(Elements.HYDROGEN);
		molecule.addAtom(hydrogen);
		molecule.addBond(0,4,Order.SINGLE);
		return molecule;
	}

	private static IAtomContainer createNitro() {
		IAtomContainer molecule = new AtomContainer();
		IAtom atom = new Atom(Elements.NITROGEN);
		atom.setFormalCharge(1);
		molecule.addAtom(atom);
		IAtom oxygen = new Atom(Elements.OXYGEN);
		oxygen.setFormalCharge(-1);
		molecule.addAtom(oxygen);
		molecule.addBond(0,1,Order.SINGLE);
		oxygen = new Atom(Elements.OXYGEN);
		molecule.addAtom(oxygen);
		molecule.addBond(0,2,Order.DOUBLE);
		IAtom carbon = new Atom(Elements.CARBON);
		molecule.addAtom(carbon);
		molecule.addBond(0,3,Order.SINGLE);
		return molecule;
	}

	private static IAtomContainer createAzide() {
		IAtomContainer molecule = new AtomContainer();
		IAtom atom = new Atom(Elements.NITROGEN);
		molecule.addAtom(atom);
		IAtom nitrogen = new Atom(Elements.NITROGEN);
		molecule.addAtom(nitrogen);
		molecule.addBond(0,1,Order.DOUBLE);
		nitrogen = new Atom(Elements.NITROGEN);
		molecule.addAtom(nitrogen);
		molecule.addBond(0,2,Order.DOUBLE);
		IAtom carbon = new Atom(Elements.CARBON);
		molecule.addAtom(carbon);
		molecule.addBond(1,3,Order.SINGLE);
		return molecule;
	}

	private static IAtomContainer createMethoxycalbene() {
		IAtomContainer molecule = new AtomContainer();
		IAtom atom = new Atom(Elements.OXYGEN);
		molecule.addAtom(atom);
		IAtom carbon = new Atom(Elements.CARBON);
		molecule.addAtom(carbon);
		molecule.addBond(0,1,Order.DOUBLE);
		carbon = new Atom(Elements.CARBON);
		molecule.addAtom(carbon);
		molecule.addBond(0,2,Order.SINGLE);
		return molecule;
	}
}
