package org.glycoinfo.MolWURCS.test.cdk;

import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.geometry.cip.CIPTool;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.IBond.Stereo;
import org.openscience.cdk.stereo.Stereocenters;
import org.openscience.cdk.stereo.TetrahedralChirality;

public class TestStereocenters {

	public static void main(String[] args) {
		// Model stereoisomer
		IAtomContainer isomer = new AtomContainer();
		isomer.addAtom(new Atom("C"));  // Atom 0
		isomer.addAtom(new Atom("Cl")); // Atom 1
		isomer.addAtom(new Atom("Br")); // Atom 2
		isomer.addAtom(new Atom("F"));  // Atom 3
		isomer.addAtom(new Atom("I"));  // Atom 4
		isomer.addBond(0,1,Order.SINGLE); // Bond 0 (C-Cl)
		isomer.addBond(0,2,Order.SINGLE); // Bond 1 (C-Br)
		isomer.getBond(1).setStereo(Stereo.UP);
		isomer.addBond(0,3,Order.SINGLE); // Bond 2 (C-F)
		isomer.getBond(2).setStereo(Stereo.UP);
		isomer.addBond(0,4,Order.SINGLE); // Bond 3 (C-I)

		for ( IAtom atom : isomer.atoms() )
			atom.setImplicitHydrogenCount(0);

		Stereocenters center = Stereocenters.of(isomer);
		for ( int i=0; i<isomer.getAtomCount(); i++ )
			System.out.println(i+": "+center.isStereocenter(i));

		IAtom[] ligands = new Atom[4];
		ligands[0] = isomer.getAtom(1);
		ligands[1] = isomer.getAtom(2);
		ligands[2] = isomer.getAtom(3);
		ligands[3] = isomer.getAtom(4);
		ITetrahedralChirality chiral = new TetrahedralChirality(
				isomer.getAtom(0), ligands,
				ITetrahedralChirality.Stereo.CLOCKWISE);
		isomer.addStereoElement(chiral);

		CIPTool.label(isomer);

		for ( IAtom atom : isomer.atoms() ) {
			String strCIP = atom.getProperty(CDKConstants.CIP_DESCRIPTOR);
			System.out.println(strCIP);
		}
	}

}
