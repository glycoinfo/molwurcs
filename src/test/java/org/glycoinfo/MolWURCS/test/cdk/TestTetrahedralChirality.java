package org.glycoinfo.MolWURCS.test.cdk;

import java.io.OutputStreamWriter;

import javax.vecmath.Vector2d;

import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.interfaces.ITetrahedralChirality;
import org.openscience.cdk.interfaces.ITetrahedralChirality.Stereo;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.stereo.TetrahedralChirality;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

public class TestTetrahedralChirality {

	public static void main(String[] args) {
		// Model stereoisomer
		IAtomContainer isomer = new AtomContainer();
		isomer.addAtom(new Atom("C"));  // Atom 0
		isomer.addAtom(new Atom("Cl")); // Atom 1
		isomer.addAtom(new Atom("Br")); // Atom 2
		isomer.addAtom(new Atom("F"));  // Atom 3
		isomer.addAtom(new Atom("I"));  // Atom 4
		isomer.addBond(0,1,Order.SINGLE); // Bond 0 (C-Cl)
		isomer.addBond(0,2,Order.SINGLE); // Bond 1 (C-Br)
//		isomer.getBond(1).setStereo(Stereo.UP);
		isomer.addBond(0,3,Order.SINGLE); // Bond 2 (C-F)
//		isomer.getBond(2).setStereo(Stereo.UP);
		isomer.addBond(0,4,Order.SINGLE); // Bond 3 (C-I)

		IAtom[] ligands = new IAtom[4];
		ligands[0] = isomer.getAtom(1);
		ligands[1] = isomer.getAtom(2);
		ligands[2] = isomer.getAtom(3);
		ligands[3] = isomer.getAtom(4);
		ITetrahedralChirality chirality = new TetrahedralChirality(
		  isomer.getAtom(0), ligands,
//		  Stereo.CLOCKWISE
		  Stereo.ANTI_CLOCKWISE
		);
		isomer.addStereoElement(chirality);

		// Write mol without coordinate
		MDLV2000Writer writer = new MDLV2000Writer(new OutputStreamWriter(System.out));
		try {
			writer.writeMolecule(isomer);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			// Percieve atom types
			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(isomer);

			// Add implicit hydrogens
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(
					DefaultChemObjectBuilder.getInstance()
				);
			adder.addImplicitHydrogens(isomer);

			// Generate 2d coordinate
			StructureDiagramGenerator sdg = new StructureDiagramGenerator(isomer);
			sdg.generateCoordinates(new Vector2d(0, 1));
		} catch (CDKException e) {
			e.printStackTrace();
		}

		// Write mol with coordinate
//		MDLV2000Writer writer = new MDLV2000Writer(new OutputStreamWriter(System.out));
		try {
			writer.writeMolecule(isomer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
