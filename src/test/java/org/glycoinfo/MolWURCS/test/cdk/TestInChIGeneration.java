package org.glycoinfo.MolWURCS.test.cdk;

import java.io.IOException;

import org.glycoinfo.MolWURCS.test.SDFReader;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.SMILESWriter;

import io.github.dan2097.jnainchi.InchiStatus;

public class TestInChIGeneration extends SDFReader {

	public static void main(String[] args) {
		new TestInChIGeneration().run(args);
	}

	@Override
	protected void process(IAtomContainer a_mol) {
		SMILESWriter writer = new SMILESWriter(System.out);
		try {
			System.out.println("Input:");
			writer.write(a_mol);
			String inchi = generateInChI(a_mol);
			System.out.println("InChI: "+inchi);
			a_mol = toStructure(inchi);
			System.out.println("Output:");
			writer.write(a_mol);
			inchi = generateInChI(a_mol);
			System.out.println("InChI: "+inchi);
		} catch (CDKException e) {
			e.printStackTrace();
		}
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String generateInChI(IAtomContainer container) throws CDKException {
		// Generate factory - throws CDKException if native code does not load
		InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
		// Get InChIGenerator
		InChIGenerator gen = factory.getInChIGenerator(container);

		InchiStatus ret = gen.getStatus();
		if (ret == InchiStatus.WARNING) {
		// InChI generated, but with warning message
		System.out.println("InChI warning: " + gen.getMessage());
		} else if (ret != InchiStatus.SUCCESS) {
		// InChI generation failed
		throw new CDKException("InChI failed: " + ret.toString()
		+ " [" + gen.getMessage() + "]");
		}

		return gen.getInchi();
	}

	private static IAtomContainer toStructure(String inchi) throws CDKException {
		InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
		// Get InChIToStructure
		InChIToStructure intostruct = factory.getInChIToStructure(
		inchi, DefaultChemObjectBuilder.getInstance()
		);

		InchiStatus ret = intostruct.getStatus();
		if (ret == InchiStatus.WARNING) {
		// Structure generated, but with warning message
		System.out.println("InChI warning: " + intostruct.getMessage());
		} else if (ret != InchiStatus.SUCCESS) {
		// Structure generation failed
		throw new CDKException("Structure generation failed failed: " + ret.toString()
		+ " [" + intostruct.getMessage() + "]");
		}

		return intostruct.getAtomContainer();
	}

}
