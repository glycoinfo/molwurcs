package org.glycoinfo.MolWURCS.test.cdk;

import java.io.OutputStreamWriter;

import org.glycoinfo.MolWURCS.test.TestCDK;
import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.aromaticity.Kekulization;
import org.openscience.cdk.config.Elements;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;
import org.openscience.cdk.interfaces.IBond.Order;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCDK2 {

	private static Logger logger = LoggerFactory.getLogger(TestCDK.class);

	public static void main(String[] args) {

		// Create carbon chain
		IAtomContainer molecule = new AtomContainer();
		for ( int i=0; i<6; i++ ) {
			IAtom atom = new Atom(Elements.CARBON);
			atom.setFormalCharge(0);
			molecule.addAtom(atom);
			if ( i==0 )
				continue;
			molecule.addBond(i-1, i, Order.UNSET);
		}
		molecule.addBond(5, 0, Order.UNSET);
		for ( IBond bond : molecule.bonds() ) {
			bond.setIsAromatic(true);
		}
		for ( IBond bond : molecule.bonds() ) {
			logger.info("Bond({}): {}-{}, {}, is aromatic? {}", bond.getIndex(),
					bond.getAtom(0).getSymbol(), bond.getAtom(1).getSymbol(),
					bond.getOrder().name(), bond.isAromatic() );
		}

		// Generate coordinates
		StructureDiagramGenerator sdg = new StructureDiagramGenerator(molecule);
		try {
			sdg.generateCoordinates();
		} catch (CDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		IAtomContainer moleculeNew = sdg.getMolecule();

		try {
			// perceive atom types
			AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(molecule);

			// add hydrogens
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance( DefaultChemObjectBuilder.getInstance() );
			adder.addImplicitHydrogens(molecule);
			AtomContainerManipulator.convertImplicitToExplicitHydrogens(molecule);

			// kekulize
			Kekulization.kekulize(molecule);

		} catch (CDKException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for ( IBond bond : molecule.bonds() ) {
			logger.info("Bond({}): {}-{}, {}, is aromatic? {}", bond.getIndex(),
					bond.getAtom(0).getSymbol(), bond.getAtom(1).getSymbol(),
					bond.getOrder().name(), bond.isAromatic() );
		}

		// write mol
		MDLV2000Writer writer = new MDLV2000Writer(new OutputStreamWriter(System.out));
		try {
			writer.writeMolecule(molecule);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		// Generate coordinates
//		StructureDiagramGenerator sdg = new StructureDiagramGenerator(molecule);
//		try {
//			sdg.generateCoordinates();
//		} catch (CDKException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		IAtomContainer moleculeNew = sdg.getMolecule();

		try {
			writer.writeMolecule(moleculeNew);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
