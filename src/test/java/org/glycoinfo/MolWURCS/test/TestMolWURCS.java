package org.glycoinfo.MolWURCS.test;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;

import org.glycoinfo.MolWURCS.cli.App;

public class TestMolWURCS {

	public static void main(String[] args) {
		// Use FileDialog
		FileDialog dialog = new FileDialog((Frame)null, "", FileDialog.LOAD);
		dialog.setMultipleMode(true);
		dialog.setVisible(true);
		for ( File file : dialog.getFiles() )
			readFile(args, file);
		dialog.dispose();
	}

	private static void readFile(String[] args, File file) {
		String[] argsNew = new String[args.length+1];
		for ( int i=0; i<args.length; i++ )
			argsNew[i] = args[i];
		argsNew[args.length] = file.getAbsolutePath();

		new App().run(argsNew);
	}
}
