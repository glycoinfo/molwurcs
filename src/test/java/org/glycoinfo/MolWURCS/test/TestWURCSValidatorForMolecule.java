package org.glycoinfo.MolWURCS.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.glycoinfo.MolWURCS.util.validation.WURCSValidatorForMolecule;

public class TestWURCSValidatorForMolecule {

	public static void main(String[] args) {
		List<String> lWURCSs;
		try {
			lWURCSs = Files.readAllLines( Paths.get("src/test/resources/AmbiguousStructures.wurcs") );
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		for ( String strWURCS : lWURCSs ) {
			WURCSValidatorForMolecule validator = new WURCSValidatorForMolecule();
			validator.start(strWURCS);
			System.out.println("WURCS: "+strWURCS);
			System.out.println("Validation results:\n"+validator.getReport().getResults());
		}
	}

	
}
